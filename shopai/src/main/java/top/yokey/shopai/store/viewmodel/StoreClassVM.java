package top.yokey.shopai.store.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.StoreClassBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.ShopClassController;

public class StoreClassVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<StoreClassBean>> storeClassLiveData = new MutableLiveData<>();

    public StoreClassVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<StoreClassBean>> getStoreClassLiveData() {

        return storeClassLiveData;

    }

    public void getStoreClass() {

        ShopClassController.index(new HttpCallBack<ArrayList<StoreClassBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<StoreClassBean> list) {
                storeClassLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
