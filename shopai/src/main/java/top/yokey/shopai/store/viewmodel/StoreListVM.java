package top.yokey.shopai.store.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.StoreListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.ShopController;
import top.yokey.shopai.zsdk.data.ShopSearchData;

public class StoreListVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<StoreListBean>> storeLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();

    public StoreListVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<StoreListBean>> getStoreLiveData() {

        return storeLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public void getShopList(ShopSearchData data) {

        ShopController.shopList(data, new HttpCallBack<ArrayList<StoreListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<StoreListBean> list) {
                storeLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
