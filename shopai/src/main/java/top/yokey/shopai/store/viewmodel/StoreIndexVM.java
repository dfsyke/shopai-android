package top.yokey.shopai.store.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.StoreGoodsRankBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberCartController;
import top.yokey.shopai.zsdk.controller.MemberFavoritesController;
import top.yokey.shopai.zsdk.controller.StoreController;

public class StoreIndexVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<StoreGoodsRankBean>> rankFavLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<StoreGoodsRankBean>> rankSaleLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> addCartLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> favoriteLiveData = new MutableLiveData<>();

    public StoreIndexVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<StoreGoodsRankBean>> getRankFavLiveData() {

        return rankFavLiveData;

    }

    public MutableLiveData<ArrayList<StoreGoodsRankBean>> getRankSaleLiveData() {

        return rankSaleLiveData;

    }

    public MutableLiveData<String> getAddCartLiveData() {

        return addCartLiveData;

    }

    public MutableLiveData<String> getFavoriteLiveData() {

        return favoriteLiveData;

    }

    public void getGoodsRankFav(String storeId) {

        StoreController.storeGoodsRank(storeId, "collectdesc", "3", new HttpCallBack<ArrayList<StoreGoodsRankBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<StoreGoodsRankBean> list) {
                rankFavLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getGoodsRankSale(String storeId) {

        StoreController.storeGoodsRank(storeId, "salenumdesc", "3", new HttpCallBack<ArrayList<StoreGoodsRankBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<StoreGoodsRankBean> list) {
                rankSaleLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void addCart(String goodsId, String quantity) {

        MemberCartController.cartAdd(goodsId, quantity, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                addCartLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

    public void favorite(String goodsId) {

        MemberFavoritesController.favoritesAdd(goodsId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                favoriteLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(4, reason));
            }
        });

    }

}
