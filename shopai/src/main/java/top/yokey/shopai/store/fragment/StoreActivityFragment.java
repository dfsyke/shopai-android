package top.yokey.shopai.store.fragment;

import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;

import com.jeremyliao.liveeventbus.LiveEventBus;

import top.yokey.shopai.R;
import top.yokey.shopai.store.viewmodel.StoreActivityVM;
import top.yokey.shopai.zcom.base.BaseFragment;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.bean.StoreBean;
import top.yokey.shopai.zsdk.bean.StoreSaleBean;

public class StoreActivityFragment extends BaseFragment {

    private CardView mansongCardView;
    private AppCompatTextView manSongNameTextView;
    private AppCompatTextView manSongTimeTextView;
    private AppCompatTextView manSongDescTextView;
    private AppCompatTextView manSongRemarkTextView;
    private CardView xianshiCardView;
    private AppCompatTextView xianshiNameTextView;
    private AppCompatTextView xianshiTimeTextView;
    private AppCompatTextView xianshiDescTextView;
    private AppCompatTextView xianshiRemarkTextView;
    private AppCompatTextView activityTextView;
    private String storeId = "";
    private StoreSaleBean bean = null;
    private StoreActivityVM vm = null;

    @Override
    public View initView() {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_store_activity, null);
        mansongCardView = view.findViewById(R.id.mansongCardView);
        manSongNameTextView = view.findViewById(R.id.manSongNameTextView);
        manSongTimeTextView = view.findViewById(R.id.manSongTimeTextView);
        manSongDescTextView = view.findViewById(R.id.manSongDescTextView);
        manSongRemarkTextView = view.findViewById(R.id.manSongRemarkTextView);
        xianshiCardView = view.findViewById(R.id.xianshiCardView);
        xianshiNameTextView = view.findViewById(R.id.xianshiNameTextView);
        xianshiTimeTextView = view.findViewById(R.id.xianshiTimeTextView);
        xianshiDescTextView = view.findViewById(R.id.xianshiDescTextView);
        xianshiRemarkTextView = view.findViewById(R.id.xianshiRemarkTextView);
        activityTextView = view.findViewById(R.id.activityTextView);
        return view;

    }

    @Override
    public void initData() {

        vm = getVM(StoreActivityVM.class);

    }

    @Override
    public void initEvent() {

        activityTextView.setOnClickListener(view -> vm.getStoreSale(storeId));

        mansongCardView.setOnClickListener(view -> {
        });

        xianshiCardView.setOnClickListener(view -> {
        });

    }

    @Override
    public void initObserve() {

        vm.getSaleClassLiveData().observe(this, string -> {
            activityTextView.setVisibility(View.GONE);
            string = string.replace("[]", "null");
            if (string.equals("null")) {
                mansongCardView.setVisibility(View.GONE);
                xianshiCardView.setVisibility(View.GONE);
                activityTextView.setVisibility(View.VISIBLE);
            } else {
                mansongCardView.setVisibility(View.VISIBLE);
                xianshiCardView.setVisibility(View.VISIBLE);
                activityTextView.setVisibility(View.GONE);
                bean = JsonUtil.json2Object(string, StoreSaleBean.class);
                if (bean.getMansong() == null) {
                    mansongCardView.setVisibility(View.GONE);
                } else {
                    mansongCardView.setVisibility(View.VISIBLE);
                    manSongNameTextView.setText(bean.getMansong().getMansongName());
                    manSongTimeTextView.setText(String.format(getString(R.string.storeActivityTime), bean.getMansong().getStartTimeText(), bean.getMansong().getEndTimeText()));
                    manSongDescTextView.setText(String.format(getString(R.string.storeActivityMansong), bean.getMansong().getRules().get(0).getPrice(), bean.getMansong().getRules().get(0).getDiscount(), bean.getMansong().getRules().get(0).getMansongGoodsName()));
                    manSongRemarkTextView.setText(String.format(getString(R.string.storeActivityDesc), bean.getMansong().getRemark()));
                }
                if (bean.getXianshi() == null) {
                    xianshiCardView.setVisibility(View.GONE);
                } else {
                    xianshiCardView.setVisibility(View.VISIBLE);
                    xianshiNameTextView.setText(bean.getXianshi().getXianshiName());
                    xianshiTimeTextView.setText(String.format(getString(R.string.storeActivityTime), bean.getXianshi().getStartTimeText(), bean.getXianshi().getEndTimeText()));
                    xianshiDescTextView.setText(String.format(getString(R.string.storeActivityXianshi), bean.getXianshi().getLowerLimit()));
                    xianshiRemarkTextView.setText(String.format(getString(R.string.storeActivityDesc), bean.getXianshi().getXianshiExplain()));
                }
            }
        });

        vm.getErrorLiveData().observe(this, bean -> {
            activityTextView.setVisibility(View.VISIBLE);
            activityTextView.setText(bean.getReason());
        });

        LiveEventBus.get(Constant.DATA_BEAN, StoreBean.class).observe(this, bean -> {
            storeId = bean.getStoreInfo().getStoreId();
            vm.getStoreSale(storeId);
        });

    }

}
