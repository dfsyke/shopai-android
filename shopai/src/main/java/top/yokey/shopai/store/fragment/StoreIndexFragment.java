package top.yokey.shopai.store.fragment;

import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.jeremyliao.liveeventbus.LiveEventBus;
import com.youth.banner.Banner;

import java.util.ArrayList;
import java.util.List;

import top.yokey.shopai.R;
import top.yokey.shopai.store.adapter.StoreGoodsAdapter;
import top.yokey.shopai.store.viewmodel.StoreIndexVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.base.BaseFragment;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.SpaceDecoration;
import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.bean.GoodsListBean;
import top.yokey.shopai.zsdk.bean.StoreBean;
import top.yokey.shopai.zsdk.bean.StoreGoodsRankBean;
import top.yokey.shopai.zsdk.data.GoodsData;

public class StoreIndexFragment extends BaseFragment {

    private final AppCompatImageView[] favoriteRankImageView = new AppCompatImageView[3];
    private final AppCompatTextView[] favoriteRankNameTextView = new AppCompatTextView[3];
    private final AppCompatImageView[] saleRankImageView = new AppCompatImageView[3];
    private final AppCompatTextView[] saleRankNameTextView = new AppCompatTextView[3];
    private final ArrayList<GoodsListBean> arrayList = new ArrayList<>();
    private final StoreGoodsAdapter adapter = new StoreGoodsAdapter(arrayList, true);
    private Banner mainBanner = null;
    private AppCompatTextView favoriteRankTextView = null;
    private AppCompatTextView saleRankTextView = null;
    private LinearLayoutCompat favoriteRankLinearLayout = null;
    private LinearLayoutCompat saleRankLinearLayout = null;
    private RecyclerView mainRecyclerView = null;
    private StoreIndexVM vm = null;

    @Override
    public View initView() {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_store_index, null);
        mainBanner = view.findViewById(R.id.mainBanner);
        favoriteRankTextView = view.findViewById(R.id.favoriteRankTextView);
        saleRankTextView = view.findViewById(R.id.saleRankTextView);
        favoriteRankLinearLayout = view.findViewById(R.id.favoriteRankLinearLayout);
        favoriteRankImageView[0] = view.findViewById(R.id.favoriteRankOneImageView);
        favoriteRankImageView[1] = view.findViewById(R.id.favoriteRankTwoImageView);
        favoriteRankImageView[2] = view.findViewById(R.id.favoriteRankThrImageView);
        favoriteRankNameTextView[0] = view.findViewById(R.id.favoriteRankOneTextView);
        favoriteRankNameTextView[1] = view.findViewById(R.id.favoriteRankTwoTextView);
        favoriteRankNameTextView[2] = view.findViewById(R.id.favoriteRankThrTextView);
        saleRankLinearLayout = view.findViewById(R.id.saleRankLinearLayout);
        saleRankImageView[0] = view.findViewById(R.id.saleRankOneImageView);
        saleRankImageView[1] = view.findViewById(R.id.saleRankTwoImageView);
        saleRankImageView[2] = view.findViewById(R.id.saleRankThrImageView);
        saleRankNameTextView[0] = view.findViewById(R.id.saleRankOneTextView);
        saleRankNameTextView[1] = view.findViewById(R.id.saleRankTwoTextView);
        saleRankNameTextView[2] = view.findViewById(R.id.saleRankThrTextView);
        mainRecyclerView = view.findViewById(R.id.mainRecyclerView);
        return view;

    }

    @Override
    public void initData() {

        App.get().setRecyclerView(mainRecyclerView, adapter);
        mainRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mainRecyclerView.addItemDecoration(new SpaceDecoration(App.get().dp2Px(4)));
        vm = getVM(StoreIndexVM.class);

    }

    @Override
    public void initEvent() {

        favoriteRankTextView.setOnClickListener(view -> {
            favoriteRankTextView.setTextColor(App.get().getColors(R.color.accent));
            favoriteRankLinearLayout.setVisibility(View.VISIBLE);
            saleRankTextView.setTextColor(App.get().getColors(R.color.textTwo));
            saleRankLinearLayout.setVisibility(View.GONE);
        });

        saleRankTextView.setOnClickListener(view -> {
            favoriteRankTextView.setTextColor(App.get().getColors(R.color.textTwo));
            favoriteRankLinearLayout.setVisibility(View.GONE);
            saleRankTextView.setTextColor(App.get().getColors(R.color.accent));
            saleRankLinearLayout.setVisibility(View.VISIBLE);
        });

        adapter.setOnItemClickListener(new StoreGoodsAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, GoodsListBean bean) {
                App.get().startGoods(new GoodsData(bean.getGoodsId()));
            }

            @Override
            public void onClickFavorate(int position, GoodsListBean bean) {
                if (App.get().isMemberLogin()) {
                    vm.favorite(bean.getGoodsId());
                } else {
                    App.get().startMemberLogin();
                }
            }

            @Override
            public void onClickCart(int position, GoodsListBean bean) {
                if (App.get().isMemberLogin()) {
                    vm.addCart(bean.getGoodsId(), "1");
                } else {
                    App.get().startMemberLogin();
                }
            }

            @Override
            public void onClickStore(int position, GoodsListBean bean) {

            }
        });

    }

    @Override
    public void initObserve() {

        vm.getRankFavLiveData().observe(this, arrayList -> {
            StoreGoodsRankBean bean;
            for (int i = 0; i < arrayList.size(); i++) {
                bean = arrayList.get(i);
                String goodsId = bean.getGoodsId();
                ImageHelp.get().displayRadius(bean.getGoodsImageUrl(), favoriteRankImageView[i]);
                favoriteRankNameTextView[i].setText(String.format(App.get().getString(R.string.storeSaleAndPrice), bean.getGoodsSalenum(), bean.getGoodsPrice()));
                favoriteRankImageView[i].setOnClickListener(view -> App.get().startGoods(new GoodsData(goodsId)));
            }
        });

        vm.getRankSaleLiveData().observe(this, arrayList -> {
            StoreGoodsRankBean bean;
            for (int i = 0; i < arrayList.size(); i++) {
                bean = arrayList.get(i);
                String goodsId = bean.getGoodsId();
                ImageHelp.get().displayRadius(bean.getGoodsImageUrl(), saleRankImageView[i]);
                saleRankNameTextView[i].setText(String.format(App.get().getString(R.string.storeSaleAndPrice), bean.getGoodsSalenum(), bean.getGoodsPrice()));
                saleRankImageView[i].setOnClickListener(view -> App.get().startGoods(new GoodsData(goodsId)));
            }
        });

        vm.getAddCartLiveData().observe(this, string -> ToastHelp.get().show(R.string.tipsAlreadyAddCart));

        vm.getFavoriteLiveData().observe(this, string -> ToastHelp.get().show(R.string.tipsAlreadyFavorite));

        vm.getErrorLiveData().observe(this, bean -> ToastHelp.get().show(bean.getReason()));

        LiveEventBus.get(Constant.DATA_BEAN, StoreBean.class).observe(this, bean -> {
            arrayList.clear();
            String json = JsonUtil.toJson(bean.getRecGoodsList());
            arrayList.addAll(JsonUtil.json2ArrayList(json, GoodsListBean.class));
            adapter.notifyDataSetChanged();
            if (bean.getStoreInfo().getMbSliders().size() != 0) {
                List<String> image = new ArrayList<>();
                List<String> type = new ArrayList<>();
                List<String> data = new ArrayList<>();
                for (int i = 0; i < bean.getStoreInfo().getMbSliders().size(); i++) {
                    image.add(bean.getStoreInfo().getMbSliders().get(i).getImgUrl());
                    type.add(bean.getStoreInfo().getMbSliders().get(i).getType());
                    data.add(bean.getStoreInfo().getMbSliders().get(i).getLink());
                }
                App.get().setBanner(mainBanner, 0, image, position -> App.get().startTypeValue(type.get(position), data.get(position)));
                LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) mainBanner.getLayoutParams();
                layoutParams.height = (int) (App.get().getWidth() / 2.5);
                mainBanner.setLayoutParams(layoutParams);
                mainBanner.setVisibility(View.VISIBLE);
            } else {
                mainBanner.setVisibility(View.GONE);
            }
            vm.getGoodsRankFav(bean.getStoreInfo().getStoreId());
            vm.getGoodsRankSale(bean.getStoreInfo().getStoreId());
        });

    }

}
