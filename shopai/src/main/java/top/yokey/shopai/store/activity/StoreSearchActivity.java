package top.yokey.shopai.store.activity;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.store.adapter.StoreGoodsClassAdapter;
import top.yokey.shopai.store.viewmodel.StoreSearchVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.voice.XFVoiceDialog;
import top.yokey.shopai.zsdk.bean.StoreGoodsClassBean;
import top.yokey.shopai.zsdk.data.GoodsSearchData;

@Route(path = ARoutePath.STORE_SEARCH)
public class StoreSearchActivity extends BaseActivity {

    private final ArrayList<StoreGoodsClassBean> arrayList = new ArrayList<>();
    private final StoreGoodsClassAdapter adapter = new StoreGoodsClassAdapter(arrayList);
    @Autowired(name = Constant.DATA_ID)
    String storeId;
    private Toolbar mainToolbar = null;
    private AppCompatEditText toolbarEditText = null;
    private AppCompatImageView toolbarImageView = null;
    private RecyclerView mainRecyclerView = null;
    private FloatingActionButton voiceButton = null;
    private StoreSearchVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_store_search);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarEditText = findViewById(R.id.toolbarEditText);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        mainRecyclerView = findViewById(R.id.mainRecyclerView);
        voiceButton = findViewById(R.id.voiceButton);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(storeId)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainLinearLayout);
        toolbarEditText.setHint(R.string.tipsSearchHint);
        App.get().setRecyclerView(mainRecyclerView, adapter);
        vm = getVM(StoreSearchVM.class);
        vm.getData(storeId);

    }

    @Override
    public void initEvent() {

        toolbarImageView.setOnClickListener(view -> {
            GoodsSearchData data = new GoodsSearchData();
            data.setStoreId(storeId);
            data.setKeyword(Objects.requireNonNull(toolbarEditText.getText()).toString());
            App.get().start(ARoutePath.STORE_GOODS, Constant.DATA_JSON, JsonUtil.toJson(data));
            onReturn(false);
        });

        adapter.setOnItemClickListener(new StoreGoodsClassAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, StoreGoodsClassBean bean) {
                GoodsSearchData data = new GoodsSearchData();
                data.setStoreId(storeId);
                data.setStcId(bean.getId());
                App.get().start(ARoutePath.STORE_GOODS, Constant.DATA_JSON, JsonUtil.toJson(data));
                onReturn(false);
            }

            @Override
            public void onChildClick(int position, StoreGoodsClassBean bean) {
                GoodsSearchData data = new GoodsSearchData();
                data.setStoreId(storeId);
                data.setStcId(bean.getId());
                App.get().start(ARoutePath.STORE_GOODS, Constant.DATA_JSON, JsonUtil.toJson(data));
                onReturn(false);
            }
        });

        voiceButton.setOnClickListener(view -> XFVoiceDialog.get().show(get(), result -> {
            if (!VerifyUtil.isEmpty(result)) {
                GoodsSearchData data = new GoodsSearchData();
                data.setStoreId(storeId);
                data.setKeyword(result);
                App.get().start(ARoutePath.STORE_GOODS, Constant.DATA_JSON, JsonUtil.toJson(data));
                onReturn(false);
            }
        }));

    }

    @Override
    public void initObserve() {

        vm.getClassLiveData().observe(this, list -> {
            arrayList.clear();
            ArrayList<StoreGoodsClassBean> levelArrayList = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getLevel().equals("1")) {
                    arrayList.add(list.get(i));
                }
                if (list.get(i).getLevel().equals("2")) {
                    levelArrayList.add(list.get(i));
                }
            }
            for (int i = 0; i < arrayList.size(); i++) {
                for (int j = 0; j < levelArrayList.size(); j++) {
                    if (arrayList.get(i).getId().equals(levelArrayList.get(j).getPid())) {
                        arrayList.get(i).getChild().add(levelArrayList.get(j));
                    }
                }
            }
            adapter.notifyDataSetChanged();
        });

    }

}
