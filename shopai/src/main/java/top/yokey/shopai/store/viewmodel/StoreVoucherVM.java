package top.yokey.shopai.store.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.VoucherTplListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberVoucherController;
import top.yokey.shopai.zsdk.controller.VoucherController;

public class StoreVoucherVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<VoucherTplListBean>> voucherLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> freexLiveData = new MutableLiveData<>();

    public StoreVoucherVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<VoucherTplListBean>> getVoucherLiveData() {

        return voucherLiveData;

    }

    public MutableLiveData<String> getFreexLiveData() {

        return freexLiveData;

    }

    public void getVoucher(String storeId) {

        VoucherController.voucherTplList(storeId, "free", new HttpCallBack<ArrayList<VoucherTplListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<VoucherTplListBean> list) {
                voucherLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void voucherFreex(String tid) {

        MemberVoucherController.voucherFreeex(tid, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                freexLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
