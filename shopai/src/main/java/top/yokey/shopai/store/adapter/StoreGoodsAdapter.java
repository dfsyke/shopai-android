package top.yokey.shopai.store.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.GoodsListBean;

public class StoreGoodsAdapter extends RecyclerView.Adapter<StoreGoodsAdapter.ViewHolder> {

    private final ArrayList<GoodsListBean> arrayList;
    private boolean isGrid;
    private OnItemClickListener onItemClickListener = null;

    public StoreGoodsAdapter(ArrayList<GoodsListBean> arrayList, boolean isGrid) {

        this.isGrid = isGrid;
        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        GoodsListBean bean = arrayList.get(position);
        holder.mobileImageView.setVisibility(View.GONE);
        ImageHelp.get().displayRadius(bean.getGoodsImageUrl(), holder.mainImageView);
        int height = (App.get().getWidth() - 64) / 2;
        ViewGroup.LayoutParams layoutParams = holder.mainImageView.getLayoutParams();
        if (isGrid) layoutParams.height = height;
        holder.mainImageView.setLayoutParams(layoutParams);
        holder.nameTextView.setText(bean.getGoodsName());
        holder.descTextView.setText(bean.getGoodsJingle());
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getGoodsPrice());
        holder.mobileImageView.setVisibility(bean.isSoleFlag() ? View.VISIBLE : View.GONE);
        holder.saleTextView.setText(R.string.saleNumber);
        holder.saleTextView.append("：");
        holder.saleTextView.append(bean.getGoodsSalenum());
        if (isGrid) {
            holder.descTextView.setVisibility(View.GONE);
            holder.descTextView.setText("");
        } else {
            holder.descTextView.setVisibility(View.VISIBLE);
            holder.descTextView.setText(bean.getGoodsJingle());
        }
        if (bean.getIsOwnShop().equals("1")) {
            String name = App.get().handlerHtml(App.get().getString(R.string.htmlOwn), "#EE0000");
            name = name + " " + holder.nameTextView.getText().toString();
            holder.nameTextView.setText(Html.fromHtml(name));
        }

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

        holder.favoriteImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickFavorate(position, bean);
            }
        });

        holder.cartImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickCart(position, bean);
            }
        });

        holder.storeImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickStore(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view;
        if (isGrid) {
            view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_store_goods, group, false);
        } else {
            view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_store_goods_ver, group, false);
        }
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setGrid(boolean grid) {

        isGrid = grid;

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, GoodsListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickFavorate(int position, GoodsListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickCart(int position, GoodsListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickStore(int position, GoodsListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView descTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatImageView mobileImageView;
        private final AppCompatTextView saleTextView;
        private final AppCompatImageView favoriteImageView;
        private final AppCompatImageView cartImageView;
        private final AppCompatImageView storeImageView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            descTextView = view.findViewById(R.id.descTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            mobileImageView = view.findViewById(R.id.mobileImageView);
            saleTextView = view.findViewById(R.id.saleTextView);
            favoriteImageView = view.findViewById(R.id.favoriteImageView);
            cartImageView = view.findViewById(R.id.cartImageView);
            storeImageView = view.findViewById(R.id.storeImageView);

        }

    }

}
