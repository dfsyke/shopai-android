package top.yokey.shopai.goods.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.bean.EvaluateBean;

public class GoodsEvaluateAdapter extends RecyclerView.Adapter<GoodsEvaluateAdapter.ViewHolder> {

    private final ArrayList<EvaluateBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public GoodsEvaluateAdapter(ArrayList<EvaluateBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        EvaluateBean bean = arrayList.get(position);
        ImageHelp.get().displayCircle(bean.getMemberAvatar(), holder.mainImageView);
        holder.nameTextView.setText(bean.getGevalFrommembername());
        holder.timeTextView.setText(bean.getGevalAddtimeDate());
        holder.scoreRatingBar.setRating(Float.parseFloat(bean.getGevalScores()));
        holder.contentTextView.setText(bean.getGevalContent());
        if (bean.getGevalImage240().size() == 0) {
            holder.imageLinearLayout.setVisibility(View.GONE);
        } else {
            holder.imageLinearLayout.setVisibility(View.VISIBLE);
            for (AppCompatImageView appCompatImageView : holder.evaImageView) {
                appCompatImageView.setVisibility(View.GONE);
            }
            for (int i = 0; i < bean.getGevalImage240().size(); i++) {
                final int pos = i;
                if (i < 5) {
                    holder.evaImageView[pos].setVisibility(View.VISIBLE);
                    ImageHelp.get().displayRadius(bean.getGevalImage240().get(pos), holder.evaImageView[pos]);
                    holder.evaImageView[i].setOnClickListener(view -> {
                        if (onItemClickListener != null) {
                            onItemClickListener.onClickImage(position, pos, bean);
                        }
                    });
                }
            }
        }
        if (VerifyUtil.isEmpty(bean.getGevalExplain())) {
            holder.explainTextView.setVisibility(View.GONE);
        } else {
            holder.explainTextView.setVisibility(View.VISIBLE);
            holder.explainTextView.setText(String.format(App.get().getString(R.string.evaluateStoreReply), bean.getGevalExplain()));
        }
        holder.appendTimeTextView.setVisibility(View.GONE);
        holder.appendTextView.setVisibility(View.GONE);
        holder.appendLinearLayout.setVisibility(View.GONE);
        holder.appendExplainTextView.setVisibility(View.GONE);
        if (!VerifyUtil.isEmpty(bean.getGevalContentAgain())) {
            holder.appendTimeTextView.setVisibility(View.VISIBLE);
            holder.appendTextView.setVisibility(View.VISIBLE);
            holder.appendTimeTextView.setText(bean.getGevalAddtimeAgainDate());
            holder.appendTimeTextView.append(" " + App.get().getString(R.string.appendEvaluate));
            holder.appendTextView.setText(bean.getGevalContentAgain());
            if (bean.getGevalImageAgain240().size() != 0) {
                holder.appendLinearLayout.setVisibility(View.VISIBLE);
                for (AppCompatImageView appCompatImageView : holder.appendImageView) {
                    appCompatImageView.setVisibility(View.GONE);
                }
                for (int i = 0; i < bean.getGevalImageAgain240().size(); i++) {
                    final int pos = i;
                    if (i < 5) {
                        holder.appendImageView[pos].setVisibility(View.VISIBLE);
                        ImageHelp.get().displayRadius(bean.getGevalImageAgain240().get(pos), holder.appendImageView[pos]);
                        holder.appendImageView[i].setOnClickListener(view -> {
                            if (onItemClickListener != null) {
                                onItemClickListener.onClickImageAgain(position, pos, bean);
                            }
                        });
                    }
                }
            }
            if (!VerifyUtil.isEmpty(bean.getGevalExplainAgain())) {
                holder.appendExplainTextView.setVisibility(View.VISIBLE);
                holder.appendExplainTextView.setText(String.format(App.get().getString(R.string.evaluateStoreReply), bean.getGevalExplainAgain()));
            }
        }

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_goods_evaluate, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, EvaluateBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickImage(int position, int pos, EvaluateBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickImageAgain(int position, int pos, EvaluateBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView timeTextView;
        private final AppCompatRatingBar scoreRatingBar;
        private final AppCompatTextView contentTextView;
        private final LinearLayoutCompat imageLinearLayout;
        private final AppCompatImageView[] evaImageView;
        private final AppCompatTextView explainTextView;
        private final AppCompatTextView appendTimeTextView;
        private final AppCompatTextView appendTextView;
        private final LinearLayoutCompat appendLinearLayout;
        private final AppCompatImageView[] appendImageView;
        private final AppCompatTextView appendExplainTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            timeTextView = view.findViewById(R.id.timeTextView);
            scoreRatingBar = view.findViewById(R.id.scoreRatingBar);
            contentTextView = view.findViewById(R.id.contentTextView);
            imageLinearLayout = view.findViewById(R.id.imageLinearLayout);
            evaImageView = new AppCompatImageView[5];
            evaImageView[0] = view.findViewById(R.id.oneImageView);
            evaImageView[1] = view.findViewById(R.id.twoImageView);
            evaImageView[2] = view.findViewById(R.id.thrImageView);
            evaImageView[3] = view.findViewById(R.id.fouImageView);
            evaImageView[4] = view.findViewById(R.id.fivImageView);
            explainTextView = view.findViewById(R.id.explainTextView);
            appendTimeTextView = view.findViewById(R.id.appendTimeTextView);
            appendTextView = view.findViewById(R.id.appendTextView);
            appendLinearLayout = view.findViewById(R.id.appendLinearLayout);
            appendImageView = new AppCompatImageView[5];
            appendImageView[0] = view.findViewById(R.id.appendOneImageView);
            appendImageView[1] = view.findViewById(R.id.appendTwoImageView);
            appendImageView[2] = view.findViewById(R.id.appendThrImageView);
            appendImageView[3] = view.findViewById(R.id.appendFouImageView);
            appendImageView[4] = view.findViewById(R.id.appendFivImageView);
            appendExplainTextView = view.findViewById(R.id.appendExplainTextView);

        }

    }

}
