package top.yokey.shopai.goods.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zsdk.bean.EvaluateBean;

public class GoodsEvaluateSAdapter extends RecyclerView.Adapter<GoodsEvaluateSAdapter.ViewHolder> {

    private final ArrayList<EvaluateBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public GoodsEvaluateSAdapter(ArrayList<EvaluateBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        EvaluateBean bean = arrayList.get(position);
        holder.mainRatingBar.setRating(Float.parseFloat(bean.getGevalScores()));
        holder.contentTextView.setText(bean.getGevalFrommembername());
        holder.contentTextView.append("：");
        holder.contentTextView.append(bean.getGevalContent());
        holder.timeTextView.setText(bean.getGevalAddtimeDate());

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_goods_evaluate_s, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, EvaluateBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatRatingBar mainRatingBar;
        private final AppCompatTextView contentTextView;
        private final AppCompatTextView timeTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainRatingBar = view.findViewById(R.id.mainRatingBar);
            contentTextView = view.findViewById(R.id.contentTextView);
            timeTextView = view.findViewById(R.id.timeTextView);

        }

    }

}
