package top.yokey.shopai.goods.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.EvaluateBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.GoodsController;

public class GoodsEvaluateVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<EvaluateBean>> evaluateLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();

    public GoodsEvaluateVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<EvaluateBean>> getEvaluateLiveData() {

        return evaluateLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public void getEvaluate(String goodsId, String type, String page) {

        GoodsController.goodsEvaluate(goodsId, type, page, new HttpCallBack<ArrayList<EvaluateBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<EvaluateBean> list) {
                evaluateLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
