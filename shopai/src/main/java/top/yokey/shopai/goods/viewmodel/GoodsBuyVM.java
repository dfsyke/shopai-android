package top.yokey.shopai.goods.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberBuyController;
import top.yokey.shopai.zsdk.data.GoodsBuyData;

public class GoodsBuyVM extends BaseViewModel {

    private final MutableLiveData<String> step1LiveData = new MutableLiveData<>();
    private final MutableLiveData<String> step2LiveData = new MutableLiveData<>();

    public GoodsBuyVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getStep1LiveData() {

        return step1LiveData;

    }

    public MutableLiveData<String> getStep2LiveData() {

        return step2LiveData;

    }

    public void buyStep1(GoodsBuyData data) {

        MemberBuyController.buyStep1(data, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                step1LiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void buyStep2(GoodsBuyData data) {

        MemberBuyController.buyStep2(data, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                step2LiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
