package top.yokey.shopai.goods.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Vector;

import top.yokey.shopai.R;
import top.yokey.shopai.goods.adapter.GoodsContractAdapter;
import top.yokey.shopai.goods.adapter.GoodsRobbuyAdapter;
import top.yokey.shopai.goods.adapter.GoodsXianshiAdapter;
import top.yokey.shopai.goods.viewmodel.GoodsRobbuyVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.GridDecoration;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.AnimUtil;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.GoodsRobbuyBean;
import top.yokey.shopai.zsdk.bean.GoodsXianshiBean;
import top.yokey.shopai.zsdk.bean.SearchShowBean;
import top.yokey.shopai.zsdk.data.GoodsData;
import top.yokey.shopai.zsdk.data.GoodsSearchData;

@Route(path = ARoutePath.GOODS_ROBBUY)
public class GoodsRobbuyActivity extends BaseActivity {

    private final GoodsSearchData data = new GoodsSearchData();
    private final Vector<String> areaId = new Vector<>();
    private final Vector<String> areaName = new Vector<>();
    private final ArrayList<GoodsXianshiBean> xianshiArrayList = new ArrayList<>();
    private final GoodsXianshiAdapter xianshiAdapter = new GoodsXianshiAdapter(xianshiArrayList);
    private final ArrayList<GoodsRobbuyBean> robbuyArrayList = new ArrayList<>();
    private final GoodsRobbuyAdapter robbuyAdapter = new GoodsRobbuyAdapter(robbuyArrayList);
    private final ArrayList<SearchShowBean.ContractListBean> contractArrayList = new ArrayList<>();
    private final GoodsContractAdapter contractAdapter = new GoodsContractAdapter(contractArrayList);
    @Autowired(name = Constant.DATA_POSITION)
    int position = 1;
    private Toolbar mainToolbar = null;
    private AppCompatTextView leftTextView = null;
    private AppCompatTextView rightTextView = null;
    private AppCompatTextView screenOrderTextView = null;
    private AppCompatTextView screenSaleTextView = null;
    private AppCompatTextView screenScreenTextView = null;
    private AppCompatImageView screenOrientationImageView = null;
    private AppCompatTextView screenNightTextView = null;
    private LinearLayoutCompat screenOrderLinearLayout = null;
    private AppCompatTextView screenOrderSynthesizeTextView = null;
    private AppCompatTextView screenOrderHighTextView = null;
    private AppCompatTextView screenOrderLowTextView = null;
    private AppCompatTextView screenOrderPopularityTextView = null;
    private LinearLayoutCompat screenConditionLinearLayout = null;
    private AppCompatEditText screenPriceFromEditText = null;
    private AppCompatEditText screenPriceToEditText = null;
    private AppCompatSpinner screenAreaSpinner = null;
    private AppCompatTextView screenGiftTextView = null;
    private AppCompatTextView screenGroupbuyTextView = null;
    private AppCompatTextView screenXianshiTextView = null;
    private AppCompatTextView screenVirtualTextView = null;
    private AppCompatTextView screenOwnTextView = null;
    private RecyclerView screenContractRecyclerView = null;
    private AppCompatTextView screenConfirmTextView = null;
    private AppCompatTextView screenResetTextView = null;
    private PullRefreshView mainPullRefreshView = null;
    private GoodsRobbuyVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_goods_robbuy);
        mainToolbar = findViewById(R.id.mainToolbar);
        leftTextView = findViewById(R.id.leftTextView);
        rightTextView = findViewById(R.id.rightTextView);
        screenOrderTextView = findViewById(R.id.screenOrderTextView);
        screenSaleTextView = findViewById(R.id.screenSaleTextView);
        screenScreenTextView = findViewById(R.id.screenScreenTextView);
        screenOrientationImageView = findViewById(R.id.screenOrientationImageView);
        screenNightTextView = findViewById(R.id.screenNightTextView);
        screenOrderLinearLayout = findViewById(R.id.screenOrderLinearLayout);
        screenOrderSynthesizeTextView = findViewById(R.id.screenOrderSynthesizeTextView);
        screenOrderHighTextView = findViewById(R.id.screenOrderHighTextView);
        screenOrderLowTextView = findViewById(R.id.screenOrderLowTextView);
        screenOrderPopularityTextView = findViewById(R.id.screenOrderPopularityTextView);
        screenConditionLinearLayout = findViewById(R.id.screenConditionLinearLayout);
        screenPriceFromEditText = findViewById(R.id.screenPriceFromEditText);
        screenPriceToEditText = findViewById(R.id.screenPriceToEditText);
        screenAreaSpinner = findViewById(R.id.screenAreaSpinner);
        screenGiftTextView = findViewById(R.id.screenGiftTextView);
        screenGroupbuyTextView = findViewById(R.id.screenGroupbuyTextView);
        screenXianshiTextView = findViewById(R.id.screenXianshiTextView);
        screenVirtualTextView = findViewById(R.id.screenVirtualTextView);
        screenOwnTextView = findViewById(R.id.screenOwnTextView);
        screenContractRecyclerView = findViewById(R.id.screenContractRecyclerView);
        screenConfirmTextView = findViewById(R.id.screenConfirmTextView);
        screenResetTextView = findViewById(R.id.screenResetTextView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainRelativeLayout);
        leftTextView.setText(R.string.timeLimitDiscount);
        rightTextView.setText(R.string.timeLimitGroup);
        mainPullRefreshView.setItemDecoration(new LineDecoration(1, false));
        screenScreenTextView.setEnabled(false);
        screenOrientationImageView.setVisibility(View.GONE);
        data.setKey("");
        data.setOrder("");
        data.setPage("1");
        vm = getVM(GoodsRobbuyVM.class);
        vm.getSearchShow();
        if (position == 1) clickLeft();
        if (position == 2) clickRight();

    }

    @Override
    public void initEvent() {

        leftTextView.setOnClickListener(view -> clickLeft());

        rightTextView.setOnClickListener(view -> clickRight());

        screenOrderTextView.setOnClickListener(view -> {
            screenConditionLinearLayout.setVisibility(View.GONE);
            screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            if (screenOrderLinearLayout.getVisibility() == View.VISIBLE) {
                screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
                goneScreenCondition();
            } else {
                screenNightTextView.setVisibility(View.VISIBLE);
                screenOrderLinearLayout.setVisibility(View.VISIBLE);
                screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_up, 0);
                AnimUtil.objectAnimator(screenOrderLinearLayout, AnimUtil.TRABSLATION_Y, -screenOrderLinearLayout.getHeight(), 0);
                AnimUtil.objectAnimator(screenNightTextView, AnimUtil.ALPHA, 0, 1.0f);
            }
        });

        screenSaleTextView.setOnClickListener(view -> order(2, "1", "2"));

        screenScreenTextView.setOnClickListener(view -> {
            screenOrderLinearLayout.setVisibility(View.GONE);
            screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            if (screenConditionLinearLayout.getVisibility() == View.VISIBLE) {
                screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
                goneScreenCondition();
            } else {
                screenNightTextView.setVisibility(View.VISIBLE);
                screenConditionLinearLayout.setVisibility(View.VISIBLE);
                screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_up, 0);
                AnimUtil.objectAnimator(screenConditionLinearLayout, AnimUtil.TRABSLATION_X, App.get().getWidth(), 0);
                AnimUtil.objectAnimator(screenNightTextView, AnimUtil.ALPHA, 0, 1.0f);
            }
        });

        screenNightTextView.setOnClickListener(view -> goneScreenCondition());

        screenOrderSynthesizeTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.synthesizeSort);
            order(1, "", "");
        });

        screenOrderHighTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.priceHighToLow);
            order(1, "3", "2");
        });

        screenOrderLowTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.priceLowToHigh);
            order(1, "3", "1");
        });

        screenOrderPopularityTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.accent));
            screenOrderTextView.setText(R.string.popularitySort);
            order(1, "2", "2");
        });

        screenGiftTextView.setOnClickListener(view -> {
            if (VerifyUtil.isEmpty(data.getGift())) {
                data.setGift("1");
                screenGiftTextView.setTextColor(App.get().getColors(R.color.white));
                screenGiftTextView.setBackgroundResource(R.drawable.selector_accent_4dp);
            } else {
                data.setGift("");
                screenGiftTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenGiftTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
            }
        });

        screenGroupbuyTextView.setOnClickListener(view -> {
            if (VerifyUtil.isEmpty(data.getRobbuy())) {
                data.setRobbuy("1");
                screenGroupbuyTextView.setTextColor(App.get().getColors(R.color.white));
                screenGroupbuyTextView.setBackgroundResource(R.drawable.selector_accent_4dp);
            } else {
                data.setRobbuy("");
                screenGroupbuyTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenGroupbuyTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
            }
        });

        screenXianshiTextView.setOnClickListener(view -> {
            if (VerifyUtil.isEmpty(data.getXianshi())) {
                data.setXianshi("1");
                screenXianshiTextView.setTextColor(App.get().getColors(R.color.white));
                screenXianshiTextView.setBackgroundResource(R.drawable.selector_accent_4dp);
            } else {
                data.setXianshi("");
                screenXianshiTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenXianshiTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
            }
        });

        screenVirtualTextView.setOnClickListener(view -> {
            if (VerifyUtil.isEmpty(data.getVirtual())) {
                data.setVirtual("1");
                screenVirtualTextView.setTextColor(App.get().getColors(R.color.white));
                screenVirtualTextView.setBackgroundResource(R.drawable.selector_accent_4dp);
            } else {
                data.setVirtual("");
                screenVirtualTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenVirtualTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
            }
        });

        screenOwnTextView.setOnClickListener(view -> {
            if (VerifyUtil.isEmpty(data.getOwnShop())) {
                data.setOwnShop("1");
                screenOwnTextView.setTextColor(App.get().getColors(R.color.white));
                screenOwnTextView.setBackgroundResource(R.drawable.selector_accent_4dp);
            } else {
                data.setOwnShop("");
                screenOwnTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOwnTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
            }
        });

        screenConfirmTextView.setOnClickListener(view -> {
            screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
            screenOrderTextView.setText(R.string.synthesizeSort);
            order(1, "", "");
        });

        screenResetTextView.setOnClickListener(view -> {
            ToastHelp.get().show(R.string.success);
            clearScreenCondition();
        });

        mainPullRefreshView.setOnClickListener(view -> {
            if (position == 1) {
                if (mainPullRefreshView.isError() || xianshiArrayList.size() == 0) {
                    data.setPage("1");
                    getData();
                }
            } else {
                if (mainPullRefreshView.isError() || robbuyArrayList.size() == 0) {
                    data.setPage("1");
                    getData();
                }
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                data.setPage("1");
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

        xianshiAdapter.setOnItemClickListener((position, bean) -> App.get().startGoods(new GoodsData(bean.getGoodsId())));

        robbuyAdapter.setOnItemClickListener((position, bean) -> App.get().startGoods(new GoodsData(bean.getGoodsId())));

    }

    @Override
    public void initObserve() {

        vm.getSearchShowLiveData().observe(this, bean -> {
            areaId.clear();
            areaName.clear();
            areaId.add("");
            areaName.add("不限");
            for (int i = 0; i < bean.getAreaList().size(); i++) {
                areaId.add(bean.getAreaList().get(i).getAreaId());
                areaName.add(bean.getAreaList().get(i).getAreaName());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(get(), R.layout.spinner_simple_small, areaName);
            adapter.setDropDownViewResource(R.layout.spinner_item_small);
            screenAreaSpinner.setAdapter(adapter);
            screenAreaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    data.setAreaId(areaId.get(position));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            screenScreenTextView.setEnabled(true);
            contractArrayList.clear();
            contractArrayList.addAll(bean.getContractList());
            contractAdapter.notifyDataSetChanged();
            App.get().setRecyclerView(screenContractRecyclerView, contractAdapter);
            screenContractRecyclerView.addItemDecoration(new GridDecoration(3, App.get().dp2Px(8), false));
            screenContractRecyclerView.setLayoutManager(new GridLayoutManager(get(), 3));
            contractAdapter.notifyDataSetChanged();
            contractAdapter.setOnItemClickListener((position, bean1) -> {
                String value = bean1.getId() + "_";
                if (bean1.isSelect()) {
                    data.setCi(data.getCi().replace(value, ""));
                    contractArrayList.get(position).setSelect(false);
                } else {
                    data.setCi(data.getCi() + value);
                    contractArrayList.get(position).setSelect(true);
                }
                contractAdapter.notifyDataSetChanged();
            });
        });

        vm.getXianshiLiveData().observe(this, list -> {
            if (data.getPage().equals("1")) xianshiArrayList.clear();
            data.setPage((ConvertUtil.string2Int(data.getPage()) + 1) + "");
            xianshiArrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getRobbuyLiveData().observe(this, list -> {
            if (data.getPage().equals("1")) robbuyArrayList.clear();
            data.setPage((ConvertUtil.string2Int(data.getPage()) + 1) + "");
            robbuyArrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getSearchShow());
                return;
            }
            if (bean.getCode() == 2) {
                if (xianshiArrayList.size() == 0) {
                    mainPullRefreshView.setError(bean.getReason());
                } else {
                    ToastHelp.get().show(bean.getReason());
                }
                return;
            }
            if (bean.getCode() == 3) {
                if (robbuyArrayList.size() == 0) {
                    mainPullRefreshView.setError(bean.getReason());
                } else {
                    ToastHelp.get().show(bean.getReason());
                }
            }
        });

    }

    @Override
    public void onReturn(boolean handler) {

        if (screenNightTextView.getVisibility() == View.VISIBLE) {
            goneScreenCondition();
            return;
        }
        if (handler && isShowKeyboard()) {
            hideKeyboard();
            return;
        }
        finish();

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        if (position == 1) vm.getXianshi(data);
        if (position == 2) vm.getRobbuy(data);

    }

    private void clickLeft() {

        leftTextView.setTextColor(App.get().getColors(R.color.primary));
        leftTextView.setBackgroundResource(R.drawable.selector_switch_left_press);
        rightTextView.setTextColor(App.get().getColors(R.color.accent));
        rightTextView.setBackgroundResource(R.drawable.selector_switch_right);
        mainPullRefreshView.setAdapter(xianshiAdapter);
        mainPullRefreshView.setComplete();
        clearScreenCondition();
        data.setPage("1");
        position = 1;
        getData();

    }

    private void clickRight() {

        leftTextView.setTextColor(App.get().getColors(R.color.accent));
        leftTextView.setBackgroundResource(R.drawable.selector_switch_left);
        rightTextView.setTextColor(App.get().getColors(R.color.primary));
        rightTextView.setBackgroundResource(R.drawable.selector_switch_right_press);
        mainPullRefreshView.setAdapter(robbuyAdapter);
        mainPullRefreshView.setComplete();
        clearScreenCondition();
        data.setPage("1");
        position = 2;
        getData();

    }

    private void goneScreenCondition() {

        if (screenNightTextView.getVisibility() == View.VISIBLE) {
            AnimUtil.objectAnimator(screenNightTextView, AnimUtil.ALPHA, () -> screenNightTextView.setVisibility(View.GONE), 1.0f, 0);
        }
        if (screenOrderLinearLayout.getVisibility() == View.VISIBLE) {
            screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            AnimUtil.objectAnimator(screenOrderLinearLayout, AnimUtil.TRABSLATION_Y, () -> screenOrderLinearLayout.setVisibility(View.GONE), 0, -screenOrderLinearLayout.getHeight());
        }
        if (screenConditionLinearLayout.getVisibility() == View.VISIBLE) {
            screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
            AnimUtil.objectAnimator(screenConditionLinearLayout, AnimUtil.TRABSLATION_X, () -> screenConditionLinearLayout.setVisibility(View.GONE), 0, App.get().getWidth());
        }

    }

    private void clearScreenCondition() {

        data.setBrandId("");
        data.setGcId("");
        data.setPriceFrom("");
        data.setPriceTo("");
        data.setAreaId("");
        data.setGift("");
        data.setRobbuy("");
        data.setXianshi("");
        data.setVirtual("");
        data.setOwnShop("");
        data.setCi("");
        screenPriceFromEditText.setText("");
        screenPriceToEditText.setText("");
        screenAreaSpinner.setSelection(0);
        screenGiftTextView.setTextColor(App.get().getColors(R.color.textTwo));
        screenGiftTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
        screenGroupbuyTextView.setTextColor(App.get().getColors(R.color.textTwo));
        screenGroupbuyTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
        screenXianshiTextView.setTextColor(App.get().getColors(R.color.textTwo));
        screenXianshiTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
        screenVirtualTextView.setTextColor(App.get().getColors(R.color.textTwo));
        screenVirtualTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
        screenOwnTextView.setTextColor(App.get().getColors(R.color.textTwo));
        screenOwnTextView.setBackgroundResource(R.drawable.selector_primary_dark_4dp);
        screenScreenTextView.setTextColor(App.get().getColors(R.color.textTwo));

    }

    private void order(int type, String key, String order) {

        hideKeyboard();
        goneScreenCondition();
        data.setPage("1");
        data.setKey(key);
        data.setOrder(order);
        data.setPriceTo(Objects.requireNonNull(screenPriceToEditText.getText()).toString());
        data.setPriceFrom(Objects.requireNonNull(screenPriceFromEditText.getText()).toString());
        screenOrderTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
        screenScreenTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_down, 0);
        switch (type) {
            case 1:
                screenOrderTextView.setTextColor(App.get().getColors(R.color.accent));
                screenSaleTextView.setTextColor(App.get().getColors(R.color.textTwo));
                break;
            case 2:
                screenOrderTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenSaleTextView.setTextColor(App.get().getColors(R.color.accent));
                screenOrderSynthesizeTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOrderHighTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOrderLowTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOrderPopularityTextView.setTextColor(App.get().getColors(R.color.textTwo));
                screenOrderTextView.setText(R.string.synthesizeSort);
                break;
        }
        if (!VerifyUtil.isEmpty(data.getPriceFrom()) || !VerifyUtil.isEmpty(data.getPriceTo())
                || !VerifyUtil.isEmpty(data.getAreaId()) || !VerifyUtil.isEmpty(data.getGift())
                || !VerifyUtil.isEmpty(data.getRobbuy()) || !VerifyUtil.isEmpty(data.getXianshi())
                || !VerifyUtil.isEmpty(data.getVirtual()) || !VerifyUtil.isEmpty(data.getOwnShop())
                || !VerifyUtil.isEmpty(data.getCi())) {
            screenScreenTextView.setTextColor(App.get().getColors(R.color.accentDark));
        } else {
            screenScreenTextView.setTextColor(App.get().getColors(R.color.textTwo));
        }
        getData();

    }

}
