package top.yokey.shopai.goods.activity;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.goods.adapter.GoodsPinGouAdapter;
import top.yokey.shopai.goods.adapter.GoodsPinGouCateAdapter;
import top.yokey.shopai.goods.viewmodel.GoodsPinGouVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.GoodsClassBean;
import top.yokey.shopai.zsdk.bean.GoodsPinGouListBean;
import top.yokey.shopai.zsdk.data.GoodsData;

@Route(path = ARoutePath.GOODS_PIN_GOU)
public class GoodsPinGouActivity extends BaseActivity {

    private final ArrayList<GoodsClassBean> cateArrayList = new ArrayList<>();
    private final ArrayList<GoodsPinGouListBean> goodsArrayList = new ArrayList<>();
    private final GoodsPinGouAdapter goodsAdapter = new GoodsPinGouAdapter(goodsArrayList);
    private final GoodsPinGouCateAdapter cateAdapter = new GoodsPinGouCateAdapter(cateArrayList);
    private Toolbar mainToolbar = null;
    private RecyclerView cateRecyclerView = null;
    private PullRefreshView mainPullRefreshView = null;
    private int page = 1;
    private String gcId = "";
    private GoodsPinGouVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_goods_pin_gou);
        mainToolbar = findViewById(R.id.mainToolbar);
        cateRecyclerView = findViewById(R.id.cateRecyclerView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.pinGouActivity);
        observeKeyborad(R.id.mainLinearLayout);
        App.get().setRecyclerView(cateRecyclerView, cateAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(get());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        cateRecyclerView.setLayoutManager(linearLayoutManager);
        mainPullRefreshView.setItemDecoration(new LineDecoration(App.get().dp2Px(16), false));
        mainPullRefreshView.setAdapter(goodsAdapter);
        mainPullRefreshView.setLoad();
        vm = getVM(GoodsPinGouVM.class);
        vm.getCate();

    }

    @Override
    public void initEvent() {

        cateAdapter.setOnItemClickListener((position, bean) -> {
            for (int i = 0; i < cateArrayList.size(); i++) {
                cateArrayList.get(i).setSelect(false);
            }
            cateArrayList.get(position).setSelect(true);
            cateAdapter.notifyDataSetChanged();
            gcId = bean.getGcId();
            page = 1;
            getData();
        });

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || goodsArrayList.size() == 0) {
                page = 1;
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

        goodsAdapter.setOnItemClickListener((position, bean) -> App.get().startGoods(new GoodsData(bean.getGoodsId())));

    }

    @Override
    public void initObserve() {

        vm.getCateLiveData().observe(this, list -> {
            cateArrayList.clear();
            GoodsClassBean goodsClassBean = new GoodsClassBean();
            goodsClassBean.setGcId("0");
            goodsClassBean.setGcName("全部");
            goodsClassBean.setSelect(true);
            list.add(0, goodsClassBean);
            cateArrayList.addAll(list);
            cateAdapter.notifyDataSetChanged();
            gcId = "";
            getData();
        });

        vm.getGoodsLiveData().observe(this, list -> {
            if (page == 1) goodsArrayList.clear();
            page = page + 1;
            goodsArrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getCate());
                return;
            }
            if (bean.getCode() == 2) {
                if (goodsArrayList.size() == 0) {
                    mainPullRefreshView.setError(bean.getReason());
                } else {
                    ToastHelp.get().show(bean.getReason());
                }
            }
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getGoods(gcId, page + "");

    }

}
