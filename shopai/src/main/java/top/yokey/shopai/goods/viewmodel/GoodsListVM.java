package top.yokey.shopai.goods.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.GoodsListBean;
import top.yokey.shopai.zsdk.bean.SearchShowBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.GoodsController;
import top.yokey.shopai.zsdk.controller.IndexController;
import top.yokey.shopai.zsdk.controller.MemberCartController;
import top.yokey.shopai.zsdk.controller.MemberFavoritesController;
import top.yokey.shopai.zsdk.data.GoodsSearchData;

public class GoodsListVM extends BaseViewModel {

    private final MutableLiveData<SearchShowBean> searchShowLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<GoodsListBean>> goodsLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> addCartLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> favoriteLiveData = new MutableLiveData<>();

    public GoodsListVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<SearchShowBean> getSearchShowLiveData() {

        return searchShowLiveData;

    }

    public MutableLiveData<ArrayList<GoodsListBean>> getGoodsLiveData() {

        return goodsLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public MutableLiveData<String> getAddCartLiveData() {

        return addCartLiveData;

    }

    public MutableLiveData<String> getFavoriteLiveData() {

        return favoriteLiveData;

    }

    public void getSearchShow() {

        IndexController.searchShow(new HttpCallBack<SearchShowBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, SearchShowBean bean) {
                searchShowLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getList(GoodsSearchData data) {

        GoodsController.goodsList(data, new HttpCallBack<ArrayList<GoodsListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<GoodsListBean> list) {
                goodsLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void addCart(String goodsId, String quantity) {

        MemberCartController.cartAdd(goodsId, quantity, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                addCartLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

    public void favorite(String goodsId) {

        MemberFavoritesController.favoritesAdd(goodsId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                favoriteLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(4, reason));
            }
        });

    }

}
