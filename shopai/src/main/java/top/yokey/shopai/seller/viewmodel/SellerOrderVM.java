package top.yokey.shopai.seller.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.OrderSellerListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.SellerOrderController;

public class SellerOrderVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<OrderSellerListBean>> orderLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> cancelLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> modifyLiveData = new MutableLiveData<>();

    public SellerOrderVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<OrderSellerListBean>> getOrderLiveData() {

        return orderLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public MutableLiveData<String> getCancelLiveData() {

        return cancelLiveData;

    }

    public MutableLiveData<String> getModifyLiveData() {

        return modifyLiveData;

    }

    public void getOrderList(String type, String keyword, String page) {

        SellerOrderController.orderList(type, keyword, page, new HttpCallBack<ArrayList<OrderSellerListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<OrderSellerListBean> list) {
                orderLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void orderCancel(String orderId, String reason) {

        SellerOrderController.orderCancel(orderId, reason, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                cancelLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void modifyPrice(String orderId, String price) {

        SellerOrderController.orderSPayPrice(orderId, price, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                modifyLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
