package top.yokey.shopai.seller.activity;

import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.seller.viewmodel.SellerIndexVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.SharedHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.ShopAISdk;

@Route(path = ARoutePath.SELLER_INDEX)
public class SellerIndexActivity extends BaseActivity {

    private final RelativeLayout[] orderRelativeLayout = new RelativeLayout[4];
    private final AppCompatTextView[] orderNumTextView = new AppCompatTextView[4];
    private final AppCompatTextView[] goodsTextView = new AppCompatTextView[4];
    private final AppCompatTextView[] staticsTextView = new AppCompatTextView[5];
    private final AppCompatTextView[] functionTextView = new AppCompatTextView[4];
    private AppCompatImageView messageImageView = null;
    private AppCompatTextView messageDotTextView = null;
    private AppCompatImageView avatarImageView = null;
    private AppCompatTextView nameTextView = null;
    private AppCompatTextView levelTextView = null;
    private AppCompatTextView yesterdayTextView = null;
    private AppCompatTextView monthTextView = null;
    private AppCompatTextView numberTextView = null;
    private AppCompatTextView orderTextView = null;
    private AppCompatTextView logoutTextView = null;

    private SellerIndexVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_seller_index);
        messageImageView = findViewById(R.id.messageImageView);
        messageDotTextView = findViewById(R.id.messageDotTextView);
        avatarImageView = findViewById(R.id.avatarImageView);
        nameTextView = findViewById(R.id.nameTextView);
        levelTextView = findViewById(R.id.levelTextView);
        yesterdayTextView = findViewById(R.id.yesterdayTextView);
        monthTextView = findViewById(R.id.monthTextView);
        numberTextView = findViewById(R.id.numberTextView);
        orderTextView = findViewById(R.id.orderTextView);
        orderRelativeLayout[0] = findViewById(R.id.waitPayRelativeLayout);
        orderRelativeLayout[1] = findViewById(R.id.waitDeliverRelativeLayout);
        orderRelativeLayout[2] = findViewById(R.id.waitReceiveRelativeLayout);
        orderRelativeLayout[3] = findViewById(R.id.completeRelativeLayout);
        orderNumTextView[0] = findViewById(R.id.waitPayNumTextView);
        orderNumTextView[1] = findViewById(R.id.waitDeliverNumTextView);
        orderNumTextView[2] = findViewById(R.id.waitReceiveNumTextView);
        orderNumTextView[3] = findViewById(R.id.completeNumTextView);
        goodsTextView[0] = findViewById(R.id.saleTextView);
        goodsTextView[1] = findViewById(R.id.wareTextView);
        goodsTextView[2] = findViewById(R.id.violationTextView);
        goodsTextView[3] = findViewById(R.id.releaseTextView);
        staticsTextView[0] = findViewById(R.id.summaryTextView);
        staticsTextView[1] = findViewById(R.id.staticsTextView);
        staticsTextView[2] = findViewById(R.id.flowTextView);
        staticsTextView[3] = findViewById(R.id.flowGoodsTextView);
        staticsTextView[4] = findViewById(R.id.rankTextView);
        functionTextView[0] = findViewById(R.id.imTextView);
        functionTextView[1] = findViewById(R.id.logisticsTextView);
        functionTextView[2] = findViewById(R.id.addressTextView);
        functionTextView[3] = findViewById(R.id.settingTextView);
        logoutTextView = findViewById(R.id.logoutTextView);

    }

    @Override
    public void initData() {

        observeKeyborad(R.id.mainScrollView);
        vm = getVM(SellerIndexVM.class);

    }

    @Override
    public void initEvent() {

        View.OnClickListener listener = view -> App.get().start(ARoutePath.SELLER_MESSAGE);

        messageImageView.setOnClickListener(listener);

        orderTextView.setOnClickListener(view -> App.get().start(ARoutePath.SELLER_ORDER, Constant.DATA_POSITION, 0));

        orderRelativeLayout[0].setOnClickListener(view -> App.get().start(ARoutePath.SELLER_ORDER, Constant.DATA_POSITION, 1));

        orderRelativeLayout[1].setOnClickListener(view -> App.get().start(ARoutePath.SELLER_ORDER, Constant.DATA_POSITION, 2));

        orderRelativeLayout[2].setOnClickListener(view -> App.get().start(ARoutePath.SELLER_ORDER, Constant.DATA_POSITION, 3));

        orderRelativeLayout[3].setOnClickListener(view -> App.get().start(ARoutePath.SELLER_ORDER, Constant.DATA_POSITION, 4));

        goodsTextView[0].setOnClickListener(view -> {

        });

        goodsTextView[1].setOnClickListener(view -> {

        });

        goodsTextView[2].setOnClickListener(view -> {

        });

        goodsTextView[3].setOnClickListener(view -> {

        });

        staticsTextView[0].setOnClickListener(view -> {

        });

        staticsTextView[1].setOnClickListener(view -> {

        });

        staticsTextView[2].setOnClickListener(view -> {

        });

        staticsTextView[3].setOnClickListener(view -> {

        });

        staticsTextView[4].setOnClickListener(view -> {

        });

        functionTextView[0].setOnClickListener(listener);

        functionTextView[1].setOnClickListener(view -> App.get().start(ARoutePath.SELLER_EXPRESS));

        functionTextView[2].setOnClickListener(view -> App.get().start(ARoutePath.SELLER_ADDRESS));

        functionTextView[3].setOnClickListener(view -> App.get().start(ARoutePath.SELLER_SETTING));

        logoutTextView.setOnClickListener(view -> {
            logoutTextView.setEnabled(false);
            logoutTextView.setText(R.string.handlerIng);
            ShopAISdk.get().setKey("");
            ToastHelp.get().showSuccess();
            SharedHelp.get().putString(Constant.SHARED_MEMBER_KEY, "");
            SharedHelp.get().putString(Constant.SHARED_SELLER_KEY, "");
            onReturn(false);
        });

    }

    @Override
    public void initObserve() {

        vm.getIndexLiveData().observe(this, bean -> {
            App.get().setSellerBean(bean);
            ImageHelp.get().displayCircle(bean.getStoreInfo().getStoreAvatar(), avatarImageView);
            nameTextView.setText(bean.getSellerInfo().getSellerName());
            levelTextView.setText(bean.getStoreInfo().getGradeName());
            yesterdayTextView.setText(String.format(getString(R.string.sellerStaticsYesterday), bean.getStoreInfo().getDailySales().getOrdernum()));
            monthTextView.setText(String.format(getString(R.string.sellerStaticsMonth), bean.getStoreInfo().getMonthlySales().getOrdernum()));
            numberTextView.setText(String.format(getString(R.string.sellerStaticsGoods), bean.getStatics().getOnline()));
            if (VerifyUtil.isEmpty(bean.getSellerInfo().getOrderNopayCount()) || bean.getSellerInfo().getOrderNopayCount().equals("0")) {
                orderNumTextView[0].setVisibility(View.INVISIBLE);
            } else {
                orderNumTextView[0].setVisibility(View.VISIBLE);
                orderNumTextView[0].setText(bean.getSellerInfo().getOrderNopayCount().length() > 1 ? "+" : bean.getSellerInfo().getOrderNopayCount());
            }
            if (VerifyUtil.isEmpty(bean.getSellerInfo().getOrderNoreceiptCount()) || bean.getSellerInfo().getOrderNoreceiptCount().equals("0")) {
                orderNumTextView[1].setVisibility(View.INVISIBLE);
            } else {
                orderNumTextView[1].setVisibility(View.VISIBLE);
                orderNumTextView[1].setText(bean.getSellerInfo().getOrderNoreceiptCount().length() > 1 ? "+" : bean.getSellerInfo().getOrderNoreceiptCount());
            }
        });

        vm.getMsgCountLiveData().observe(this, string -> {
            messageDotTextView.setVisibility(string.equals("0") ? View.GONE : View.VISIBLE);
            messageDotTextView.setText(string);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                SharedHelp.get().putString(Constant.SHARED_SELLER_KEY, "");
                ToastHelp.get().show(bean.getReason());
                ShopAISdk.get().setKey("");
                App.get().startSellerLogin();
                onReturn(false);
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    @Override
    public void onResume() {

        super.onResume();
        ShopAISdk.get().setKey(App.get().getSellerKey());
        vm.index();
        vm.getMsgCount();

    }

    @Override
    public void onReturn(boolean handler) {

        ShopAISdk.get().setKey(App.get().getMemberKey());
        finish();

    }

}
