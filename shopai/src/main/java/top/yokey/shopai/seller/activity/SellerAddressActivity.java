package top.yokey.shopai.seller.activity;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.view.PullRefreshView;

@Route(path = ARoutePath.SELLER_ADDRESS)
public class SellerAddressActivity extends BaseActivity {

    private Toolbar mainToolbar = null;
    private AppCompatImageView toolbarImageView = null;
    private PullRefreshView mainPullRefreshView = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initObserve() {

    }

}
