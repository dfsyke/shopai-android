package top.yokey.shopai.distribu.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberFXIndexBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFXController;

public class DistribuIndexVM extends BaseViewModel {

    private final MutableLiveData<MemberFXIndexBean> memberLiveData = new MutableLiveData<>();

    public DistribuIndexVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<MemberFXIndexBean> getMemberLiveData() {

        return memberLiveData;

    }

    public void getIndex() {

        MemberFXController.index(new HttpCallBack<MemberFXIndexBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberFXIndexBean bean) {
                memberLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
