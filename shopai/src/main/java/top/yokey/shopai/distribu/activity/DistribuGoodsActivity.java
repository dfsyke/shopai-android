package top.yokey.shopai.distribu.activity;

import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.distribu.adapter.DistribuGoodsAdapter;
import top.yokey.shopai.distribu.viewmodel.DistribuGoodsVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.GoodsFXListBean;

@Route(path = ARoutePath.DISTRIBU_GOODS)
public class DistribuGoodsActivity extends BaseActivity {

    private final ArrayList<GoodsFXListBean> arrayList = new ArrayList<>();
    private final DistribuGoodsAdapter adapter = new DistribuGoodsAdapter(arrayList);
    private Toolbar mainToolbar = null;
    private PullRefreshView mainPullRefreshView = null;
    private int page = 1;
    private int position = -1;
    private DistribuGoodsVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.distribuGoods);
        observeKeyborad(R.id.mainLinearLayout);
        mainPullRefreshView.setItemDecoration(new LineDecoration(App.get().dp2Px(16), true));
        mainPullRefreshView.setAdapter(adapter);
        vm = getVM(DistribuGoodsVM.class);
        getData();

    }

    @Override
    public void initEvent() {

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                page = 1;
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

        adapter.setOnItemClickListener(new DistribuGoodsAdapter.onItemClickListener() {
            @Override
            public void onClick(int position, GoodsFXListBean bean) {

            }

            @Override
            public void onClickDrop(int position, GoodsFXListBean bean) {
                DistribuGoodsActivity.this.position = position;
                DialogHelp.get().query(get(), R.string.confirmSelection, R.string.delete, null, view -> vm.drop(bean.getFxId()));
            }

            @Override
            public void onClickShare(int position, GoodsFXListBean bean) {
                App.get().start(ARoutePath.DISTRIBU_SHARE, Constant.DATA_ID, bean.getGoodsCommonid());
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getGoodsLiveData().observe(this, list -> {
            if (page == 1) arrayList.clear();
            page++;
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getDropLiveData().observe(this, bean -> {
            arrayList.remove(position);
            mainPullRefreshView.setComplete();
            ToastHelp.get().showSuccess();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList.size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    mainPullRefreshView.setError(bean.getReason());
                }
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getGoods(page + "");

    }

}
