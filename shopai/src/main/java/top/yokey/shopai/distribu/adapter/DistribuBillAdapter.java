package top.yokey.shopai.distribu.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.DistribuBillListBean;

public class DistribuBillAdapter extends RecyclerView.Adapter<DistribuBillAdapter.ViewHolder> {

    private final ArrayList<DistribuBillListBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public DistribuBillAdapter(ArrayList<DistribuBillListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        DistribuBillListBean bean = arrayList.get(position);
        holder.snTextView.setText(String.format(App.get().getString(R.string.orderPaySn), bean.getOrderSn()));
        holder.descTextView.setText(bean.getBillStateTxt());
        ImageHelp.get().display(bean.getGoodsImageUrl(), holder.goodsImageView);
        holder.goodsTextView.setText(bean.getGoodsName());
        holder.infoTextView.setText(R.string.finishTime);
        holder.infoTextView.append("：");
        holder.infoTextView.append(bean.getFxPayTimeTxt());
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getPayGoodsAmount());
        holder.numberTextView.setText(R.string.payAmount);
        holder.totalTextView.setText(Html.fromHtml(App.get().handlerHtml(String.format(App.get().getString(R.string.htmlDistribuBillSettlement),
                bean.getRefundAmount(), bean.getFxCommisRate(), bean.getFxPayAmount()), "#FF0000")));

        holder.goodsRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickGoods(position, bean);
            }
        });

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_distribu_bill, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, DistribuBillListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickGoods(int position, DistribuBillListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatTextView snTextView;
        private final AppCompatTextView descTextView;
        private final RelativeLayout goodsRelativeLayout;
        private final AppCompatImageView goodsImageView;
        private final AppCompatTextView goodsTextView;
        private final AppCompatTextView infoTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView numberTextView;
        private final AppCompatTextView totalTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            snTextView = view.findViewById(R.id.snTextView);
            descTextView = view.findViewById(R.id.descTextView);
            goodsRelativeLayout = view.findViewById(R.id.goodsRelativeLayout);
            goodsImageView = view.findViewById(R.id.goodsImageView);
            goodsTextView = view.findViewById(R.id.goodsTextView);
            infoTextView = view.findViewById(R.id.infoTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            numberTextView = view.findViewById(R.id.numberTextView);
            totalTextView = view.findViewById(R.id.totalTextView);

        }

    }

}
