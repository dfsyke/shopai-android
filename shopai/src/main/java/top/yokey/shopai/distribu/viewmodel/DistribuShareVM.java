package top.yokey.shopai.distribu.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberFXAddBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFXController;

public class DistribuShareVM extends BaseViewModel {

    private final MutableLiveData<MemberFXAddBean> addLiveData = new MutableLiveData<>();

    public DistribuShareVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<MemberFXAddBean> getAddLiveData() {

        return addLiveData;

    }

    public void add(String id) {

        MemberFXController.fxAdd(id, new HttpCallBack<MemberFXAddBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberFXAddBean bean) {
                addLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
