package top.yokey.shopai.distribu.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.GoodsFXListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFXController;

public class DistribuGoodsVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<GoodsFXListBean>> goodsLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> dropLiveData = new MutableLiveData<>();

    public DistribuGoodsVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<GoodsFXListBean>> getGoodsLiveData() {

        return goodsLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public MutableLiveData<String> getDropLiveData() {

        return dropLiveData;

    }

    public void getGoods(String page) {

        MemberFXController.fxGoods(page, new HttpCallBack<ArrayList<GoodsFXListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<GoodsFXListBean> list) {
                hasMoreLiveData.setValue(baseBean.isHasmore());
                goodsLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void drop(String fxId) {

        MemberFXController.dropGoods(fxId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                dropLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
