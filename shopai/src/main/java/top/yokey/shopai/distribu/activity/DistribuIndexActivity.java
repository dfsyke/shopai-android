package top.yokey.shopai.distribu.activity;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.distribu.viewmodel.DistribuIndexVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;

@Route(path = ARoutePath.DISTRIBU_INDEX)
public class DistribuIndexActivity extends BaseActivity {

    private final AppCompatTextView[] functionTextView = new AppCompatTextView[8];
    private Toolbar mainToolbar = null;
    private AppCompatTextView amountTextView = null;
    private AppCompatTextView frozenTextView = null;
    private DistribuIndexVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_distribu_index);
        mainToolbar = findViewById(R.id.mainToolbar);
        amountTextView = findViewById(R.id.amountTextView);
        frozenTextView = findViewById(R.id.frozenTextView);
        functionTextView[0] = findViewById(R.id.goodsTextView);
        functionTextView[1] = findViewById(R.id.ordersTextView);
        functionTextView[2] = findViewById(R.id.settlementTextView);
        functionTextView[3] = findViewById(R.id.accountTextView);
        functionTextView[4] = findViewById(R.id.balanceTextView);
        functionTextView[5] = findViewById(R.id.recordTextView);
        functionTextView[6] = findViewById(R.id.cashTextView);
        functionTextView[7] = findViewById(R.id.addTextView);

    }

    @Override
    public void initData() {

        observeKeyborad(R.id.mainLinearLayout);
        setToolbar(mainToolbar, R.string.distribuCenter);
        vm = getVM(DistribuIndexVM.class);

    }

    @Override
    public void initEvent() {

        functionTextView[0].setOnClickListener(view -> App.get().start(ARoutePath.DISTRIBU_GOODS));

        functionTextView[1].setOnClickListener(view -> App.get().start(ARoutePath.DISTRIBU_ORDER));

        functionTextView[2].setOnClickListener(view -> App.get().start(ARoutePath.DISTRIBU_BILL));

        functionTextView[3].setOnClickListener(view -> App.get().start(ARoutePath.DISTRIBU_ACCOUNT));

        functionTextView[4].setOnClickListener(view -> App.get().start(ARoutePath.DISTRIBU_DEPOSIT));

        functionTextView[5].setOnClickListener(view -> App.get().start(ARoutePath.DISTRIBU_CASH_LOG));

        functionTextView[6].setOnClickListener(view -> App.get().start(ARoutePath.DISTRIBU_CASH_APPLY));

        functionTextView[7].setOnClickListener(view -> App.get().start(ARoutePath.DISTRIBU_ADD));

    }

    @Override
    public void initObserve() {

        vm.getMemberLiveData().observe(this, bean -> {
            if (!bean.getIsFxuser().equals(Constant.COMMON_ENABLE)) {
                ToastHelp.get().show(R.string.tipsDistribuNoPower);
                onReturn(false);
                return;
            }
            amountTextView.setText("￥");
            amountTextView.append(bean.getAvailableFxTrad());
            frozenTextView.setText("￥");
            frozenTextView.append(bean.getFreezeFxTrad());
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                ToastHelp.get().show(bean.getReason());
                onReturn(false);
            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();
        vm.getIndex();

    }

}
