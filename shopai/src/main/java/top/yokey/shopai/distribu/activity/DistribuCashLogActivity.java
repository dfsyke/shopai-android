package top.yokey.shopai.distribu.activity;

import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.distribu.adapter.DistribuCashAdapter;
import top.yokey.shopai.distribu.viewmodel.DistribuCashVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.MemberFXCashListBean;

@Route(path = ARoutePath.DISTRIBU_CASH_LOG)
public class DistribuCashLogActivity extends BaseActivity {

    private final ArrayList<MemberFXCashListBean> arrayList = new ArrayList<>();
    private final DistribuCashAdapter adapter = new DistribuCashAdapter(arrayList);
    private Toolbar mainToolbar = null;
    private PullRefreshView mainPullRefreshView = null;
    private int page = 1;
    private DistribuCashVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.cashRecord);
        observeKeyborad(R.id.mainLinearLayout);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setItemDecoration(new LineDecoration(1, App.get().getColors(R.color.divider), true));
        vm = getVM(DistribuCashVM.class);
        getLog();

    }

    @Override
    public void initEvent() {

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                getLog();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getLog();
            }

            @Override
            public void onLoadMore() {
                getLog();
            }
        });

        adapter.setOnItemClickListener((position, bean) -> App.get().start(ARoutePath.DISTRIBU_CASH_INFO, Constant.DATA_ID, bean.getTradcId()));

    }

    @Override
    public void initObserve() {

        vm.getLogLiveData().observe(this, list -> {
            if (page == 1) arrayList.clear();
            page = page + 1;
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList.size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    mainPullRefreshView.setError(bean.getReason());
                }
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    //自定义方法

    private void getLog() {

        mainPullRefreshView.setLoad();
        vm.getLog(page + "");

    }

}
