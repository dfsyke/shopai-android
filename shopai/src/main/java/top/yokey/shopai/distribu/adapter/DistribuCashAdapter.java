package top.yokey.shopai.distribu.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zsdk.bean.MemberFXCashListBean;

public class DistribuCashAdapter extends RecyclerView.Adapter<DistribuCashAdapter.ViewHolder> {

    private final ArrayList<MemberFXCashListBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public DistribuCashAdapter(ArrayList<MemberFXCashListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        MemberFXCashListBean bean = arrayList.get(position);
        holder.mainTextView.setText(String.format(App.get().getString(R.string.cashExamine), bean.getTradcPaymentStateTxt()));
        holder.snTextView.setText(String.format(App.get().getString(R.string.cashSn), bean.getTradcSn()));
        holder.timeTextView.setText(bean.getTradcAddTime());
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getTradcAmount());

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_distribu_cash, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, MemberFXCashListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatTextView mainTextView;
        private final AppCompatTextView snTextView;
        private final AppCompatTextView timeTextView;
        private final AppCompatTextView priceTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainTextView = view.findViewById(R.id.mainTextView);
            snTextView = view.findViewById(R.id.snTextView);
            timeTextView = view.findViewById(R.id.timeTextView);
            priceTextView = view.findViewById(R.id.priceTextView);

        }

    }

}
