package top.yokey.shopai.distribu.activity;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.distribu.viewmodel.DistribuCashVM;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.other.Constant;

@Route(path = ARoutePath.DISTRIBU_CASH_INFO)
public class DistribuCashInfoActivity extends BaseActivity {

    @Autowired(name = Constant.DATA_ID)
    String id = "";
    private Toolbar mainToolbar = null;
    private AppCompatTextView snTextView = null;
    private AppCompatTextView amountTextView = null;
    private AppCompatTextView bankTextView = null;
    private AppCompatTextView accountTextView = null;
    private AppCompatTextView payeeTextView = null;
    private AppCompatTextView timeTextView = null;
    private AppCompatTextView stateTextView = null;
    private AppCompatTextView finishTextView = null;
    private LinearLayoutCompat finishLinearLayout = null;
    private DistribuCashVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_distribu_cash_info);
        mainToolbar = findViewById(R.id.mainToolbar);
        snTextView = findViewById(R.id.snTextView);
        amountTextView = findViewById(R.id.amountTextView);
        bankTextView = findViewById(R.id.bankTextView);
        accountTextView = findViewById(R.id.accountTextView);
        payeeTextView = findViewById(R.id.payeeTextView);
        timeTextView = findViewById(R.id.timeTextView);
        stateTextView = findViewById(R.id.stateTextView);
        finishTextView = findViewById(R.id.finishTextView);
        finishLinearLayout = findViewById(R.id.finishLinearLayout);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.cashDetail);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(DistribuCashVM.class);
        vm.getInfo(id);

    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initObserve() {

        vm.getInfoLiveData().observe(this, bean -> {
            snTextView.setText(bean.getTradcSn());
            amountTextView.setText("￥");
            amountTextView.append(bean.getTradcAmount());
            bankTextView.setText(bean.getTradcBankName());
            accountTextView.setText(bean.getTradcBankNo());
            payeeTextView.setText(bean.getTradcBankUser());
            timeTextView.setText(bean.getTradcAddTimeText());
            stateTextView.setText(bean.getTradcPaymentStateTxt());
            if (bean.getTradcPaymentState().equals(Constant.COMMON_ENABLE)) {
                finishLinearLayout.setVisibility(View.VISIBLE);
                finishTextView.setText(bean.getTradcPaymentTimeText());
            }
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 4) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getInfo(id));
            }
        });

    }

}
