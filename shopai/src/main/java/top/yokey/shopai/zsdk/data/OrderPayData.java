package top.yokey.shopai.zsdk.data;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class OrderPayData implements Serializable {

    private String paySn = "";
    private String password = "";
    private String rcbPay = "0";
    private String pdPay = "0";
    private String paymentCode = "alipay";

    public String getPaySn() {
        return paySn;
    }

    public void setPaySn(String paySn) {
        this.paySn = paySn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRcbPay() {
        return rcbPay;
    }

    public void setRcbPay(String rcbPay) {
        this.rcbPay = rcbPay;
    }

    public String getPdPay() {
        return pdPay;
    }

    public void setPdPay(String pdPay) {
        this.pdPay = pdPay;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

}
