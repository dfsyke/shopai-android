package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class MemberFXCashListBean implements Serializable {

    @SerializedName("tradc_sn")
    private String tradcSn = "";
    @SerializedName("tradc_add_time")
    private String tradcAddTime = "";
    @SerializedName("tradc_amount")
    private String tradcAmount = "";
    @SerializedName("tradc_payment_state")
    private String tradcPaymentState = "";
    @SerializedName("tradc_payment_state_txt")
    private String tradcPaymentStateTxt = "";
    @SerializedName("tradc_id")
    private String tradcId = "";

    public String getTradcSn() {
        return tradcSn;
    }

    public void setTradcSn(String tradcSn) {
        this.tradcSn = tradcSn;
    }

    public String getTradcAddTime() {
        return tradcAddTime;
    }

    public void setTradcAddTime(String tradcAddTime) {
        this.tradcAddTime = tradcAddTime;
    }

    public String getTradcAmount() {
        return tradcAmount;
    }

    public void setTradcAmount(String tradcAmount) {
        this.tradcAmount = tradcAmount;
    }

    public String getTradcPaymentState() {
        return tradcPaymentState;
    }

    public void setTradcPaymentState(String tradcPaymentState) {
        this.tradcPaymentState = tradcPaymentState;
    }

    public String getTradcPaymentStateTxt() {
        return tradcPaymentStateTxt;
    }

    public void setTradcPaymentStateTxt(String tradcPaymentStateTxt) {
        this.tradcPaymentStateTxt = tradcPaymentStateTxt;
    }

    public String getTradcId() {
        return tradcId;
    }

    public void setTradcId(String tradcId) {
        this.tradcId = tradcId;
    }

}
