package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class RetreatRefundListBean implements Serializable {

    @SerializedName("refund_id")
    private String refundId = "";
    @SerializedName("order_id")
    private String orderId = "";
    @SerializedName("refund_amount")
    private String refundAmount = "";
    @SerializedName("refund_sn")
    private String refundSn = "";
    @SerializedName("order_sn")
    private String orderSn = "";
    @SerializedName("add_time")
    private String addTime = "";
    @SerializedName("seller_state_v")
    private String sellerStateV = "";
    @SerializedName("seller_state")
    private String sellerState = "";
    @SerializedName("admin_state_v")
    private String adminStateV = "";
    @SerializedName("admin_state")
    private String adminState = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("goods_list")
    private ArrayList<GoodsListBean> goodsList = new ArrayList<>();

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getRefundSn() {
        return refundSn;
    }

    public void setRefundSn(String refundSn) {
        this.refundSn = refundSn;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getSellerStateV() {
        return sellerStateV;
    }

    public void setSellerStateV(String sellerStateV) {
        this.sellerStateV = sellerStateV;
    }

    public String getSellerState() {
        return sellerState;
    }

    public void setSellerState(String sellerState) {
        this.sellerState = sellerState;
    }

    public String getAdminStateV() {
        return adminStateV;
    }

    public void setAdminStateV(String adminStateV) {
        this.adminStateV = adminStateV;
    }

    public String getAdminState() {
        return adminState;
    }

    public void setAdminState(String adminState) {
        this.adminState = adminState;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public ArrayList<GoodsListBean> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(ArrayList<GoodsListBean> goodsList) {
        this.goodsList = goodsList;
    }

    public static class GoodsListBean {

        @SerializedName("goods_id")
        private String goodsId = "";
        @SerializedName("goods_name")
        private String goodsName = "";
        @SerializedName("goods_spec")
        private String goodsSpec = "";
        @SerializedName("goods_img_360")
        private String goodsImg360 = "";

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public String getGoodsSpec() {
            return goodsSpec;
        }

        public void setGoodsSpec(String goodsSpec) {
            this.goodsSpec = goodsSpec;
        }

        public String getGoodsImg360() {
            return goodsImg360;
        }

        public void setGoodsImg360(String goodsImg360) {
            this.goodsImg360 = goodsImg360;
        }

    }

}
