package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberInviteBean;
import top.yokey.shopai.zsdk.bean.MemberInviteIncomeBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberInviteController {

    private static final String ACT = "member_invite";

    public static void index(HttpCallBack<MemberInviteBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "index")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "member_info");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, MemberInviteBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void inviteOne(String page, HttpCallBack<ArrayList<MemberInviteIncomeBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "inviteone")
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, MemberInviteIncomeBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void inviteTwo(String page, HttpCallBack<ArrayList<MemberInviteIncomeBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "invitetwo")
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, MemberInviteIncomeBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void inviteThr(String page, HttpCallBack<ArrayList<MemberInviteIncomeBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "invitethir")
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, MemberInviteIncomeBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
