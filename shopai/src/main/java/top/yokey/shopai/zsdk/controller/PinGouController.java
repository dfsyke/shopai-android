package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.GoodsPinGouListBean;
import top.yokey.shopai.zsdk.bean.OrderPinGouBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.OrderPinGouData;

@SuppressWarnings("ALL")
public class PinGouController {

    private static final String ACT = "pingou";

    public static void info(OrderPinGouData data, HttpCallBack<OrderPinGouBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "info")
                .add("pingou_id", data.getPinGouId())
                .add("buyer_id", data.getBuyerId())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "pingou_info");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, OrderPinGouBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void pingouList(String gcId, String page, HttpCallBack<ArrayList<GoodsPinGouListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "pingou_list")
                .add("gc_id", gcId)
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "goods_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, GoodsPinGouListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
