package top.yokey.shopai.zsdk.controller;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberAssetBean;
import top.yokey.shopai.zsdk.bean.MemberBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberIndexController {

    private static final String ACT = "member_index";

    public static void index(HttpCallBack<MemberBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "index")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "member_info");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, MemberBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void myAsset(HttpCallBack<MemberAssetBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "my_asset")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), MemberAssetBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
