package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.OrderBean;
import top.yokey.shopai.zsdk.bean.OrderDeliverBean;
import top.yokey.shopai.zsdk.bean.OrderInfoBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberOrderController {

    private static final String ACT = "member_order";

    public static void orderInfo(String orderId, HttpCallBack<OrderInfoBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "order_info")
                .add("order_id", orderId)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "order_info");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, OrderInfoBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void orderCancel(String orderId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "order_cancel")
                .add("order_id", orderId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void orderDelete(String orderId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "order_delete")
                .add("order_id", orderId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void orderReceive(String orderId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "order_receive")
                .add("order_id", orderId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void getCurrentDeliver(String orderId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_current_deliver")
                .add("order_id", orderId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void searchDeliver(String orderId, HttpCallBack<OrderDeliverBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "search_deliver")
                .add("order_id", orderId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), OrderDeliverBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void orderList(String stateType, String orderKey, String page, HttpCallBack<ArrayList<OrderBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "order_list")
                .add("state_type", stateType)
                .add("order_key", orderKey)
                .add("page", ShopAISdk.get().getPageNumber())
                .add("curpage", page)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "order_group_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, OrderBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
