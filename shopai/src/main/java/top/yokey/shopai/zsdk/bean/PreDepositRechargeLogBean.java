package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class PreDepositRechargeLogBean implements Serializable {

    @SerializedName("pdr_id")
    private String pdrId = "";
    @SerializedName("pdr_sn")
    private String pdrSn = "";
    @SerializedName("pdr_member_id")
    private String pdrMemberId = "";
    @SerializedName("pdr_member_name")
    private String pdrMemberName = "";
    @SerializedName("pdr_amount")
    private String pdrAmount = "";
    @SerializedName("pdr_payment_code")
    private String pdrPaymentCode = "";
    @SerializedName("pdr_payment_name")
    private String pdrPaymentName = "";
    @SerializedName("pdr_trade_sn")
    private String pdrTradeSn = "";
    @SerializedName("pdr_add_time")
    private String pdrAddTime = "";
    @SerializedName("pdr_payment_state")
    private String pdrPaymentState = "";
    @SerializedName("pdr_payment_time")
    private String pdrPaymentTime = "";
    @SerializedName("pdr_admin")
    private String pdrAdmin = "";
    @SerializedName("pdr_add_time_text")
    private String pdrAddTimeText = "";
    @SerializedName("pdr_payment_state_text")
    private String pdrPaymentStateText = "";

    public String getPdrId() {
        return pdrId;
    }

    public void setPdrId(String pdrId) {
        this.pdrId = pdrId;
    }

    public String getPdrSn() {
        return pdrSn;
    }

    public void setPdrSn(String pdrSn) {
        this.pdrSn = pdrSn;
    }

    public String getPdrMemberId() {
        return pdrMemberId;
    }

    public void setPdrMemberId(String pdrMemberId) {
        this.pdrMemberId = pdrMemberId;
    }

    public String getPdrMemberName() {
        return pdrMemberName;
    }

    public void setPdrMemberName(String pdrMemberName) {
        this.pdrMemberName = pdrMemberName;
    }

    public String getPdrAmount() {
        return pdrAmount;
    }

    public void setPdrAmount(String pdrAmount) {
        this.pdrAmount = pdrAmount;
    }

    public String getPdrPaymentCode() {
        return pdrPaymentCode;
    }

    public void setPdrPaymentCode(String pdrPaymentCode) {
        this.pdrPaymentCode = pdrPaymentCode;
    }

    public String getPdrPaymentName() {
        return pdrPaymentName;
    }

    public void setPdrPaymentName(String pdrPaymentName) {
        this.pdrPaymentName = pdrPaymentName;
    }

    public String getPdrTradeSn() {
        return pdrTradeSn;
    }

    public void setPdrTradeSn(String pdrTradeSn) {
        this.pdrTradeSn = pdrTradeSn;
    }

    public String getPdrAddTime() {
        return pdrAddTime;
    }

    public void setPdrAddTime(String pdrAddTime) {
        this.pdrAddTime = pdrAddTime;
    }

    public String getPdrPaymentState() {
        return pdrPaymentState;
    }

    public void setPdrPaymentState(String pdrPaymentState) {
        this.pdrPaymentState = pdrPaymentState;
    }

    public String getPdrPaymentTime() {
        return pdrPaymentTime;
    }

    public void setPdrPaymentTime(String pdrPaymentTime) {
        this.pdrPaymentTime = pdrPaymentTime;
    }

    public String getPdrAdmin() {
        return pdrAdmin;
    }

    public void setPdrAdmin(String pdrAdmin) {
        this.pdrAdmin = pdrAdmin;
    }

    public String getPdrAddTimeText() {
        return pdrAddTimeText;
    }

    public void setPdrAddTimeText(String pdrAddTimeText) {
        this.pdrAddTimeText = pdrAddTimeText;
    }

    public String getPdrPaymentStateText() {
        return pdrPaymentStateText;
    }

    public void setPdrPaymentStateText(String pdrPaymentStateText) {
        this.pdrPaymentStateText = pdrPaymentStateText;
    }

}
