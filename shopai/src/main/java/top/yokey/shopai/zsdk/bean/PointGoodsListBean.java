package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class PointGoodsListBean implements Serializable {

    @SerializedName("pgoods_id")
    private String pgoodsId;
    @SerializedName("pgoods_name")
    private String pgoodsName;
    @SerializedName("pgoods_price")
    private String pgoodsPrice;
    @SerializedName("pgoods_points")
    private String pgoodsPoints;
    @SerializedName("pgoods_image")
    private String pgoodsImage;
    @SerializedName("pgoods_tag")
    private String pgoodsTag;
    @SerializedName("pgoods_serial")
    private String pgoodsSerial;
    @SerializedName("pgoods_storage")
    private String pgoodsStorage;
    @SerializedName("pgoods_commend")
    private String pgoodsCommend;
    @SerializedName("pgoods_keywords")
    private String pgoodsKeywords;
    @SerializedName("pgoods_salenum")
    private String pgoodsSalenum;
    @SerializedName("pgoods_view")
    private String pgoodsView;
    @SerializedName("pgoods_limitnum")
    private String pgoodsLimitnum;
    @SerializedName("pgoods_image_old")
    private String pgoodsImageOld;
    @SerializedName("pgoods_image_small")
    private String pgoodsImageSmall;
    @SerializedName("ex_state")
    private String exState;

    public String getPgoodsId() {
        return pgoodsId;
    }

    public void setPgoodsId(String pgoodsId) {
        this.pgoodsId = pgoodsId;
    }

    public String getPgoodsName() {
        return pgoodsName;
    }

    public void setPgoodsName(String pgoodsName) {
        this.pgoodsName = pgoodsName;
    }

    public String getPgoodsPrice() {
        return pgoodsPrice;
    }

    public void setPgoodsPrice(String pgoodsPrice) {
        this.pgoodsPrice = pgoodsPrice;
    }

    public String getPgoodsPoints() {
        return pgoodsPoints;
    }

    public void setPgoodsPoints(String pgoodsPoints) {
        this.pgoodsPoints = pgoodsPoints;
    }

    public String getPgoodsImage() {
        return pgoodsImage;
    }

    public void setPgoodsImage(String pgoodsImage) {
        this.pgoodsImage = pgoodsImage;
    }

    public String getPgoodsTag() {
        return pgoodsTag;
    }

    public void setPgoodsTag(String pgoodsTag) {
        this.pgoodsTag = pgoodsTag;
    }

    public String getPgoodsSerial() {
        return pgoodsSerial;
    }

    public void setPgoodsSerial(String pgoodsSerial) {
        this.pgoodsSerial = pgoodsSerial;
    }

    public String getPgoodsStorage() {
        return pgoodsStorage;
    }

    public void setPgoodsStorage(String pgoodsStorage) {
        this.pgoodsStorage = pgoodsStorage;
    }

    public String getPgoodsCommend() {
        return pgoodsCommend;
    }

    public void setPgoodsCommend(String pgoodsCommend) {
        this.pgoodsCommend = pgoodsCommend;
    }

    public String getPgoodsKeywords() {
        return pgoodsKeywords;
    }

    public void setPgoodsKeywords(String pgoodsKeywords) {
        this.pgoodsKeywords = pgoodsKeywords;
    }

    public String getPgoodsSalenum() {
        return pgoodsSalenum;
    }

    public void setPgoodsSalenum(String pgoodsSalenum) {
        this.pgoodsSalenum = pgoodsSalenum;
    }

    public String getPgoodsView() {
        return pgoodsView;
    }

    public void setPgoodsView(String pgoodsView) {
        this.pgoodsView = pgoodsView;
    }

    public String getPgoodsLimitnum() {
        return pgoodsLimitnum;
    }

    public void setPgoodsLimitnum(String pgoodsLimitnum) {
        this.pgoodsLimitnum = pgoodsLimitnum;
    }

    public String getPgoodsImageOld() {
        return pgoodsImageOld;
    }

    public void setPgoodsImageOld(String pgoodsImageOld) {
        this.pgoodsImageOld = pgoodsImageOld;
    }

    public String getPgoodsImageSmall() {
        return pgoodsImageSmall;
    }

    public void setPgoodsImageSmall(String pgoodsImageSmall) {
        this.pgoodsImageSmall = pgoodsImageSmall;
    }

    public String getExState() {
        return exState;
    }

    public void setExState(String exState) {
        this.exState = exState;
    }

}
