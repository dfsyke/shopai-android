package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class RedPacketDetailBean implements Serializable {

    @SerializedName("id")
    private String id = "";
    @SerializedName("packet_name")
    private String packetName = "";
    @SerializedName("packet_number")
    private String packetNumber = "";
    @SerializedName("packet_numbered")
    private String packetNumbered = "";
    @SerializedName("packet_amount")
    private String packetAmount = "";
    @SerializedName("win_rate")
    private String winRate = "";
    @SerializedName("packet_descript")
    private String packetDescript = "";
    @SerializedName("valid_date")
    private String validDate = "";
    @SerializedName("valid_date2")
    private String validDate2 = "";
    @SerializedName("start_time")
    private String startTime = "";
    @SerializedName("end_time")
    private String endTime = "";
    @SerializedName("add_time")
    private String addTime = "";
    @SerializedName("state")
    private String state = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPacketName() {
        return packetName;
    }

    public void setPacketName(String packetName) {
        this.packetName = packetName;
    }

    public String getPacketNumber() {
        return packetNumber;
    }

    public void setPacketNumber(String packetNumber) {
        this.packetNumber = packetNumber;
    }

    public String getPacketNumbered() {
        return packetNumbered;
    }

    public void setPacketNumbered(String packetNumbered) {
        this.packetNumbered = packetNumbered;
    }

    public String getPacketAmount() {
        return packetAmount;
    }

    public void setPacketAmount(String packetAmount) {
        this.packetAmount = packetAmount;
    }

    public String getWinRate() {
        return winRate;
    }

    public void setWinRate(String winRate) {
        this.winRate = winRate;
    }

    public String getPacketDescript() {
        return packetDescript;
    }

    public void setPacketDescript(String packetDescript) {
        this.packetDescript = packetDescript;
    }

    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    public String getValidDate2() {
        return validDate2;
    }

    public void setValidDate2(String validDate2) {
        this.validDate2 = validDate2;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}
