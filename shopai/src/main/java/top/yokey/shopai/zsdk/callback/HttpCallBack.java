package top.yokey.shopai.zsdk.callback;

import top.yokey.shopai.zsdk.bean.BaseBean;

@SuppressWarnings("ALL")
public interface HttpCallBack<ResultType> {

    void onSuccess(String result, BaseBean baseBean, ResultType resultType);

    void onFailure(String reason);

}
