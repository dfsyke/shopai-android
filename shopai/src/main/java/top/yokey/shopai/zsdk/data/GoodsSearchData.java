package top.yokey.shopai.zsdk.data;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class GoodsSearchData implements Serializable {

    private String key = "";
    private String page = "1";
    private String order = "";
    private String gcId = "";
    private String stcId = "";
    private String brandId = "";
    private String storeId = "";
    private String keyword = "";
    private String priceFrom = "";
    private String priceTo = "";
    private String areaId = "";
    private String gift = "";
    private String robbuy = "";
    private String xianshi = "";
    private String virtual = "";
    private String ownShop = "";
    private String ci = "";

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getGcId() {
        return gcId;
    }

    public void setGcId(String gcId) {
        this.gcId = gcId;
    }

    public String getStcId() {
        return stcId;
    }

    public void setStcId(String stcId) {
        this.stcId = stcId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(String priceFrom) {
        this.priceFrom = priceFrom;
    }

    public String getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(String priceTo) {
        this.priceTo = priceTo;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getGift() {
        return gift;
    }

    public void setGift(String gift) {
        this.gift = gift;
    }

    public String getRobbuy() {
        return robbuy;
    }

    public void setRobbuy(String robbuy) {
        this.robbuy = robbuy;
    }

    public String getXianshi() {
        return xianshi;
    }

    public void setXianshi(String xianshi) {
        this.xianshi = xianshi;
    }

    public String getVirtual() {
        return virtual;
    }

    public void setVirtual(String virtual) {
        this.virtual = virtual;
    }

    public String getOwnShop() {
        return ownShop;
    }

    public void setOwnShop(String ownShop) {
        this.ownShop = ownShop;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

}
