package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class OrderPinGouBean implements Serializable {

    @SerializedName("pingou_end_time")
    private String pingouEndTime = "";
    @SerializedName("member_id")
    private String memberId = "";
    @SerializedName("order_id")
    private String orderId = "";
    @SerializedName("log_id")
    private String logId = "";
    @SerializedName("buyer_id")
    private String buyerId = "";
    @SerializedName("goods_id")
    private String goodsId = "";
    @SerializedName("min_num")
    private String minNum = "";
    @SerializedName("goods_name")
    private String goodsName = "";
    @SerializedName("pingou_price")
    private String pingouPrice = "";
    @SerializedName("goods_image_url")
    private String goodsImageUrl = "";
    @SerializedName("goods_price")
    private String goodsPrice = "";
    @SerializedName("num")
    private String num = "";
    @SerializedName("log_list")
    private ArrayList<LogListBean> logList = new ArrayList<>();

    public String getPingouEndTime() {
        return pingouEndTime;
    }

    public void setPingouEndTime(String pingouEndTime) {
        this.pingouEndTime = pingouEndTime;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getMinNum() {
        return minNum;
    }

    public void setMinNum(String minNum) {
        this.minNum = minNum;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getPingouPrice() {
        return pingouPrice;
    }

    public void setPingouPrice(String pingouPrice) {
        this.pingouPrice = pingouPrice;
    }

    public String getGoodsImageUrl() {
        return goodsImageUrl;
    }

    public void setGoodsImageUrl(String goodsImageUrl) {
        this.goodsImageUrl = goodsImageUrl;
    }

    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public ArrayList<LogListBean> getLogList() {
        return logList;
    }

    public void setLogList(ArrayList<LogListBean> logList) {
        this.logList = logList;
    }

    public static class LogListBean {

        @SerializedName("buyer_id")
        private String buyerId = "";
        @SerializedName("buyer_name")
        private String buyerName = "";
        @SerializedName("buyer_type")
        private String buyerType = "";
        @SerializedName("avatar")
        private String avatar = "";
        @SerializedName("time_text")
        private String timeText = "";
        @SerializedName("type_text")
        private String typeText = "";

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getBuyerName() {
            return buyerName;
        }

        public void setBuyerName(String buyerName) {
            this.buyerName = buyerName;
        }

        public String getBuyerType() {
            return buyerType;
        }

        public void setBuyerType(String buyerType) {
            this.buyerType = buyerType;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getTimeText() {
            return timeText;
        }

        public void setTimeText(String timeText) {
            this.timeText = timeText;
        }

        public String getTypeText() {
            return typeText;
        }

        public void setTypeText(String typeText) {
            this.typeText = typeText;
        }

    }

}
