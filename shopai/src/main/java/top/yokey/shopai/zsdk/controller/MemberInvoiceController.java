package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.InvoiceListBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.InvoiceAddData;

@SuppressWarnings("ALL")
public class MemberInvoiceController {

    private static final String ACT = "member_invoice";

    public static void invoiceList(HttpCallBack<ArrayList<InvoiceListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "invoice_list")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "invoice_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, InvoiceListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void invoiceContentList(HttpCallBack<ArrayList<String>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "invoice_content_list")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "invoice_content_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, String.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void invoiceAdd(InvoiceAddData data, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "invoice_add")
                .add("inv_title_select", data.getSelect())
                .add("inv_title", data.getTitle())
                .add("inv_content", data.getContent())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void invoiceDel(String invId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "invoice_del")
                .add("inv_id", invId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
