package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.AndroidConfigBean;
import top.yokey.shopai.zsdk.bean.AppAdvertBean;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.SpecialListBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class AppController {

    private static final String ACT = "app";

    public static void getAndroid(HttpCallBack<AndroidConfigBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_android")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "android_config");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, AndroidConfigBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void getSpecialList(HttpCallBack<ArrayList<SpecialListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_special_list")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "special_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, SpecialListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void getAppAdvert(HttpCallBack<AppAdvertBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_app_advert")
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "app_advert");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, AppAdvertBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
