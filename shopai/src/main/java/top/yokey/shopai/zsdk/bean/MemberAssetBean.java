package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class MemberAssetBean implements Serializable {

    @SerializedName("point")
    private String point;
    @SerializedName("predepoit")
    private String predepoit;
    @SerializedName("available_rc_balance")
    private String availableRcBalance;
    @SerializedName("coupon")
    private String coupon;
    @SerializedName("voucher")
    private String voucher;

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getPredepoit() {
        return predepoit;
    }

    public void setPredepoit(String predepoit) {
        this.predepoit = predepoit;
    }

    public String getAvailableRcBalance() {
        return availableRcBalance;
    }

    public void setAvailableRcBalance(String availableRcBalance) {
        this.availableRcBalance = availableRcBalance;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

}
