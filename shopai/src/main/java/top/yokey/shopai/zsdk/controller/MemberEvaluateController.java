package top.yokey.shopai.zsdk.controller;

import java.util.HashMap;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.OrderEvaluateAgainBean;
import top.yokey.shopai.zsdk.bean.OrderEvaluateBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberEvaluateController {

    private static final String ACT = "member_evaluate";

    public static void index(String orderId, HttpCallBack<OrderEvaluateBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "index")
                .add("order_id", orderId)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), OrderEvaluateBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void again(String orderId, HttpCallBack<OrderEvaluateAgainBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "again")
                .add("order_id", orderId)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), OrderEvaluateAgainBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void save(HashMap<String, String> hashMap, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "save")
                .add(hashMap)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void saveAgain(HashMap<String, String> hashMap, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "save_again")
                .add(hashMap)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
