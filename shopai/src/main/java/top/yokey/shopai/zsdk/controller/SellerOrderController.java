package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.OrderSellerListBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class SellerOrderController {

    private static final String ACT = "seller_order";

    public static void orderCancel(String orderId, String reason, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "order_cancel")
                .add("order_id", orderId)
                .add("reason", reason)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void orderSPayPrice(String orderId, String price, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "order_spay_price")
                .add("order_id", orderId)
                .add("order_fee", price)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void orderList(String type, String key, String page, HttpCallBack<ArrayList<OrderSellerListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "order_list")
                .add("state_type", type)
                .add("order_key", key)
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "order_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, OrderSellerListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
