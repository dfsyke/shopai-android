package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class VoucherBean implements Serializable {

    @SerializedName("voucher_id")
    private String voucherId = "";
    @SerializedName("voucher_code")
    private String voucherCode = "";
    @SerializedName("voucher_title")
    private String voucherTitle = "";
    @SerializedName("voucher_desc")
    private String voucherDesc = "";
    @SerializedName("voucher_start_date")
    private String voucherStartDate = "";
    @SerializedName("voucher_end_date")
    private String voucherEndDate = "";
    @SerializedName("voucher_price")
    private String voucherPrice = "";
    @SerializedName("voucher_limit")
    private String voucherLimit = "";
    @SerializedName("voucher_state")
    private String voucherState = "";
    @SerializedName("voucher_order_id")
    private String voucherOrderId = "";
    @SerializedName("voucher_store_id")
    private String voucherStoreId = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("store_domain")
    private String storeDomain = "";
    @SerializedName("store_label")
    private String storeLabel = "";
    @SerializedName("store_avatar")
    private String storeAvatar = "";
    @SerializedName("is_own_shop")
    private String isOwnShop = "";
    @SerializedName("voucher_t_customimg")
    private String voucherTCustomimg = "";
    @SerializedName("store_logo_url")
    private String storeLogoUrl = "";
    @SerializedName("store_avatar_url")
    private String storeAvatarUrl = "";
    @SerializedName("voucher_state_text")
    private String voucherStateText = "";
    @SerializedName("voucher_start_date_text")
    private String voucherStartDateText = "";
    @SerializedName("voucher_end_date_text")
    private String voucherEndDateText = "";

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherTitle() {
        return voucherTitle;
    }

    public void setVoucherTitle(String voucherTitle) {
        this.voucherTitle = voucherTitle;
    }

    public String getVoucherDesc() {
        return voucherDesc;
    }

    public void setVoucherDesc(String voucherDesc) {
        this.voucherDesc = voucherDesc;
    }

    public String getVoucherStartDate() {
        return voucherStartDate;
    }

    public void setVoucherStartDate(String voucherStartDate) {
        this.voucherStartDate = voucherStartDate;
    }

    public String getVoucherEndDate() {
        return voucherEndDate;
    }

    public void setVoucherEndDate(String voucherEndDate) {
        this.voucherEndDate = voucherEndDate;
    }

    public String getVoucherPrice() {
        return voucherPrice;
    }

    public void setVoucherPrice(String voucherPrice) {
        this.voucherPrice = voucherPrice;
    }

    public String getVoucherLimit() {
        return voucherLimit;
    }

    public void setVoucherLimit(String voucherLimit) {
        this.voucherLimit = voucherLimit;
    }

    public String getVoucherState() {
        return voucherState;
    }

    public void setVoucherState(String voucherState) {
        this.voucherState = voucherState;
    }

    public String getVoucherOrderId() {
        return voucherOrderId;
    }

    public void setVoucherOrderId(String voucherOrderId) {
        this.voucherOrderId = voucherOrderId;
    }

    public String getVoucherStoreId() {
        return voucherStoreId;
    }

    public void setVoucherStoreId(String voucherStoreId) {
        this.voucherStoreId = voucherStoreId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreDomain() {
        return storeDomain;
    }

    public void setStoreDomain(String storeDomain) {
        this.storeDomain = storeDomain;
    }

    public String getStoreLabel() {
        return storeLabel;
    }

    public void setStoreLabel(String storeLabel) {
        this.storeLabel = storeLabel;
    }

    public String getStoreAvatar() {
        return storeAvatar;
    }

    public void setStoreAvatar(String storeAvatar) {
        this.storeAvatar = storeAvatar;
    }

    public String getIsOwnShop() {
        return isOwnShop;
    }

    public void setIsOwnShop(String isOwnShop) {
        this.isOwnShop = isOwnShop;
    }

    public String getVoucherTCustomimg() {
        return voucherTCustomimg;
    }

    public void setVoucherTCustomimg(String voucherTCustomimg) {
        this.voucherTCustomimg = voucherTCustomimg;
    }

    public String getStoreLogoUrl() {
        return storeLogoUrl;
    }

    public void setStoreLogoUrl(String storeLogoUrl) {
        this.storeLogoUrl = storeLogoUrl;
    }

    public String getStoreAvatarUrl() {
        return storeAvatarUrl;
    }

    public void setStoreAvatarUrl(String storeAvatarUrl) {
        this.storeAvatarUrl = storeAvatarUrl;
    }

    public String getVoucherStateText() {
        return voucherStateText;
    }

    public void setVoucherStateText(String voucherStateText) {
        this.voucherStateText = voucherStateText;
    }

    public String getVoucherStartDateText() {
        return voucherStartDateText;
    }

    public void setVoucherStartDateText(String voucherStartDateText) {
        this.voucherStartDateText = voucherStartDateText;
    }

    public String getVoucherEndDateText() {
        return voucherEndDateText;
    }

    public void setVoucherEndDateText(String voucherEndDateText) {
        this.voucherEndDateText = voucherEndDateText;
    }

}
