package top.yokey.shopai.zsdk.data;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class OrderPinGouData implements Serializable {

    private String pinGouId = "";
    private String buyerId = "";

    public String getPinGouId() {
        return pinGouId;
    }

    public void setPinGouId(String pinGouId) {
        this.pinGouId = pinGouId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

}
