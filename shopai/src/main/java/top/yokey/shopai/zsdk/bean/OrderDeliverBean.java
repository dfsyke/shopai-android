package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class OrderDeliverBean implements Serializable {

    @SerializedName("express_name")
    private String expressName = "";
    @SerializedName("shipping_code")
    private String shippingCode = "";
    @SerializedName("deliver_info")
    private ArrayList<String> deliverInfo = new ArrayList<>();

    public String getExpressName() {
        return expressName;
    }

    public void setExpressName(String expressName) {
        this.expressName = expressName;
    }

    public String getShippingCode() {
        return shippingCode;
    }

    public void setShippingCode(String shippingCode) {
        this.shippingCode = shippingCode;
    }

    public ArrayList<String> getDeliverInfo() {
        return deliverInfo;
    }

    public void setDeliverInfo(ArrayList<String> deliverInfo) {
        this.deliverInfo = deliverInfo;
    }

}
