package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class PointOrderBean implements Serializable {

    @SerializedName("order_info")
    private OrderInfoBean orderInfo = null;
    @SerializedName("express_info")
    private ExpressInfoBean expressInfo = null;
    @SerializedName("orderaddress_info")
    private OrderaddressInfoBean orderaddressInfo = null;
    @SerializedName("prod_list")
    private ArrayList<ProdListBean> prodList = new ArrayList<>();

    public OrderInfoBean getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfoBean orderInfo) {
        this.orderInfo = orderInfo;
    }

    public ExpressInfoBean getExpressInfo() {
        return expressInfo;
    }

    public void setExpressInfo(ExpressInfoBean expressInfo) {
        this.expressInfo = expressInfo;
    }

    public OrderaddressInfoBean getOrderaddressInfo() {
        return orderaddressInfo;
    }

    public void setOrderaddressInfo(OrderaddressInfoBean orderaddressInfo) {
        this.orderaddressInfo = orderaddressInfo;
    }

    public ArrayList<ProdListBean> getProdList() {
        return prodList;
    }

    public void setProdList(ArrayList<ProdListBean> prodList) {
        this.prodList = prodList;
    }

    public static class OrderInfoBean {

        @SerializedName("point_orderid")
        private String pointOrderid = "";
        @SerializedName("point_ordersn")
        private String pointOrdersn = "";
        @SerializedName("point_buyerid")
        private String pointBuyerid = "";
        @SerializedName("point_buyername")
        private String pointBuyername = "";
        @SerializedName("point_buyeremail")
        private String pointBuyeremail = "";
        @SerializedName("point_addtime")
        private String pointAddtime = "";
        @SerializedName("point_shippingtime")
        private String pointShippingtime = "";
        @SerializedName("point_shippingcode")
        private String pointShippingcode = "";
        @SerializedName("point_shipping_ecode")
        private String pointShippingEcode = "";
        @SerializedName("point_finnshedtime")
        private String pointFinnshedtime = "";
        @SerializedName("point_allpoint")
        private String pointAllpoint = "";
        @SerializedName("point_ordermessage")
        private String pointOrdermessage = "";
        @SerializedName("point_orderstate")
        private String pointOrderstate = "";
        @SerializedName("point_orderstatetext")
        private String pointOrderstatetext = "";
        @SerializedName("point_orderstatesign")
        private String pointOrderstatesign = "";
        @SerializedName("point_orderallowship")
        private boolean pointOrderallowship = false;
        @SerializedName("point_orderalloweditship")
        private boolean pointOrderalloweditship = false;
        @SerializedName("point_orderallowcancel")
        private boolean pointOrderallowcancel = false;
        @SerializedName("point_orderallowdelete")
        private boolean pointOrderallowdelete = false;
        @SerializedName("point_orderallowreceiving")
        private boolean pointOrderallowreceiving = false;
        @SerializedName("deliver_info")
        private ArrayList<String> deliverInfo = new ArrayList<>();

        public String getPointOrderid() {
            return pointOrderid;
        }

        public void setPointOrderid(String pointOrderid) {
            this.pointOrderid = pointOrderid;
        }

        public String getPointOrdersn() {
            return pointOrdersn;
        }

        public void setPointOrdersn(String pointOrdersn) {
            this.pointOrdersn = pointOrdersn;
        }

        public String getPointBuyerid() {
            return pointBuyerid;
        }

        public void setPointBuyerid(String pointBuyerid) {
            this.pointBuyerid = pointBuyerid;
        }

        public String getPointBuyername() {
            return pointBuyername;
        }

        public void setPointBuyername(String pointBuyername) {
            this.pointBuyername = pointBuyername;
        }

        public String getPointBuyeremail() {
            return pointBuyeremail;
        }

        public void setPointBuyeremail(String pointBuyeremail) {
            this.pointBuyeremail = pointBuyeremail;
        }

        public String getPointAddtime() {
            return pointAddtime;
        }

        public void setPointAddtime(String pointAddtime) {
            this.pointAddtime = pointAddtime;
        }

        public String getPointShippingtime() {
            return pointShippingtime;
        }

        public void setPointShippingtime(String pointShippingtime) {
            this.pointShippingtime = pointShippingtime;
        }

        public String getPointShippingcode() {
            return pointShippingcode;
        }

        public void setPointShippingcode(String pointShippingcode) {
            this.pointShippingcode = pointShippingcode;
        }

        public String getPointShippingEcode() {
            return pointShippingEcode;
        }

        public void setPointShippingEcode(String pointShippingEcode) {
            this.pointShippingEcode = pointShippingEcode;
        }

        public String getPointFinnshedtime() {
            return pointFinnshedtime;
        }

        public void setPointFinnshedtime(String pointFinnshedtime) {
            this.pointFinnshedtime = pointFinnshedtime;
        }

        public String getPointAllpoint() {
            return pointAllpoint;
        }

        public void setPointAllpoint(String pointAllpoint) {
            this.pointAllpoint = pointAllpoint;
        }

        public String getPointOrdermessage() {
            return pointOrdermessage;
        }

        public void setPointOrdermessage(String pointOrdermessage) {
            this.pointOrdermessage = pointOrdermessage;
        }

        public String getPointOrderstate() {
            return pointOrderstate;
        }

        public void setPointOrderstate(String pointOrderstate) {
            this.pointOrderstate = pointOrderstate;
        }

        public String getPointOrderstatetext() {
            return pointOrderstatetext;
        }

        public void setPointOrderstatetext(String pointOrderstatetext) {
            this.pointOrderstatetext = pointOrderstatetext;
        }

        public String getPointOrderstatesign() {
            return pointOrderstatesign;
        }

        public void setPointOrderstatesign(String pointOrderstatesign) {
            this.pointOrderstatesign = pointOrderstatesign;
        }

        public boolean isPointOrderallowship() {
            return pointOrderallowship;
        }

        public void setPointOrderallowship(boolean pointOrderallowship) {
            this.pointOrderallowship = pointOrderallowship;
        }

        public boolean isPointOrderalloweditship() {
            return pointOrderalloweditship;
        }

        public void setPointOrderalloweditship(boolean pointOrderalloweditship) {
            this.pointOrderalloweditship = pointOrderalloweditship;
        }

        public boolean isPointOrderallowcancel() {
            return pointOrderallowcancel;
        }

        public void setPointOrderallowcancel(boolean pointOrderallowcancel) {
            this.pointOrderallowcancel = pointOrderallowcancel;
        }

        public boolean isPointOrderallowdelete() {
            return pointOrderallowdelete;
        }

        public void setPointOrderallowdelete(boolean pointOrderallowdelete) {
            this.pointOrderallowdelete = pointOrderallowdelete;
        }

        public boolean isPointOrderallowreceiving() {
            return pointOrderallowreceiving;
        }

        public void setPointOrderallowreceiving(boolean pointOrderallowreceiving) {
            this.pointOrderallowreceiving = pointOrderallowreceiving;
        }

        public ArrayList<String> getDeliverInfo() {
            return deliverInfo;
        }

        public void setDeliverInfo(ArrayList<String> deliverInfo) {
            this.deliverInfo = deliverInfo;
        }

    }

    public static class ExpressInfoBean {

        @SerializedName("id")
        private String id = "";
        @SerializedName("e_name")
        private String eName = "";
        @SerializedName("e_code")
        private String eCode = "";
        @SerializedName("e_letter")
        private String eLetter = "";
        @SerializedName("e_order")
        private String eOrder = "";
        @SerializedName("e_url")
        private String eUrl = "";
        @SerializedName("e_zt_state")
        private String eZtState = "";

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEName() {
            return eName;
        }

        public void setEName(String eName) {
            this.eName = eName;
        }

        public String getECode() {
            return eCode;
        }

        public void setECode(String eCode) {
            this.eCode = eCode;
        }

        public String getELetter() {
            return eLetter;
        }

        public void setELetter(String eLetter) {
            this.eLetter = eLetter;
        }

        public String getEOrder() {
            return eOrder;
        }

        public void setEOrder(String eOrder) {
            this.eOrder = eOrder;
        }

        public String getEUrl() {
            return eUrl;
        }

        public void setEUrl(String eUrl) {
            this.eUrl = eUrl;
        }

        public String getEZtState() {
            return eZtState;
        }

        public void setEZtState(String eZtState) {
            this.eZtState = eZtState;
        }

    }

    public static class OrderaddressInfoBean {

        @SerializedName("point_oaid")
        private String pointOaid = "";
        @SerializedName("point_orderid")
        private String pointOrderid = "";
        @SerializedName("point_truename")
        private String pointTruename = "";
        @SerializedName("point_areaid")
        private String pointAreaid = "";
        @SerializedName("point_areainfo")
        private String pointAreainfo = "";
        @SerializedName("point_address")
        private String pointAddress = "";
        @SerializedName("point_telphone")
        private Object pointTelphone = "";
        @SerializedName("point_mobphone")
        private String pointMobphone = "";

        public String getPointOaid() {
            return pointOaid;
        }

        public void setPointOaid(String pointOaid) {
            this.pointOaid = pointOaid;
        }

        public String getPointOrderid() {
            return pointOrderid;
        }

        public void setPointOrderid(String pointOrderid) {
            this.pointOrderid = pointOrderid;
        }

        public String getPointTruename() {
            return pointTruename;
        }

        public void setPointTruename(String pointTruename) {
            this.pointTruename = pointTruename;
        }

        public String getPointAreaid() {
            return pointAreaid;
        }

        public void setPointAreaid(String pointAreaid) {
            this.pointAreaid = pointAreaid;
        }

        public String getPointAreainfo() {
            return pointAreainfo;
        }

        public void setPointAreainfo(String pointAreainfo) {
            this.pointAreainfo = pointAreainfo;
        }

        public String getPointAddress() {
            return pointAddress;
        }

        public void setPointAddress(String pointAddress) {
            this.pointAddress = pointAddress;
        }

        public Object getPointTelphone() {
            return pointTelphone;
        }

        public void setPointTelphone(Object pointTelphone) {
            this.pointTelphone = pointTelphone;
        }

        public String getPointMobphone() {
            return pointMobphone;
        }

        public void setPointMobphone(String pointMobphone) {
            this.pointMobphone = pointMobphone;
        }

    }

    public static class ProdListBean {

        @SerializedName("point_recid")
        private String pointRecid = "";
        @SerializedName("point_orderid")
        private String pointOrderid = "";
        @SerializedName("point_goodsid")
        private String pointGoodsid = "";
        @SerializedName("point_goodsname")
        private String pointGoodsname = "";
        @SerializedName("point_goodspoints")
        private String pointGoodspoints = "";
        @SerializedName("point_goodsnum")
        private String pointGoodsnum = "";
        @SerializedName("point_goodsimage")
        private String pointGoodsimage = "";
        @SerializedName("point_goodsimage_old")
        private String pointGoodsimageOld = "";
        @SerializedName("point_goodsimage_small")
        private String pointGoodsimageSmall = "";

        public String getPointRecid() {
            return pointRecid;
        }

        public void setPointRecid(String pointRecid) {
            this.pointRecid = pointRecid;
        }

        public String getPointOrderid() {
            return pointOrderid;
        }

        public void setPointOrderid(String pointOrderid) {
            this.pointOrderid = pointOrderid;
        }

        public String getPointGoodsid() {
            return pointGoodsid;
        }

        public void setPointGoodsid(String pointGoodsid) {
            this.pointGoodsid = pointGoodsid;
        }

        public String getPointGoodsname() {
            return pointGoodsname;
        }

        public void setPointGoodsname(String pointGoodsname) {
            this.pointGoodsname = pointGoodsname;
        }

        public String getPointGoodspoints() {
            return pointGoodspoints;
        }

        public void setPointGoodspoints(String pointGoodspoints) {
            this.pointGoodspoints = pointGoodspoints;
        }

        public String getPointGoodsnum() {
            return pointGoodsnum;
        }

        public void setPointGoodsnum(String pointGoodsnum) {
            this.pointGoodsnum = pointGoodsnum;
        }

        public String getPointGoodsimage() {
            return pointGoodsimage;
        }

        public void setPointGoodsimage(String pointGoodsimage) {
            this.pointGoodsimage = pointGoodsimage;
        }

        public String getPointGoodsimageOld() {
            return pointGoodsimageOld;
        }

        public void setPointGoodsimageOld(String pointGoodsimageOld) {
            this.pointGoodsimageOld = pointGoodsimageOld;
        }

        public String getPointGoodsimageSmall() {
            return pointGoodsimageSmall;
        }

        public void setPointGoodsimageSmall(String pointGoodsimageSmall) {
            this.pointGoodsimageSmall = pointGoodsimageSmall;
        }

    }

}
