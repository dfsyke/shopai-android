package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.RetreatReturnDeliverBean;
import top.yokey.shopai.zsdk.bean.RetreatReturnListBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberReturnController {

    private static final String ACT = "member_return";

    public static void shipForm(String returnId, HttpCallBack<RetreatReturnDeliverBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "ship_form")
                .add("return_id", returnId)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), RetreatReturnDeliverBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void getReturnList(String page, HttpCallBack<ArrayList<RetreatReturnListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_return_list")
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "return_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, RetreatReturnListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void shipPost(String returnId, String expressId, String invoiceNo, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "ship_post")
                .add("return_id", returnId)
                .add("express_id", expressId)
                .add("invoice_no", invoiceNo)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
