package top.yokey.shopai.zsdk.callback;

@SuppressWarnings("ALL")
public interface HtmlCallBack {

    void onSuccess(String result);

    void onFailure(String reason);

}
