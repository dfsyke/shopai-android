package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class MemberFXIndexBean implements Serializable {

    @SerializedName("user_name")
    private String userName = "";
    @SerializedName("avatar")
    private String avatar = "";
    @SerializedName("level")
    private String level = "";
    @SerializedName("level_name")
    private String levelName = "";
    @SerializedName("available_fx_trad")
    private String availableFxTrad = "";
    @SerializedName("freeze_fx_trad")
    private String freezeFxTrad = "";
    @SerializedName("is_fxuser")
    private String isFxuser = "";

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getAvailableFxTrad() {
        return availableFxTrad;
    }

    public void setAvailableFxTrad(String availableFxTrad) {
        this.availableFxTrad = availableFxTrad;
    }

    public String getFreezeFxTrad() {
        return freezeFxTrad;
    }

    public void setFreezeFxTrad(String freezeFxTrad) {
        this.freezeFxTrad = freezeFxTrad;
    }

    public String getIsFxuser() {
        return isFxuser;
    }

    public void setIsFxuser(String isFxuser) {
        this.isFxuser = isFxuser;
    }

}
