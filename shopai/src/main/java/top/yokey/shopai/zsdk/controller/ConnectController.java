package top.yokey.shopai.zsdk.controller;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class ConnectController {

    private static final String ACT = "connect";

    public static void getSmsCaptcha(String phone, String type, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "get_sms_captcha")
                .add("phone", phone)
                .add("type", type)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.getString(baseBean.getDatas(), "sms_time"));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void checkSmsCaptcha(String phone, String type, String captcha, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "check_sms_captcha")
                .add("phone", phone)
                .add("type", type)
                .add("captcha", captcha)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void smsRegister(String phone, String password, String captcha, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "sms_register")
                .add("phone", phone)
                .add("password", password)
                .add("captcha", captcha)
                .add("client", "android")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void findPassword(String phone, String password, String captcha, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "find_password")
                .add("phone", phone)
                .add("password", password)
                .add("captcha", captcha)
                .add("client", "android")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
