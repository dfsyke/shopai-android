package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.SellerAddressBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.data.AddressData;

@SuppressWarnings("ALL")
public class SellerAddressController {

    private static final String ACT = "seller_address";

    public static void addressAdd(AddressData data, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "address_add")
                .add("address_id", data.getAddressId().isEmpty() ? "" : data.getAddressId())
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void addressInfo(String id, HttpCallBack<SellerAddressBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "address_info")
                .add("address_id", id)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "address_info");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(data, SellerAddressBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void addressList(HttpCallBack<ArrayList<SellerAddressBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "address_list")
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "address_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, SellerAddressBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
