package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.VoucherListBean;
import top.yokey.shopai.zsdk.bean.VoucherTplListBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class VoucherController {

    private static final String ACT = "voucher";

    public static void voucherList(String page, HttpCallBack<ArrayList<VoucherListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "voucher_list")
                .add("curpage", page)
                .add("page", ShopAISdk.get().getPageNumber())
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "voucher_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, VoucherListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void voucherTplList(String storeId, String getType, HttpCallBack<ArrayList<VoucherTplListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "voucher_tpl_list")
                .add("store_id", storeId)
                .add("gettype", getType)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "voucher_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, VoucherTplListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
