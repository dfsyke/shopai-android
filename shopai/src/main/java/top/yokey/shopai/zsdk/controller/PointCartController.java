package top.yokey.shopai.zsdk.controller;

import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class PointCartController {

    private static final String ACT = "pointcart";

    public static void step1(String addressId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "step1")
                .add("cart_id", "|")
                .add("address_id", addressId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void add(String pgid, String quantity, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "add")
                .add("pgid", pgid)
                .add("quantity", quantity)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void step2(String addressId, String message, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "step2")
                .add("address_options", addressId)
                .add("pcart_message", message)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
