package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class OrderBean implements Serializable {

    @SerializedName("pay_amount")
    private String payAmount = "";
    @SerializedName("add_time")
    private String addTime = "";
    @SerializedName("pay_sn")
    private String paySn = "";
    @SerializedName("order_list")
    private ArrayList<OrderListBean> orderList = new ArrayList<>();

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getPaySn() {
        return paySn;
    }

    public void setPaySn(String paySn) {
        this.paySn = paySn;
    }

    public ArrayList<OrderListBean> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<OrderListBean> orderList) {
        this.orderList = orderList;
    }

    public static class OrderListBean {

        @SerializedName("order_id")
        private String orderId = "";
        @SerializedName("order_sn")
        private String orderSn = "";
        @SerializedName("pay_sn")
        private String paySn = "";
        @SerializedName("pay_sn1")
        private String paySn1 = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("buyer_id")
        private String buyerId = "";
        @SerializedName("buyer_name")
        private String buyerName = "";
        @SerializedName("buyer_email")
        private String buyerEmail = "";
        @SerializedName("buyer_phone")
        private String buyerPhone = "";
        @SerializedName("add_time")
        private String addTime = "";
        @SerializedName("payment_code")
        private String paymentCode = "";
        @SerializedName("payment_time")
        private String paymentTime = "";
        @SerializedName("finnshed_time")
        private String finnshedTime = "";
        @SerializedName("goods_amount")
        private String goodsAmount = "";
        @SerializedName("order_amount")
        private String orderAmount = "";
        @SerializedName("rcb_amount")
        private String rcbAmount = "";
        @SerializedName("pd_amount")
        private String pdAmount = "";
        @SerializedName("points_money")
        private String pointsMoney = "";
        @SerializedName("points_number")
        private String pointsNumber = "";
        @SerializedName("shipping_fee")
        private String shippingFee = "";
        @SerializedName("evaluation_state")
        private String evaluationState = "";
        @SerializedName("evaluation_again_state")
        private String evaluationAgainState = "";
        @SerializedName("order_state")
        private String orderState = "";
        @SerializedName("refund_state")
        private String refundState = "";
        @SerializedName("lock_state")
        private String lockState = "";
        @SerializedName("delete_state")
        private String deleteState = "";
        @SerializedName("refund_amount")
        private String refundAmount = "";
        @SerializedName("delay_time")
        private String delayTime = "";
        @SerializedName("order_from")
        private String orderFrom = "";
        @SerializedName("shipping_code")
        private String shippingCode = "";
        @SerializedName("order_type")
        private String orderType = "";
        @SerializedName("api_pay_time")
        private String apiPayTime = "";
        @SerializedName("chain_id")
        private String chainId = "";
        @SerializedName("chain_code")
        private String chainCode = "";
        @SerializedName("rpt_amount")
        private String rptAmount = "";
        @SerializedName("trade_no")
        private String tradeNo = "";
        @SerializedName("is_fx")
        private String isFx = "";
        @SerializedName("state_desc")
        private String stateDesc = "";
        @SerializedName("payment_name")
        private String paymentName = "";
        @SerializedName("if_cancel")
        private boolean ifCancel = false;
        @SerializedName("if_refund_cancel")
        private boolean ifRefundCancel = false;
        @SerializedName("if_receive")
        private boolean ifReceive = false;
        @SerializedName("if_lock")
        private boolean ifLock = false;
        @SerializedName("if_deliver")
        private boolean ifDeliver = false;
        @SerializedName("if_evaluation")
        private boolean ifEvaluation = false;
        @SerializedName("if_evaluation_again")
        private boolean ifEvaluationAgain = false;
        @SerializedName("if_delete")
        private boolean ifDelete = false;
        @SerializedName("ownshop")
        private boolean ownshop = false;
        @SerializedName("pingou_info")
        private PinGouInfoBean pinGouInfo = null;
        @SerializedName("extend_order_goods")
        private ArrayList<ExtendOrderGoodsBean> extendOrderGoods = new ArrayList<>();
        @SerializedName("zengpin_list")
        private ArrayList<ZengpinListBean> zengpinList = new ArrayList<>();

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getOrderSn() {
            return orderSn;
        }

        public void setOrderSn(String orderSn) {
            this.orderSn = orderSn;
        }

        public String getPaySn() {
            return paySn;
        }

        public void setPaySn(String paySn) {
            this.paySn = paySn;
        }

        public String getPaySn1() {
            return paySn1;
        }

        public void setPaySn1(String paySn1) {
            this.paySn1 = paySn1;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getBuyerName() {
            return buyerName;
        }

        public void setBuyerName(String buyerName) {
            this.buyerName = buyerName;
        }

        public String getBuyerEmail() {
            return buyerEmail;
        }

        public void setBuyerEmail(String buyerEmail) {
            this.buyerEmail = buyerEmail;
        }

        public String getBuyerPhone() {
            return buyerPhone;
        }

        public void setBuyerPhone(String buyerPhone) {
            this.buyerPhone = buyerPhone;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getPaymentCode() {
            return paymentCode;
        }

        public void setPaymentCode(String paymentCode) {
            this.paymentCode = paymentCode;
        }

        public String getPaymentTime() {
            return paymentTime;
        }

        public void setPaymentTime(String paymentTime) {
            this.paymentTime = paymentTime;
        }

        public String getFinnshedTime() {
            return finnshedTime;
        }

        public void setFinnshedTime(String finnshedTime) {
            this.finnshedTime = finnshedTime;
        }

        public String getGoodsAmount() {
            return goodsAmount;
        }

        public void setGoodsAmount(String goodsAmount) {
            this.goodsAmount = goodsAmount;
        }

        public String getOrderAmount() {
            return orderAmount;
        }

        public void setOrderAmount(String orderAmount) {
            this.orderAmount = orderAmount;
        }

        public String getRcbAmount() {
            return rcbAmount;
        }

        public void setRcbAmount(String rcbAmount) {
            this.rcbAmount = rcbAmount;
        }

        public String getPdAmount() {
            return pdAmount;
        }

        public void setPdAmount(String pdAmount) {
            this.pdAmount = pdAmount;
        }

        public String getPointsMoney() {
            return pointsMoney;
        }

        public void setPointsMoney(String pointsMoney) {
            this.pointsMoney = pointsMoney;
        }

        public String getPointsNumber() {
            return pointsNumber;
        }

        public void setPointsNumber(String pointsNumber) {
            this.pointsNumber = pointsNumber;
        }

        public String getShippingFee() {
            return shippingFee;
        }

        public void setShippingFee(String shippingFee) {
            this.shippingFee = shippingFee;
        }

        public String getEvaluationState() {
            return evaluationState;
        }

        public void setEvaluationState(String evaluationState) {
            this.evaluationState = evaluationState;
        }

        public String getEvaluationAgainState() {
            return evaluationAgainState;
        }

        public void setEvaluationAgainState(String evaluationAgainState) {
            this.evaluationAgainState = evaluationAgainState;
        }

        public String getOrderState() {
            return orderState;
        }

        public void setOrderState(String orderState) {
            this.orderState = orderState;
        }

        public String getRefundState() {
            return refundState;
        }

        public void setRefundState(String refundState) {
            this.refundState = refundState;
        }

        public String getLockState() {
            return lockState;
        }

        public void setLockState(String lockState) {
            this.lockState = lockState;
        }

        public String getDeleteState() {
            return deleteState;
        }

        public void setDeleteState(String deleteState) {
            this.deleteState = deleteState;
        }

        public String getRefundAmount() {
            return refundAmount;
        }

        public void setRefundAmount(String refundAmount) {
            this.refundAmount = refundAmount;
        }

        public String getDelayTime() {
            return delayTime;
        }

        public void setDelayTime(String delayTime) {
            this.delayTime = delayTime;
        }

        public String getOrderFrom() {
            return orderFrom;
        }

        public void setOrderFrom(String orderFrom) {
            this.orderFrom = orderFrom;
        }

        public String getShippingCode() {
            return shippingCode;
        }

        public void setShippingCode(String shippingCode) {
            this.shippingCode = shippingCode;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getApiPayTime() {
            return apiPayTime;
        }

        public void setApiPayTime(String apiPayTime) {
            this.apiPayTime = apiPayTime;
        }

        public String getChainId() {
            return chainId;
        }

        public void setChainId(String chainId) {
            this.chainId = chainId;
        }

        public String getChainCode() {
            return chainCode;
        }

        public void setChainCode(String chainCode) {
            this.chainCode = chainCode;
        }

        public String getRptAmount() {
            return rptAmount;
        }

        public void setRptAmount(String rptAmount) {
            this.rptAmount = rptAmount;
        }

        public String getTradeNo() {
            return tradeNo;
        }

        public void setTradeNo(String tradeNo) {
            this.tradeNo = tradeNo;
        }

        public String getIsFx() {
            return isFx;
        }

        public void setIsFx(String isFx) {
            this.isFx = isFx;
        }

        public String getStateDesc() {
            return stateDesc;
        }

        public void setStateDesc(String stateDesc) {
            this.stateDesc = stateDesc;
        }

        public String getPaymentName() {
            return paymentName;
        }

        public void setPaymentName(String paymentName) {
            this.paymentName = paymentName;
        }

        public boolean isIfCancel() {
            return ifCancel;
        }

        public void setIfCancel(boolean ifCancel) {
            this.ifCancel = ifCancel;
        }

        public boolean isIfRefundCancel() {
            return ifRefundCancel;
        }

        public void setIfRefundCancel(boolean ifRefundCancel) {
            this.ifRefundCancel = ifRefundCancel;
        }

        public boolean isIfReceive() {
            return ifReceive;
        }

        public void setIfReceive(boolean ifReceive) {
            this.ifReceive = ifReceive;
        }

        public boolean isIfLock() {
            return ifLock;
        }

        public void setIfLock(boolean ifLock) {
            this.ifLock = ifLock;
        }

        public boolean isIfDeliver() {
            return ifDeliver;
        }

        public void setIfDeliver(boolean ifDeliver) {
            this.ifDeliver = ifDeliver;
        }

        public boolean isIfEvaluation() {
            return ifEvaluation;
        }

        public void setIfEvaluation(boolean ifEvaluation) {
            this.ifEvaluation = ifEvaluation;
        }

        public boolean isIfEvaluationAgain() {
            return ifEvaluationAgain;
        }

        public void setIfEvaluationAgain(boolean ifEvaluationAgain) {
            this.ifEvaluationAgain = ifEvaluationAgain;
        }

        public boolean isIfDelete() {
            return ifDelete;
        }

        public void setIfDelete(boolean ifDelete) {
            this.ifDelete = ifDelete;
        }

        public boolean isOwnshop() {
            return ownshop;
        }

        public void setOwnshop(boolean ownshop) {
            this.ownshop = ownshop;
        }

        public ArrayList<ExtendOrderGoodsBean> getExtendOrderGoods() {
            return extendOrderGoods;
        }

        public void setExtendOrderGoods(ArrayList<ExtendOrderGoodsBean> extendOrderGoods) {
            this.extendOrderGoods = extendOrderGoods;
        }

        public ArrayList<ZengpinListBean> getZengpinList() {
            return zengpinList;
        }

        public void setZengpinList(ArrayList<ZengpinListBean> zengpinList) {
            this.zengpinList = zengpinList;
        }

        public PinGouInfoBean getPinGouInfo() {
            return pinGouInfo;
        }

        public void setPinGouInfo(PinGouInfoBean pinGouInfo) {
            this.pinGouInfo = pinGouInfo;
        }

        public static class PinGouInfoBean {

            @SerializedName("log_id")
            private String logId = "";
            @SerializedName("order_id")
            private String orderId = "";
            @SerializedName("order_sn")
            private String orderSn = "";
            @SerializedName("buyer_type")
            private String buyerType = "";
            @SerializedName("buyer_id")
            private String buyerId = "";
            @SerializedName("buyer_name")
            private String buyerName = "";
            @SerializedName("goods_id")
            private String goodsId = "";
            @SerializedName("goods_commonid")
            private String goodsCommonid = "";
            @SerializedName("min_num")
            private String minNum = "";
            @SerializedName("add_time")
            private String addTime = "";
            @SerializedName("pay_time")
            private String payTime = "";
            @SerializedName("end_time")
            private String endTime = "";
            @SerializedName("cancel_time")
            private String cancelTime = "";
            @SerializedName("store_id")
            private String storeId = "";
            @SerializedName("lock_state")
            private String lockState = "";
            @SerializedName("goods_num")
            private String goodsNum = "";
            @SerializedName("pingou_id")
            private String pingouId = "";

            public String getLogId() {
                return logId;
            }

            public void setLogId(String logId) {
                this.logId = logId;
            }

            public String getOrderId() {
                return orderId;
            }

            public void setOrderId(String orderId) {
                this.orderId = orderId;
            }

            public String getOrderSn() {
                return orderSn;
            }

            public void setOrderSn(String orderSn) {
                this.orderSn = orderSn;
            }

            public String getBuyerType() {
                return buyerType;
            }

            public void setBuyerType(String buyerType) {
                this.buyerType = buyerType;
            }

            public String getBuyerId() {
                return buyerId;
            }

            public void setBuyerId(String buyerId) {
                this.buyerId = buyerId;
            }

            public String getBuyerName() {
                return buyerName;
            }

            public void setBuyerName(String buyerName) {
                this.buyerName = buyerName;
            }

            public String getGoodsId() {
                return goodsId;
            }

            public void setGoodsId(String goodsId) {
                this.goodsId = goodsId;
            }

            public String getGoodsCommonid() {
                return goodsCommonid;
            }

            public void setGoodsCommonid(String goodsCommonid) {
                this.goodsCommonid = goodsCommonid;
            }

            public String getMinNum() {
                return minNum;
            }

            public void setMinNum(String minNum) {
                this.minNum = minNum;
            }

            public String getAddTime() {
                return addTime;
            }

            public void setAddTime(String addTime) {
                this.addTime = addTime;
            }

            public String getPayTime() {
                return payTime;
            }

            public void setPayTime(String payTime) {
                this.payTime = payTime;
            }

            public String getEndTime() {
                return endTime;
            }

            public void setEndTime(String endTime) {
                this.endTime = endTime;
            }

            public String getCancelTime() {
                return cancelTime;
            }

            public void setCancelTime(String cancelTime) {
                this.cancelTime = cancelTime;
            }

            public String getStoreId() {
                return storeId;
            }

            public void setStoreId(String storeId) {
                this.storeId = storeId;
            }

            public String getLockState() {
                return lockState;
            }

            public void setLockState(String lockState) {
                this.lockState = lockState;
            }

            public String getGoodsNum() {
                return goodsNum;
            }

            public void setGoodsNum(String goodsNum) {
                this.goodsNum = goodsNum;
            }

            public String getPingouId() {
                return pingouId;
            }

            public void setPingouId(String pingouId) {
                this.pingouId = pingouId;
            }

        }

        public static class ExtendOrderGoodsBean {

            @SerializedName("goods_id")
            private String goodsId = "";
            @SerializedName("goods_name")
            private String goodsName = "";
            @SerializedName("goods_price")
            private String goodsPrice = "";
            @SerializedName("goods_num")
            private String goodsNum = "";
            @SerializedName("goods_type")
            private String goodsType = "";
            @SerializedName("goods_spec")
            private String goodsSpec = "";
            @SerializedName("invite_rates")
            private String inviteRates = "";
            @SerializedName("goods_commonid")
            private String goodsCommonid = "";
            @SerializedName("add_time")
            private String addTime = "";
            @SerializedName("is_fx")
            private String isFx = "";
            @SerializedName("fx_commis_rate")
            private String fxCommisRate = "";
            @SerializedName("fx_member_id")
            private String fxMemberId = "";
            @SerializedName("goods_serial")
            private String goodsSerial = "";
            @SerializedName("goods_image_url")
            private String goodsImageUrl = "";
            @SerializedName("refund")
            private boolean refund = false;

            public String getGoodsId() {
                return goodsId;
            }

            public void setGoodsId(String goodsId) {
                this.goodsId = goodsId;
            }

            public String getGoodsName() {
                return goodsName;
            }

            public void setGoodsName(String goodsName) {
                this.goodsName = goodsName;
            }

            public String getGoodsPrice() {
                return goodsPrice;
            }

            public void setGoodsPrice(String goodsPrice) {
                this.goodsPrice = goodsPrice;
            }

            public String getGoodsNum() {
                return goodsNum;
            }

            public void setGoodsNum(String goodsNum) {
                this.goodsNum = goodsNum;
            }

            public String getGoodsType() {
                return goodsType;
            }

            public void setGoodsType(String goodsType) {
                this.goodsType = goodsType;
            }

            public String getGoodsSpec() {
                return goodsSpec;
            }

            public void setGoodsSpec(String goodsSpec) {
                this.goodsSpec = goodsSpec;
            }

            public String getInviteRates() {
                return inviteRates;
            }

            public void setInviteRates(String inviteRates) {
                this.inviteRates = inviteRates;
            }

            public String getGoodsCommonid() {
                return goodsCommonid;
            }

            public void setGoodsCommonid(String goodsCommonid) {
                this.goodsCommonid = goodsCommonid;
            }

            public String getAddTime() {
                return addTime;
            }

            public void setAddTime(String addTime) {
                this.addTime = addTime;
            }

            public String getIsFx() {
                return isFx;
            }

            public void setIsFx(String isFx) {
                this.isFx = isFx;
            }

            public String getFxCommisRate() {
                return fxCommisRate;
            }

            public void setFxCommisRate(String fxCommisRate) {
                this.fxCommisRate = fxCommisRate;
            }

            public String getFxMemberId() {
                return fxMemberId;
            }

            public void setFxMemberId(String fxMemberId) {
                this.fxMemberId = fxMemberId;
            }

            public String getGoodsSerial() {
                return goodsSerial;
            }

            public void setGoodsSerial(String goodsSerial) {
                this.goodsSerial = goodsSerial;
            }

            public String getGoodsImageUrl() {
                return goodsImageUrl;
            }

            public void setGoodsImageUrl(String goodsImageUrl) {
                this.goodsImageUrl = goodsImageUrl;
            }

            public boolean isRefund() {
                return refund;
            }

            public void setRefund(boolean refund) {
                this.refund = refund;
            }

        }

        public static class ZengpinListBean {

            @SerializedName("goods_id")
            private String goodsId = "";
            @SerializedName("goods_name")
            private String goodsName = "";
            @SerializedName("goods_price")
            private String goodsPrice = "";
            @SerializedName("goods_num")
            private String goodsNum = "";
            @SerializedName("goods_type")
            private String goodsType = "";
            @SerializedName("goods_spec")
            private String goodsSpec = "";
            @SerializedName("invite_rates")
            private String inviteRates = "";
            @SerializedName("goods_commonid")
            private String goodsCommonid = "";
            @SerializedName("add_time")
            private String addTime = "";
            @SerializedName("is_fx")
            private String isFx = "";
            @SerializedName("fx_commis_rate")
            private String fxCommisRate = "";
            @SerializedName("fx_member_id")
            private String fxMemberId = "";
            @SerializedName("goods_serial")
            private String goodsSerial = "";
            @SerializedName("goods_image_url")
            private String goodsImageUrl = "";
            @SerializedName("refund")
            private boolean refund = false;

            public String getGoodsId() {
                return goodsId;
            }

            public void setGoodsId(String goodsId) {
                this.goodsId = goodsId;
            }

            public String getGoodsName() {
                return goodsName;
            }

            public void setGoodsName(String goodsName) {
                this.goodsName = goodsName;
            }

            public String getGoodsPrice() {
                return goodsPrice;
            }

            public void setGoodsPrice(String goodsPrice) {
                this.goodsPrice = goodsPrice;
            }

            public String getGoodsNum() {
                return goodsNum;
            }

            public void setGoodsNum(String goodsNum) {
                this.goodsNum = goodsNum;
            }

            public String getGoodsType() {
                return goodsType;
            }

            public void setGoodsType(String goodsType) {
                this.goodsType = goodsType;
            }

            public String getGoodsSpec() {
                return goodsSpec;
            }

            public void setGoodsSpec(String goodsSpec) {
                this.goodsSpec = goodsSpec;
            }

            public String getInviteRates() {
                return inviteRates;
            }

            public void setInviteRates(String inviteRates) {
                this.inviteRates = inviteRates;
            }

            public String getGoodsCommonid() {
                return goodsCommonid;
            }

            public void setGoodsCommonid(String goodsCommonid) {
                this.goodsCommonid = goodsCommonid;
            }

            public String getAddTime() {
                return addTime;
            }

            public void setAddTime(String addTime) {
                this.addTime = addTime;
            }

            public String getIsFx() {
                return isFx;
            }

            public void setIsFx(String isFx) {
                this.isFx = isFx;
            }

            public String getFxCommisRate() {
                return fxCommisRate;
            }

            public void setFxCommisRate(String fxCommisRate) {
                this.fxCommisRate = fxCommisRate;
            }

            public String getFxMemberId() {
                return fxMemberId;
            }

            public void setFxMemberId(String fxMemberId) {
                this.fxMemberId = fxMemberId;
            }

            public String getGoodsSerial() {
                return goodsSerial;
            }

            public void setGoodsSerial(String goodsSerial) {
                this.goodsSerial = goodsSerial;
            }

            public String getGoodsImageUrl() {
                return goodsImageUrl;
            }

            public void setGoodsImageUrl(String goodsImageUrl) {
                this.goodsImageUrl = goodsImageUrl;
            }

            public boolean isRefund() {
                return refund;
            }

            public void setRefund(boolean refund) {
                this.refund = refund;
            }

        }

    }

}
