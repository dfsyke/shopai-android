package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class WhatIndexBean implements Serializable {

    @SerializedName("member_info")
    private MemberInfoBean memberInfo = null;
    @SerializedName("index_show_list")
    private ArrayList<IndexShowListBean> indexShowList = new ArrayList<>();
    @SerializedName("member_list")
    private ArrayList<MemberListBean> memberList = new ArrayList<>();
    @SerializedName("personal_list")
    private ArrayList<PersonalListBean> personalList = new ArrayList<>();
    @SerializedName("store_list")
    private ArrayList<StoreListBean> storeList = new ArrayList<>();

    public MemberInfoBean getMemberInfo() {
        return memberInfo;
    }

    public void setMemberInfo(MemberInfoBean memberInfo) {
        this.memberInfo = memberInfo;
    }

    public ArrayList<IndexShowListBean> getIndexShowList() {
        return indexShowList;
    }

    public void setIndexShowList(ArrayList<IndexShowListBean> indexShowList) {
        this.indexShowList = indexShowList;
    }

    public ArrayList<MemberListBean> getMemberList() {
        return memberList;
    }

    public void setMemberList(ArrayList<MemberListBean> memberList) {
        this.memberList = memberList;
    }

    public ArrayList<PersonalListBean> getPersonalList() {
        return personalList;
    }

    public void setPersonalList(ArrayList<PersonalListBean> personalList) {
        this.personalList = personalList;
    }

    public ArrayList<StoreListBean> getStoreList() {
        return storeList;
    }

    public void setStoreList(ArrayList<StoreListBean> storeList) {
        this.storeList = storeList;
    }

    public static class MemberInfoBean {

        @SerializedName("personal_count")
        private String personalCount = "";
        @SerializedName("goods_count")
        private String goodsCount = "";

        public String getPersonalCount() {
            return personalCount;
        }

        public void setPersonalCount(String personalCount) {
            this.personalCount = personalCount;
        }

        public String getGoodsCount() {
            return goodsCount;
        }

        public void setGoodsCount(String goodsCount) {
            this.goodsCount = goodsCount;
        }

    }

    public static class IndexShowListBean {

        @SerializedName("show_id")
        private String showId = "";
        @SerializedName("show_type")
        private String showType = "";
        @SerializedName("show_name")
        private String showName = "";
        @SerializedName("show_image")
        private String showImage = "";
        @SerializedName("show_url")
        private String showUrl = "";
        @SerializedName("show_sort")
        private String showSort = "";
        @SerializedName("image_url")
        private String imageUrl = "";

        public String getShowId() {
            return showId;
        }

        public void setShowId(String showId) {
            this.showId = showId;
        }

        public String getShowType() {
            return showType;
        }

        public void setShowType(String showType) {
            this.showType = showType;
        }

        public String getShowName() {
            return showName;
        }

        public void setShowName(String showName) {
            this.showName = showName;
        }

        public String getShowImage() {
            return showImage;
        }

        public void setShowImage(String showImage) {
            this.showImage = showImage;
        }

        public String getShowUrl() {
            return showUrl;
        }

        public void setShowUrl(String showUrl) {
            this.showUrl = showUrl;
        }

        public String getShowSort() {
            return showSort;
        }

        public void setShowSort(String showSort) {
            this.showSort = showSort;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

    }

    public static class MemberListBean {

        @SerializedName("member_id")
        private String memberId = "";
        @SerializedName("visit_count")
        private String visitCount = "";
        @SerializedName("personal_count")
        private String personalCount = "";
        @SerializedName("goods_count")
        private String goodsCount = "";
        @SerializedName("member_name")
        private String memberName = "";
        @SerializedName("member_truename")
        private String memberTruename = "";
        @SerializedName("member_avatar")
        private String memberAvatar = "";
        @SerializedName("member_sex")
        private String memberSex = "";
        @SerializedName("member_birthday")
        private String memberBirthday = "";
        @SerializedName("member_passwd")
        private String memberPasswd = "";
        @SerializedName("member_paypwd")
        private String memberPaypwd = "";
        @SerializedName("member_email")
        private String memberEmail = "";
        @SerializedName("member_email_bind")
        private String memberEmailBind = "";
        @SerializedName("member_mobile")
        private String memberMobile = "";
        @SerializedName("member_mobile_bind")
        private String memberMobileBind = "";
        @SerializedName("member_qq")
        private String memberQq = "";
        @SerializedName("member_ww")
        private String memberWw = "";
        @SerializedName("member_login_num")
        private String memberLoginNum = "";
        @SerializedName("member_time")
        private String memberTime = "";
        @SerializedName("member_login_time")
        private String memberLoginTime = "";
        @SerializedName("member_old_login_time")
        private String memberOldLoginTime = "";
        @SerializedName("member_login_ip")
        private String memberLoginIp = "";
        @SerializedName("member_old_login_ip")
        private String memberOldLoginIp = "";
        @SerializedName("member_qqopenid")
        private String memberPoints = "";
        @SerializedName("available_predeposit")
        private String availablePredeposit = "";
        @SerializedName("freeze_predeposit")
        private String freezePredeposit = "";
        @SerializedName("available_rc_balance")
        private String availableRcBalance = "";
        @SerializedName("freeze_rc_balance")
        private String freezeRcBalance = "";
        @SerializedName("inform_allow")
        private String informAllow = "";
        @SerializedName("is_buy")
        private String isBuy = "";
        @SerializedName("is_allowtalk")
        private String isAllowtalk = "";
        @SerializedName("member_state")
        private String memberState = "";
        @SerializedName("member_snsvisitnum")
        private String memberSnsvisitnum = "";
        @SerializedName("member_areaid")
        private String memberAreaid = "";
        @SerializedName("member_cityid")
        private String memberCityid = "";
        @SerializedName("member_provinceid")
        private String memberProvinceid = "";
        @SerializedName("member_areainfo")
        private String memberAreainfo = "";
        @SerializedName("member_exppoints")
        private String memberExppoints = "";
        @SerializedName("invite_one")
        private String inviteOne = "";
        @SerializedName("invite_two")
        private String inviteTwo = "";
        @SerializedName("invite_three")
        private String inviteThree = "";
        @SerializedName("inviter_id")
        private String inviterId = "";
        @SerializedName("trad_amount")
        private String tradAmount = "";
        @SerializedName("auth_message")
        private String authMessage = "";
        @SerializedName("fx_state")
        private String fxState = "";
        @SerializedName("bill_user_name")
        private String billUserName = "";
        @SerializedName("bill_type_code")
        private String billTypeCode = "";
        @SerializedName("bill_type_number")
        private String billTypeNumber = "";
        @SerializedName("bill_bank_name")
        private String billBankName = "";
        @SerializedName("freeze_trad")
        private String freezeTrad = "";
        @SerializedName("fx_code")
        private String fxCode = "";
        @SerializedName("fx_time")
        private String fxTime = "";
        @SerializedName("fx_handle_time")
        private String fxHandleTime = "";
        @SerializedName("fx_show")
        private String fxShow = "";
        @SerializedName("fx_apply_times")
        private String fxApplyTimes = "";
        @SerializedName("fx_quit_times")
        private String fxQuitTimes = "";
        @SerializedName("member_reg_ip")
        private String memberRegIp = "";
        @SerializedName("xinge_token")
        private String xingeToken = "";
        @SerializedName("fan_count")
        private String fanCount = "";
        @SerializedName("attention_count")
        private String attentionCount = "";
        @SerializedName("avatar")
        private String avatar = "";

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getVisitCount() {
            return visitCount;
        }

        public void setVisitCount(String visitCount) {
            this.visitCount = visitCount;
        }

        public String getPersonalCount() {
            return personalCount;
        }

        public void setPersonalCount(String personalCount) {
            this.personalCount = personalCount;
        }

        public String getGoodsCount() {
            return goodsCount;
        }

        public void setGoodsCount(String goodsCount) {
            this.goodsCount = goodsCount;
        }

        public String getMemberName() {
            return memberName;
        }

        public void setMemberName(String memberName) {
            this.memberName = memberName;
        }

        public String getMemberTruename() {
            return memberTruename;
        }

        public void setMemberTruename(String memberTruename) {
            this.memberTruename = memberTruename;
        }

        public String getMemberAvatar() {
            return memberAvatar;
        }

        public void setMemberAvatar(String memberAvatar) {
            this.memberAvatar = memberAvatar;
        }

        public String getMemberSex() {
            return memberSex;
        }

        public void setMemberSex(String memberSex) {
            this.memberSex = memberSex;
        }

        public String getMemberBirthday() {
            return memberBirthday;
        }

        public void setMemberBirthday(String memberBirthday) {
            this.memberBirthday = memberBirthday;
        }

        public String getMemberPasswd() {
            return memberPasswd;
        }

        public void setMemberPasswd(String memberPasswd) {
            this.memberPasswd = memberPasswd;
        }

        public String getMemberPaypwd() {
            return memberPaypwd;
        }

        public void setMemberPaypwd(String memberPaypwd) {
            this.memberPaypwd = memberPaypwd;
        }

        public String getMemberEmail() {
            return memberEmail;
        }

        public void setMemberEmail(String memberEmail) {
            this.memberEmail = memberEmail;
        }

        public String getMemberEmailBind() {
            return memberEmailBind;
        }

        public void setMemberEmailBind(String memberEmailBind) {
            this.memberEmailBind = memberEmailBind;
        }

        public String getMemberMobile() {
            return memberMobile;
        }

        public void setMemberMobile(String memberMobile) {
            this.memberMobile = memberMobile;
        }

        public String getMemberMobileBind() {
            return memberMobileBind;
        }

        public void setMemberMobileBind(String memberMobileBind) {
            this.memberMobileBind = memberMobileBind;
        }

        public String getMemberQq() {
            return memberQq;
        }

        public void setMemberQq(String memberQq) {
            this.memberQq = memberQq;
        }

        public String getMemberWw() {
            return memberWw;
        }

        public void setMemberWw(String memberWw) {
            this.memberWw = memberWw;
        }

        public String getMemberLoginNum() {
            return memberLoginNum;
        }

        public void setMemberLoginNum(String memberLoginNum) {
            this.memberLoginNum = memberLoginNum;
        }

        public String getMemberTime() {
            return memberTime;
        }

        public void setMemberTime(String memberTime) {
            this.memberTime = memberTime;
        }

        public String getMemberLoginTime() {
            return memberLoginTime;
        }

        public void setMemberLoginTime(String memberLoginTime) {
            this.memberLoginTime = memberLoginTime;
        }

        public String getMemberOldLoginTime() {
            return memberOldLoginTime;
        }

        public void setMemberOldLoginTime(String memberOldLoginTime) {
            this.memberOldLoginTime = memberOldLoginTime;
        }

        public String getMemberLoginIp() {
            return memberLoginIp;
        }

        public void setMemberLoginIp(String memberLoginIp) {
            this.memberLoginIp = memberLoginIp;
        }

        public String getMemberOldLoginIp() {
            return memberOldLoginIp;
        }

        public void setMemberOldLoginIp(String memberOldLoginIp) {
            this.memberOldLoginIp = memberOldLoginIp;
        }

        public String getMemberPoints() {
            return memberPoints;
        }

        public void setMemberPoints(String memberPoints) {
            this.memberPoints = memberPoints;
        }

        public String getAvailablePredeposit() {
            return availablePredeposit;
        }

        public void setAvailablePredeposit(String availablePredeposit) {
            this.availablePredeposit = availablePredeposit;
        }

        public String getFreezePredeposit() {
            return freezePredeposit;
        }

        public void setFreezePredeposit(String freezePredeposit) {
            this.freezePredeposit = freezePredeposit;
        }

        public String getAvailableRcBalance() {
            return availableRcBalance;
        }

        public void setAvailableRcBalance(String availableRcBalance) {
            this.availableRcBalance = availableRcBalance;
        }

        public String getFreezeRcBalance() {
            return freezeRcBalance;
        }

        public void setFreezeRcBalance(String freezeRcBalance) {
            this.freezeRcBalance = freezeRcBalance;
        }

        public String getInformAllow() {
            return informAllow;
        }

        public void setInformAllow(String informAllow) {
            this.informAllow = informAllow;
        }

        public String getIsBuy() {
            return isBuy;
        }

        public void setIsBuy(String isBuy) {
            this.isBuy = isBuy;
        }

        public String getIsAllowtalk() {
            return isAllowtalk;
        }

        public void setIsAllowtalk(String isAllowtalk) {
            this.isAllowtalk = isAllowtalk;
        }

        public String getMemberState() {
            return memberState;
        }

        public void setMemberState(String memberState) {
            this.memberState = memberState;
        }

        public String getMemberSnsvisitnum() {
            return memberSnsvisitnum;
        }

        public void setMemberSnsvisitnum(String memberSnsvisitnum) {
            this.memberSnsvisitnum = memberSnsvisitnum;
        }

        public String getMemberAreaid() {
            return memberAreaid;
        }

        public void setMemberAreaid(String memberAreaid) {
            this.memberAreaid = memberAreaid;
        }

        public String getMemberCityid() {
            return memberCityid;
        }

        public void setMemberCityid(String memberCityid) {
            this.memberCityid = memberCityid;
        }

        public String getMemberProvinceid() {
            return memberProvinceid;
        }

        public void setMemberProvinceid(String memberProvinceid) {
            this.memberProvinceid = memberProvinceid;
        }

        public String getMemberAreainfo() {
            return memberAreainfo;
        }

        public void setMemberAreainfo(String memberAreainfo) {
            this.memberAreainfo = memberAreainfo;
        }

        public String getMemberExppoints() {
            return memberExppoints;
        }

        public void setMemberExppoints(String memberExppoints) {
            this.memberExppoints = memberExppoints;
        }

        public String getInviteOne() {
            return inviteOne;
        }

        public void setInviteOne(String inviteOne) {
            this.inviteOne = inviteOne;
        }

        public String getInviteTwo() {
            return inviteTwo;
        }

        public void setInviteTwo(String inviteTwo) {
            this.inviteTwo = inviteTwo;
        }

        public String getInviteThree() {
            return inviteThree;
        }

        public void setInviteThree(String inviteThree) {
            this.inviteThree = inviteThree;
        }

        public String getInviterId() {
            return inviterId;
        }

        public void setInviterId(String inviterId) {
            this.inviterId = inviterId;
        }

        public String getTradAmount() {
            return tradAmount;
        }

        public void setTradAmount(String tradAmount) {
            this.tradAmount = tradAmount;
        }

        public String getAuthMessage() {
            return authMessage;
        }

        public void setAuthMessage(String authMessage) {
            this.authMessage = authMessage;
        }

        public String getFxState() {
            return fxState;
        }

        public void setFxState(String fxState) {
            this.fxState = fxState;
        }

        public String getBillUserName() {
            return billUserName;
        }

        public void setBillUserName(String billUserName) {
            this.billUserName = billUserName;
        }

        public String getBillTypeCode() {
            return billTypeCode;
        }

        public void setBillTypeCode(String billTypeCode) {
            this.billTypeCode = billTypeCode;
        }

        public String getBillTypeNumber() {
            return billTypeNumber;
        }

        public void setBillTypeNumber(String billTypeNumber) {
            this.billTypeNumber = billTypeNumber;
        }

        public String getBillBankName() {
            return billBankName;
        }

        public void setBillBankName(String billBankName) {
            this.billBankName = billBankName;
        }

        public String getFreezeTrad() {
            return freezeTrad;
        }

        public void setFreezeTrad(String freezeTrad) {
            this.freezeTrad = freezeTrad;
        }

        public String getFxCode() {
            return fxCode;
        }

        public void setFxCode(String fxCode) {
            this.fxCode = fxCode;
        }

        public String getFxTime() {
            return fxTime;
        }

        public void setFxTime(String fxTime) {
            this.fxTime = fxTime;
        }

        public String getFxHandleTime() {
            return fxHandleTime;
        }

        public void setFxHandleTime(String fxHandleTime) {
            this.fxHandleTime = fxHandleTime;
        }

        public String getFxShow() {
            return fxShow;
        }

        public void setFxShow(String fxShow) {
            this.fxShow = fxShow;
        }

        public String getFxApplyTimes() {
            return fxApplyTimes;
        }

        public void setFxApplyTimes(String fxApplyTimes) {
            this.fxApplyTimes = fxApplyTimes;
        }

        public String getFxQuitTimes() {
            return fxQuitTimes;
        }

        public void setFxQuitTimes(String fxQuitTimes) {
            this.fxQuitTimes = fxQuitTimes;
        }

        public String getMemberRegIp() {
            return memberRegIp;
        }

        public void setMemberRegIp(String memberRegIp) {
            this.memberRegIp = memberRegIp;
        }

        public String getXingeToken() {
            return xingeToken;
        }

        public void setXingeToken(String xingeToken) {
            this.xingeToken = xingeToken;
        }

        public String getFanCount() {
            return fanCount;
        }

        public void setFanCount(String fanCount) {
            this.fanCount = fanCount;
        }

        public String getAttentionCount() {
            return attentionCount;
        }

        public void setAttentionCount(String attentionCount) {
            this.attentionCount = attentionCount;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

    }

    public static class PersonalListBean {

        @SerializedName("personal_id")
        private String personalId = "";
        @SerializedName("commend_member_id")
        private String commendMemberId = "";
        @SerializedName("commend_image")
        private String commendImage = "";
        @SerializedName("commend_buy")
        private String commendBuy = "";
        @SerializedName("commend_message")
        private String commendMessage = "";
        @SerializedName("commend_time")
        private String commendTime = "";
        @SerializedName("class_id")
        private String classId = "";
        @SerializedName("like_count")
        private String likeCount = "";
        @SerializedName("comment_count")
        private String commentCount = "";
        @SerializedName("click_count")
        private String clickCount = "";
        @SerializedName("what_commend")
        private String whatCommend = "";
        @SerializedName("what_sort")
        private String whatSort = "";
        @SerializedName("member_id")
        private String memberId = "";
        @SerializedName("member_name")
        private String memberName = "";
        @SerializedName("member_truename")
        private String memberTruename = "";
        @SerializedName("member_avatar")
        private String memberAvatar = "";
        @SerializedName("member_sex")
        private String memberSex = "";
        @SerializedName("member_birthday")
        private String memberBirthday = "";
        @SerializedName("member_passwd")
        private String memberPasswd = "";
        @SerializedName("member_paypwd")
        private String memberPaypwd = "";
        @SerializedName("member_email")
        private String memberEmail = "";
        @SerializedName("member_email_bind")
        private String memberEmailBind = "";
        @SerializedName("member_mobile")
        private String memberMobile = "";
        @SerializedName("member_mobile_bind")
        private String memberMobileBind = "";
        @SerializedName("member_qq")
        private String memberQq = "";
        @SerializedName("member_ww")
        private String memberWw = "";
        @SerializedName("member_login_num")
        private String memberLoginNum = "";
        @SerializedName("member_time")
        private String memberTime = "";
        @SerializedName("member_login_time")
        private String memberLoginTime = "";
        @SerializedName("member_old_login_time")
        private String memberOldLoginTime = "";
        @SerializedName("member_login_ip")
        private String memberLoginIp = "";
        @SerializedName("member_old_login_ip")
        private String memberOldLoginIp = "";
        @SerializedName("member_points")
        private String memberPoints = "";
        @SerializedName("available_predeposit")
        private String availablePredeposit = "";
        @SerializedName("freeze_predeposit")
        private String freezePredeposit = "";
        @SerializedName("available_rc_balance")
        private String availableRcBalance = "";
        @SerializedName("freeze_rc_balance")
        private String freezeRcBalance = "";
        @SerializedName("inform_allow")
        private String informAllow = "";
        @SerializedName("is_buy")
        private String isBuy = "";
        @SerializedName("is_allowtalk")
        private String isAllowtalk = "";
        @SerializedName("member_state")
        private String memberState = "";
        @SerializedName("member_snsvisitnum")
        private String memberSnsvisitnum = "";
        @SerializedName("member_areaid")
        private String memberAreaid = "";
        @SerializedName("member_cityid")
        private String memberCityid = "";
        @SerializedName("member_provinceid")
        private String memberProvinceid = "";
        @SerializedName("member_areainfo")
        private String memberAreainfo = "";
        @SerializedName("member_exppoints")
        private String memberExppoints = "";
        @SerializedName("invite_one")
        private String inviteOne = "";
        @SerializedName("invite_two")
        private String inviteTwo = "";
        @SerializedName("invite_three")
        private String inviteThree = "";
        @SerializedName("inviter_id")
        private String inviterId = "";
        @SerializedName("trad_amount")
        private String tradAmount = "";
        @SerializedName("auth_message")
        private String authMessage = "";
        @SerializedName("fx_state")
        private String fxState = "";
        @SerializedName("bill_user_name")
        private String billUserName = "";
        @SerializedName("bill_type_code")
        private String billTypeCode = "";
        @SerializedName("bill_type_number")
        private String billTypeNumber = "";
        @SerializedName("bill_bank_name")
        private String billBankName = "";
        @SerializedName("freeze_trad")
        private String freezeTrad = "";
        @SerializedName("fx_code")
        private String fxCode = "";
        @SerializedName("fx_time")
        private String fxTime = "";
        @SerializedName("fx_handle_time")
        private String fxHandleTime = "";
        @SerializedName("fx_show")
        private String fxShow = "";
        @SerializedName("quit_time")
        private String fxApplyTimes = "";
        @SerializedName("fx_quit_times")
        private String fxQuitTimes = "";
        @SerializedName("member_reg_ip")
        private String memberRegIp = "";
        @SerializedName("xinge_token")
        private String xingeToken = "";
        @SerializedName("commend_image_url")
        private String commendImageUrl = "";

        public String getPersonalId() {
            return personalId;
        }

        public void setPersonalId(String personalId) {
            this.personalId = personalId;
        }

        public String getCommendMemberId() {
            return commendMemberId;
        }

        public void setCommendMemberId(String commendMemberId) {
            this.commendMemberId = commendMemberId;
        }

        public String getCommendImage() {
            return commendImage;
        }

        public void setCommendImage(String commendImage) {
            this.commendImage = commendImage;
        }

        public String getCommendBuy() {
            return commendBuy;
        }

        public void setCommendBuy(String commendBuy) {
            this.commendBuy = commendBuy;
        }

        public String getCommendMessage() {
            return commendMessage;
        }

        public void setCommendMessage(String commendMessage) {
            this.commendMessage = commendMessage;
        }

        public String getCommendTime() {
            return commendTime;
        }

        public void setCommendTime(String commendTime) {
            this.commendTime = commendTime;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getLikeCount() {
            return likeCount;
        }

        public void setLikeCount(String likeCount) {
            this.likeCount = likeCount;
        }

        public String getCommentCount() {
            return commentCount;
        }

        public void setCommentCount(String commentCount) {
            this.commentCount = commentCount;
        }

        public String getClickCount() {
            return clickCount;
        }

        public void setClickCount(String clickCount) {
            this.clickCount = clickCount;
        }

        public String getWhatCommend() {
            return whatCommend;
        }

        public void setWhatCommend(String whatCommend) {
            this.whatCommend = whatCommend;
        }

        public String getWhatSort() {
            return whatSort;
        }

        public void setWhatSort(String whatSort) {
            this.whatSort = whatSort;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getMemberName() {
            return memberName;
        }

        public void setMemberName(String memberName) {
            this.memberName = memberName;
        }

        public String getMemberTruename() {
            return memberTruename;
        }

        public void setMemberTruename(String memberTruename) {
            this.memberTruename = memberTruename;
        }

        public String getMemberAvatar() {
            return memberAvatar;
        }

        public void setMemberAvatar(String memberAvatar) {
            this.memberAvatar = memberAvatar;
        }

        public String getMemberSex() {
            return memberSex;
        }

        public void setMemberSex(String memberSex) {
            this.memberSex = memberSex;
        }

        public String getMemberBirthday() {
            return memberBirthday;
        }

        public void setMemberBirthday(String memberBirthday) {
            this.memberBirthday = memberBirthday;
        }

        public String getMemberPasswd() {
            return memberPasswd;
        }

        public void setMemberPasswd(String memberPasswd) {
            this.memberPasswd = memberPasswd;
        }

        public String getMemberPaypwd() {
            return memberPaypwd;
        }

        public void setMemberPaypwd(String memberPaypwd) {
            this.memberPaypwd = memberPaypwd;
        }

        public String getMemberEmail() {
            return memberEmail;
        }

        public void setMemberEmail(String memberEmail) {
            this.memberEmail = memberEmail;
        }

        public String getMemberEmailBind() {
            return memberEmailBind;
        }

        public void setMemberEmailBind(String memberEmailBind) {
            this.memberEmailBind = memberEmailBind;
        }

        public String getMemberMobile() {
            return memberMobile;
        }

        public void setMemberMobile(String memberMobile) {
            this.memberMobile = memberMobile;
        }

        public String getMemberMobileBind() {
            return memberMobileBind;
        }

        public void setMemberMobileBind(String memberMobileBind) {
            this.memberMobileBind = memberMobileBind;
        }

        public String getMemberQq() {
            return memberQq;
        }

        public void setMemberQq(String memberQq) {
            this.memberQq = memberQq;
        }

        public String getMemberWw() {
            return memberWw;
        }

        public void setMemberWw(String memberWw) {
            this.memberWw = memberWw;
        }

        public String getMemberLoginNum() {
            return memberLoginNum;
        }

        public void setMemberLoginNum(String memberLoginNum) {
            this.memberLoginNum = memberLoginNum;
        }

        public String getMemberTime() {
            return memberTime;
        }

        public void setMemberTime(String memberTime) {
            this.memberTime = memberTime;
        }

        public String getMemberLoginTime() {
            return memberLoginTime;
        }

        public void setMemberLoginTime(String memberLoginTime) {
            this.memberLoginTime = memberLoginTime;
        }

        public String getMemberOldLoginTime() {
            return memberOldLoginTime;
        }

        public void setMemberOldLoginTime(String memberOldLoginTime) {
            this.memberOldLoginTime = memberOldLoginTime;
        }

        public String getMemberLoginIp() {
            return memberLoginIp;
        }

        public void setMemberLoginIp(String memberLoginIp) {
            this.memberLoginIp = memberLoginIp;
        }

        public String getMemberOldLoginIp() {
            return memberOldLoginIp;
        }

        public void setMemberOldLoginIp(String memberOldLoginIp) {
            this.memberOldLoginIp = memberOldLoginIp;
        }

        public String getMemberPoints() {
            return memberPoints;
        }

        public void setMemberPoints(String memberPoints) {
            this.memberPoints = memberPoints;
        }

        public String getAvailablePredeposit() {
            return availablePredeposit;
        }

        public void setAvailablePredeposit(String availablePredeposit) {
            this.availablePredeposit = availablePredeposit;
        }

        public String getFreezePredeposit() {
            return freezePredeposit;
        }

        public void setFreezePredeposit(String freezePredeposit) {
            this.freezePredeposit = freezePredeposit;
        }

        public String getAvailableRcBalance() {
            return availableRcBalance;
        }

        public void setAvailableRcBalance(String availableRcBalance) {
            this.availableRcBalance = availableRcBalance;
        }

        public String getFreezeRcBalance() {
            return freezeRcBalance;
        }

        public void setFreezeRcBalance(String freezeRcBalance) {
            this.freezeRcBalance = freezeRcBalance;
        }

        public String getInformAllow() {
            return informAllow;
        }

        public void setInformAllow(String informAllow) {
            this.informAllow = informAllow;
        }

        public String getIsBuy() {
            return isBuy;
        }

        public void setIsBuy(String isBuy) {
            this.isBuy = isBuy;
        }

        public String getIsAllowtalk() {
            return isAllowtalk;
        }

        public void setIsAllowtalk(String isAllowtalk) {
            this.isAllowtalk = isAllowtalk;
        }

        public String getMemberState() {
            return memberState;
        }

        public void setMemberState(String memberState) {
            this.memberState = memberState;
        }

        public String getMemberSnsvisitnum() {
            return memberSnsvisitnum;
        }

        public void setMemberSnsvisitnum(String memberSnsvisitnum) {
            this.memberSnsvisitnum = memberSnsvisitnum;
        }

        public String getMemberAreaid() {
            return memberAreaid;
        }

        public void setMemberAreaid(String memberAreaid) {
            this.memberAreaid = memberAreaid;
        }

        public String getMemberCityid() {
            return memberCityid;
        }

        public void setMemberCityid(String memberCityid) {
            this.memberCityid = memberCityid;
        }

        public String getMemberProvinceid() {
            return memberProvinceid;
        }

        public void setMemberProvinceid(String memberProvinceid) {
            this.memberProvinceid = memberProvinceid;
        }

        public String getMemberAreainfo() {
            return memberAreainfo;
        }

        public void setMemberAreainfo(String memberAreainfo) {
            this.memberAreainfo = memberAreainfo;
        }

        public String getMemberExppoints() {
            return memberExppoints;
        }

        public void setMemberExppoints(String memberExppoints) {
            this.memberExppoints = memberExppoints;
        }

        public String getInviteOne() {
            return inviteOne;
        }

        public void setInviteOne(String inviteOne) {
            this.inviteOne = inviteOne;
        }

        public String getInviteTwo() {
            return inviteTwo;
        }

        public void setInviteTwo(String inviteTwo) {
            this.inviteTwo = inviteTwo;
        }

        public String getInviteThree() {
            return inviteThree;
        }

        public void setInviteThree(String inviteThree) {
            this.inviteThree = inviteThree;
        }

        public String getInviterId() {
            return inviterId;
        }

        public void setInviterId(String inviterId) {
            this.inviterId = inviterId;
        }

        public String getTradAmount() {
            return tradAmount;
        }

        public void setTradAmount(String tradAmount) {
            this.tradAmount = tradAmount;
        }

        public String getAuthMessage() {
            return authMessage;
        }

        public void setAuthMessage(String authMessage) {
            this.authMessage = authMessage;
        }

        public String getFxState() {
            return fxState;
        }

        public void setFxState(String fxState) {
            this.fxState = fxState;
        }

        public String getBillUserName() {
            return billUserName;
        }

        public void setBillUserName(String billUserName) {
            this.billUserName = billUserName;
        }

        public String getBillTypeCode() {
            return billTypeCode;
        }

        public void setBillTypeCode(String billTypeCode) {
            this.billTypeCode = billTypeCode;
        }

        public String getBillTypeNumber() {
            return billTypeNumber;
        }

        public void setBillTypeNumber(String billTypeNumber) {
            this.billTypeNumber = billTypeNumber;
        }

        public String getBillBankName() {
            return billBankName;
        }

        public void setBillBankName(String billBankName) {
            this.billBankName = billBankName;
        }

        public String getFreezeTrad() {
            return freezeTrad;
        }

        public void setFreezeTrad(String freezeTrad) {
            this.freezeTrad = freezeTrad;
        }

        public String getFxCode() {
            return fxCode;
        }

        public void setFxCode(String fxCode) {
            this.fxCode = fxCode;
        }

        public String getFxTime() {
            return fxTime;
        }

        public void setFxTime(String fxTime) {
            this.fxTime = fxTime;
        }

        public String getFxHandleTime() {
            return fxHandleTime;
        }

        public void setFxHandleTime(String fxHandleTime) {
            this.fxHandleTime = fxHandleTime;
        }

        public String getFxShow() {
            return fxShow;
        }

        public void setFxShow(String fxShow) {
            this.fxShow = fxShow;
        }

        public String getFxApplyTimes() {
            return fxApplyTimes;
        }

        public void setFxApplyTimes(String fxApplyTimes) {
            this.fxApplyTimes = fxApplyTimes;
        }

        public String getFxQuitTimes() {
            return fxQuitTimes;
        }

        public void setFxQuitTimes(String fxQuitTimes) {
            this.fxQuitTimes = fxQuitTimes;
        }

        public String getMemberRegIp() {
            return memberRegIp;
        }

        public void setMemberRegIp(String memberRegIp) {
            this.memberRegIp = memberRegIp;
        }

        public String getXingeToken() {
            return xingeToken;
        }

        public void setXingeToken(String xingeToken) {
            this.xingeToken = xingeToken;
        }

        public String getCommendImageUrl() {
            return commendImageUrl;
        }

        public void setCommendImageUrl(String commendImageUrl) {
            this.commendImageUrl = commendImageUrl;
        }

    }

    public static class StoreListBean {

        @SerializedName("what_store_id")
        private String whatStoreId = "";
        @SerializedName("shop_store_id")
        private String shopStoreId = "";
        @SerializedName("what_sort")
        private String whatSort = "";
        @SerializedName("what_commend")
        private String whatCommend = "";
        @SerializedName("like_count")
        private String likeCount = "";
        @SerializedName("comment_count")
        private String commentCount = "";
        @SerializedName("click_count")
        private String clickCount = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("grade_id")
        private String gradeId = "";
        @SerializedName("member_id")
        private String memberId = "";
        @SerializedName("member_name")
        private String memberName = "";
        @SerializedName("seller_name")
        private String sellerName = "";
        @SerializedName("sc_id")
        private String scId = "";
        @SerializedName("province_id")
        private String provinceId = "";
        @SerializedName("area_info")
        private String areaInfo = "";
        @SerializedName("store_address")
        private String storeAddress = "";
        @SerializedName("store_zip")
        private String storeZip = "";
        @SerializedName("store_state")
        private String storeState = "";
        @SerializedName("store_sort")
        private String storeSort = "";
        @SerializedName("store_time")
        private String storeTime = "";
        @SerializedName("store_label")
        private String storeLabel = "";
        @SerializedName("store_banner")
        private String storeBanner = "";
        @SerializedName("store_avatar")
        private String storeAvatar = "";
        @SerializedName("store_keywords")
        private String storeKeywords = "";
        @SerializedName("store_description")
        private String storeDescription = "";
        @SerializedName("store_qq")
        private String storeQq = "";
        @SerializedName("store_ww")
        private String storeWw = "";
        @SerializedName("store_phone")
        private String storePhone = "";
        @SerializedName("store_zy")
        private String storeZy = "";
        @SerializedName("store_domain_times")
        private String storeDomainTimes = "";
        @SerializedName("store_recommend")
        private String storeRecommend = "";
        @SerializedName("store_theme")
        private String storeTheme = "";
        @SerializedName("store_credit")
        private StoreCreditBean storeCredit = null;
        @SerializedName("store_desccredit")
        private String storeDesccredit = "";
        @SerializedName("store_servicecredit")
        private String storeServicecredit = "";
        @SerializedName("store_deliverycredit")
        private String storeDeliverycredit = "";
        @SerializedName("store_collect")
        private String storeCollect = "";
        @SerializedName("store_slide")
        private String storeSlide = "";
        @SerializedName("store_slide_url")
        private String storeSlideUrl = "";
        @SerializedName("store_sales")
        private String storeSales = "";
        @SerializedName("store_free_price")
        private String storeFreePrice = "";
        @SerializedName("store_free_time")
        private String storeFreeTime = "";
        @SerializedName("store_decoration_switch")
        private String storeDecorationSwitch = "";
        @SerializedName("store_decoration_only")
        private String storeDecorationOnly = "";
        @SerializedName("store_decoration_image_count")
        private String storeDecorationImageCount = "";
        @SerializedName("is_own_shop")
        private String isOwnShop = "";
        @SerializedName("bind_all_gc")
        private String bindAllGc = "";
        @SerializedName("store_vrcode_prefix")
        private String storeVrcodePrefix = "";
        @SerializedName("mb_title_img")
        private String mbTitleImg = "";
        @SerializedName("mb_sliders")
        private String mbSliders = "";
        @SerializedName("left_bar_type")
        private String leftBarType = "";
        @SerializedName("is_distribution")
        private String isDistribution = "";
        @SerializedName("is_person")
        private String isPerson = "";
        @SerializedName("mb_store_decoration_switch")
        private String mbStoreDecorationSwitch = "";
        @SerializedName("goods_count")
        private String goodsCount = "";
        @SerializedName("store_credit_average")
        private String storeCreditAverage = "";
        @SerializedName("store_credit_percent")
        private String storeCreditPercent = "";
        @SerializedName("hot_sales_list")
        private ArrayList<HotSalesListBean> hotSalesList = new ArrayList<>();

        public String getWhatStoreId() {
            return whatStoreId;
        }

        public void setWhatStoreId(String whatStoreId) {
            this.whatStoreId = whatStoreId;
        }

        public String getShopStoreId() {
            return shopStoreId;
        }

        public void setShopStoreId(String shopStoreId) {
            this.shopStoreId = shopStoreId;
        }

        public String getWhatSort() {
            return whatSort;
        }

        public void setWhatSort(String whatSort) {
            this.whatSort = whatSort;
        }

        public String getWhatCommend() {
            return whatCommend;
        }

        public void setWhatCommend(String whatCommend) {
            this.whatCommend = whatCommend;
        }

        public String getLikeCount() {
            return likeCount;
        }

        public void setLikeCount(String likeCount) {
            this.likeCount = likeCount;
        }

        public String getCommentCount() {
            return commentCount;
        }

        public void setCommentCount(String commentCount) {
            this.commentCount = commentCount;
        }

        public String getClickCount() {
            return clickCount;
        }

        public void setClickCount(String clickCount) {
            this.clickCount = clickCount;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getMemberName() {
            return memberName;
        }

        public void setMemberName(String memberName) {
            this.memberName = memberName;
        }

        public String getSellerName() {
            return sellerName;
        }

        public void setSellerName(String sellerName) {
            this.sellerName = sellerName;
        }

        public String getScId() {
            return scId;
        }

        public void setScId(String scId) {
            this.scId = scId;
        }

        public String getProvinceId() {
            return provinceId;
        }

        public void setProvinceId(String provinceId) {
            this.provinceId = provinceId;
        }

        public String getAreaInfo() {
            return areaInfo;
        }

        public void setAreaInfo(String areaInfo) {
            this.areaInfo = areaInfo;
        }

        public String getStoreAddress() {
            return storeAddress;
        }

        public void setStoreAddress(String storeAddress) {
            this.storeAddress = storeAddress;
        }

        public String getStoreZip() {
            return storeZip;
        }

        public void setStoreZip(String storeZip) {
            this.storeZip = storeZip;
        }

        public String getStoreState() {
            return storeState;
        }

        public void setStoreState(String storeState) {
            this.storeState = storeState;
        }

        public String getStoreSort() {
            return storeSort;
        }

        public void setStoreSort(String storeSort) {
            this.storeSort = storeSort;
        }

        public String getStoreTime() {
            return storeTime;
        }

        public void setStoreTime(String storeTime) {
            this.storeTime = storeTime;
        }

        public String getStoreLabel() {
            return storeLabel;
        }

        public void setStoreLabel(String storeLabel) {
            this.storeLabel = storeLabel;
        }

        public String getStoreBanner() {
            return storeBanner;
        }

        public void setStoreBanner(String storeBanner) {
            this.storeBanner = storeBanner;
        }

        public String getStoreAvatar() {
            return storeAvatar;
        }

        public void setStoreAvatar(String storeAvatar) {
            this.storeAvatar = storeAvatar;
        }

        public String getStoreKeywords() {
            return storeKeywords;
        }

        public void setStoreKeywords(String storeKeywords) {
            this.storeKeywords = storeKeywords;
        }

        public String getStoreDescription() {
            return storeDescription;
        }

        public void setStoreDescription(String storeDescription) {
            this.storeDescription = storeDescription;
        }

        public String getStoreQq() {
            return storeQq;
        }

        public void setStoreQq(String storeQq) {
            this.storeQq = storeQq;
        }

        public String getStoreWw() {
            return storeWw;
        }

        public void setStoreWw(String storeWw) {
            this.storeWw = storeWw;
        }

        public String getStorePhone() {
            return storePhone;
        }

        public void setStorePhone(String storePhone) {
            this.storePhone = storePhone;
        }

        public String getStoreZy() {
            return storeZy;
        }

        public void setStoreZy(String storeZy) {
            this.storeZy = storeZy;
        }

        public String getStoreDomainTimes() {
            return storeDomainTimes;
        }

        public void setStoreDomainTimes(String storeDomainTimes) {
            this.storeDomainTimes = storeDomainTimes;
        }

        public String getStoreRecommend() {
            return storeRecommend;
        }

        public void setStoreRecommend(String storeRecommend) {
            this.storeRecommend = storeRecommend;
        }

        public String getStoreTheme() {
            return storeTheme;
        }

        public void setStoreTheme(String storeTheme) {
            this.storeTheme = storeTheme;
        }

        public StoreCreditBean getStoreCredit() {
            return storeCredit;
        }

        public void setStoreCredit(StoreCreditBean storeCredit) {
            this.storeCredit = storeCredit;
        }

        public String getStoreDesccredit() {
            return storeDesccredit;
        }

        public void setStoreDesccredit(String storeDesccredit) {
            this.storeDesccredit = storeDesccredit;
        }

        public String getStoreServicecredit() {
            return storeServicecredit;
        }

        public void setStoreServicecredit(String storeServicecredit) {
            this.storeServicecredit = storeServicecredit;
        }

        public String getStoreDeliverycredit() {
            return storeDeliverycredit;
        }

        public void setStoreDeliverycredit(String storeDeliverycredit) {
            this.storeDeliverycredit = storeDeliverycredit;
        }

        public String getStoreCollect() {
            return storeCollect;
        }

        public void setStoreCollect(String storeCollect) {
            this.storeCollect = storeCollect;
        }

        public String getStoreSlide() {
            return storeSlide;
        }

        public void setStoreSlide(String storeSlide) {
            this.storeSlide = storeSlide;
        }

        public String getStoreSlideUrl() {
            return storeSlideUrl;
        }

        public void setStoreSlideUrl(String storeSlideUrl) {
            this.storeSlideUrl = storeSlideUrl;
        }

        public String getStoreSales() {
            return storeSales;
        }

        public void setStoreSales(String storeSales) {
            this.storeSales = storeSales;
        }

        public String getStoreFreePrice() {
            return storeFreePrice;
        }

        public void setStoreFreePrice(String storeFreePrice) {
            this.storeFreePrice = storeFreePrice;
        }

        public String getStoreFreeTime() {
            return storeFreeTime;
        }

        public void setStoreFreeTime(String storeFreeTime) {
            this.storeFreeTime = storeFreeTime;
        }

        public String getStoreDecorationSwitch() {
            return storeDecorationSwitch;
        }

        public void setStoreDecorationSwitch(String storeDecorationSwitch) {
            this.storeDecorationSwitch = storeDecorationSwitch;
        }

        public String getStoreDecorationOnly() {
            return storeDecorationOnly;
        }

        public void setStoreDecorationOnly(String storeDecorationOnly) {
            this.storeDecorationOnly = storeDecorationOnly;
        }

        public String getStoreDecorationImageCount() {
            return storeDecorationImageCount;
        }

        public void setStoreDecorationImageCount(String storeDecorationImageCount) {
            this.storeDecorationImageCount = storeDecorationImageCount;
        }

        public String getIsOwnShop() {
            return isOwnShop;
        }

        public void setIsOwnShop(String isOwnShop) {
            this.isOwnShop = isOwnShop;
        }

        public String getBindAllGc() {
            return bindAllGc;
        }

        public void setBindAllGc(String bindAllGc) {
            this.bindAllGc = bindAllGc;
        }

        public String getStoreVrcodePrefix() {
            return storeVrcodePrefix;
        }

        public void setStoreVrcodePrefix(String storeVrcodePrefix) {
            this.storeVrcodePrefix = storeVrcodePrefix;
        }

        public String getMbTitleImg() {
            return mbTitleImg;
        }

        public void setMbTitleImg(String mbTitleImg) {
            this.mbTitleImg = mbTitleImg;
        }

        public String getMbSliders() {
            return mbSliders;
        }

        public void setMbSliders(String mbSliders) {
            this.mbSliders = mbSliders;
        }

        public String getLeftBarType() {
            return leftBarType;
        }

        public void setLeftBarType(String leftBarType) {
            this.leftBarType = leftBarType;
        }

        public String getIsDistribution() {
            return isDistribution;
        }

        public void setIsDistribution(String isDistribution) {
            this.isDistribution = isDistribution;
        }

        public String getIsPerson() {
            return isPerson;
        }

        public void setIsPerson(String isPerson) {
            this.isPerson = isPerson;
        }

        public String getMbStoreDecorationSwitch() {
            return mbStoreDecorationSwitch;
        }

        public void setMbStoreDecorationSwitch(String mbStoreDecorationSwitch) {
            this.mbStoreDecorationSwitch = mbStoreDecorationSwitch;
        }

        public String getGoodsCount() {
            return goodsCount;
        }

        public void setGoodsCount(String goodsCount) {
            this.goodsCount = goodsCount;
        }

        public String getStoreCreditAverage() {
            return storeCreditAverage;
        }

        public void setStoreCreditAverage(String storeCreditAverage) {
            this.storeCreditAverage = storeCreditAverage;
        }

        public String getStoreCreditPercent() {
            return storeCreditPercent;
        }

        public void setStoreCreditPercent(String storeCreditPercent) {
            this.storeCreditPercent = storeCreditPercent;
        }

        public ArrayList<HotSalesListBean> getHotSalesList() {
            return hotSalesList;
        }

        public void setHotSalesList(ArrayList<HotSalesListBean> hotSalesList) {
            this.hotSalesList = hotSalesList;
        }

        public static class StoreCreditBean {

            @SerializedName("store_desccredit")
            private StoreDesccreditBean storeDesccredit = null;
            @SerializedName("store_servicecredit")
            private StoreServicecreditBean storeServicecredit = null;
            @SerializedName("store_deliverycredit")
            private StoreDeliverycreditBean storeDeliverycredit = null;

            public StoreDesccreditBean getStoreDesccredit() {
                return storeDesccredit;
            }

            public void setStoreDesccredit(StoreDesccreditBean storeDesccredit) {
                this.storeDesccredit = storeDesccredit;
            }

            public StoreServicecreditBean getStoreServicecredit() {
                return storeServicecredit;
            }

            public void setStoreServicecredit(StoreServicecreditBean storeServicecredit) {
                this.storeServicecredit = storeServicecredit;
            }

            public StoreDeliverycreditBean getStoreDeliverycredit() {
                return storeDeliverycredit;
            }

            public void setStoreDeliverycredit(StoreDeliverycreditBean storeDeliverycredit) {
                this.storeDeliverycredit = storeDeliverycredit;
            }

            public static class StoreDesccreditBean {

                @SerializedName("text")
                private String text = "";
                @SerializedName("credit")
                private String credit = "";

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public String getCredit() {
                    return credit;
                }

                public void setCredit(String credit) {
                    this.credit = credit;
                }

            }

            public static class StoreServicecreditBean {

                @SerializedName("text")
                private String text = "";
                @SerializedName("credit")
                private String credit = "";

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public String getCredit() {
                    return credit;
                }

                public void setCredit(String credit) {
                    this.credit = credit;
                }

            }

            public static class StoreDeliverycreditBean {

                @SerializedName("text")
                private String text = "";
                @SerializedName("credit")
                private String credit = "";

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public String getCredit() {
                    return credit;
                }

                public void setCredit(String credit) {
                    this.credit = credit;
                }

            }

        }

        public static class HotSalesListBean {

            @SerializedName("goods_id")
            private String goodsId = "";
            @SerializedName("goods_commonid")
            private String goodsCommonid = "";
            @SerializedName("goods_name")
            private String goodsName = "";
            @SerializedName("goods_jingle")
            private String goodsJingle = "";
            @SerializedName("store_id")
            private String storeId = "";
            @SerializedName("store_name")
            private String storeName = "";
            @SerializedName("gc_id")
            private String gcId = "";
            @SerializedName("gc_id_1")
            private String gcId1 = "";
            @SerializedName("gc_id_2")
            private String gcId2 = "";
            @SerializedName("gc_id_3")
            private String gcId3 = "";
            @SerializedName("brand_id")
            private String brandId = "";
            @SerializedName("goods_price")
            private String goodsPrice = "";
            @SerializedName("goods_sale_price")
            private String goodsSalePrice = "";
            @SerializedName("goods_sale_type")
            private String goodsSaleType = "";
            @SerializedName("goods_marketprice")
            private String goodsMarketprice = "";
            @SerializedName("goods_serial")
            private String goodsSerial = "";
            @SerializedName("goods_storage_alarm")
            private String goodsStorageAlarm = "";
            @SerializedName("goods_barcode")
            private String goodsBarcode = "";
            @SerializedName("goods_click")
            private String goodsClick = "";
            @SerializedName("goods_salenum")
            private String goodsSalenum = "";
            @SerializedName("goods_collect")
            private String goodsCollect = "";
            @SerializedName("spec_name")
            private String specName = "";
            @SerializedName("goods_spec")
            private String goodsSpec = "";
            @SerializedName("goods_storage")
            private String goodsStorage = "";
            @SerializedName("goods_image")
            private String goodsImage = "";
            @SerializedName("goods_body")
            private String goodsBody = "";
            @SerializedName("mobile_body")
            private String mobileBody = "";
            @SerializedName("goods_state")
            private String goodsState = "";
            @SerializedName("goods_verify")
            private String goodsVerify = "";
            @SerializedName("goods_addtime")
            private String goodsAddtime = "";
            @SerializedName("goods_edittime")
            private String goodsEdittime = "";
            @SerializedName("areaid_1")
            private String areaid1 = "";
            @SerializedName("areaid_2")
            private String areaid2 = "";
            @SerializedName("areaid_3")
            private String areaid3 = "";
            @SerializedName("color_id")
            private String colorId = "";
            @SerializedName("transport_id")
            private String transportId = "";
            @SerializedName("goods_freight")
            private String goodsFreight = "";
            @SerializedName("goods_trans_v")
            private String goodsTransV = "";
            @SerializedName("goods_vat")
            private String goodsVat = "";
            @SerializedName("goods_commend")
            private String goodsCommend = "";
            @SerializedName("goods_stcids")
            private String goodsStcids = "";
            @SerializedName("evaluation_good_star")
            private String evaluationGoodStar = "";
            @SerializedName("evaluation_count")
            private String evaluationCount = "";
            @SerializedName("is_virtual")
            private String isVirtual = "";
            @SerializedName("virtual_indate")
            private String virtualIndate = "";
            @SerializedName("virtual_limit")
            private String virtualLimit = "";
            @SerializedName("virtual_invalid_refund")
            private String virtualInvalidRefund = "";
            @SerializedName("is_fcode")
            private String isFcode = "";
            @SerializedName("is_presell")
            private String isPresell = "";
            @SerializedName("presell_deliverdate")
            private String presellDeliverdate = "";
            @SerializedName("is_book")
            private String isBook = "";
            @SerializedName("book_down_payment")
            private String bookDownPayment = "";
            @SerializedName("book_final_payment")
            private String bookFinalPayment = "";
            @SerializedName("book_down_time")
            private String bookDownTime = "";
            @SerializedName("book_buyers")
            private String bookBuyers = "";
            @SerializedName("have_gift")
            private String haveGift = "";
            @SerializedName("is_own_shop")
            private String isOwnShop = "";
            @SerializedName("contract_1")
            private String contract1 = "";
            @SerializedName("contract_2")
            private String contract2 = "";
            @SerializedName("contract_3")
            private String contract3 = "";
            @SerializedName("contract_4")
            private String contract4 = "";
            @SerializedName("contract_5")
            private String contract5 = "";
            @SerializedName("contract_6")
            private String contract6 = "";
            @SerializedName("contract_7")
            private String contract7 = "";
            @SerializedName("contract_8")
            private String contract8 = "";
            @SerializedName("contract_9")
            private String contract9 = "";
            @SerializedName("contract_10")
            private String contract10 = "";
            @SerializedName("is_chain")
            private String isChain = "";
            @SerializedName("invite_rate")
            private String inviteRate = "";
            @SerializedName("is_fx")
            private String isFx = "";
            @SerializedName("is_bat")
            private String isBat = "";

            public String getGoodsId() {
                return goodsId;
            }

            public void setGoodsId(String goodsId) {
                this.goodsId = goodsId;
            }

            public String getGoodsCommonid() {
                return goodsCommonid;
            }

            public void setGoodsCommonid(String goodsCommonid) {
                this.goodsCommonid = goodsCommonid;
            }

            public String getGoodsName() {
                return goodsName;
            }

            public void setGoodsName(String goodsName) {
                this.goodsName = goodsName;
            }

            public String getGoodsJingle() {
                return goodsJingle;
            }

            public void setGoodsJingle(String goodsJingle) {
                this.goodsJingle = goodsJingle;
            }

            public String getStoreId() {
                return storeId;
            }

            public void setStoreId(String storeId) {
                this.storeId = storeId;
            }

            public String getStoreName() {
                return storeName;
            }

            public void setStoreName(String storeName) {
                this.storeName = storeName;
            }

            public String getGcId() {
                return gcId;
            }

            public void setGcId(String gcId) {
                this.gcId = gcId;
            }

            public String getGcId1() {
                return gcId1;
            }

            public void setGcId1(String gcId1) {
                this.gcId1 = gcId1;
            }

            public String getGcId2() {
                return gcId2;
            }

            public void setGcId2(String gcId2) {
                this.gcId2 = gcId2;
            }

            public String getGcId3() {
                return gcId3;
            }

            public void setGcId3(String gcId3) {
                this.gcId3 = gcId3;
            }

            public String getBrandId() {
                return brandId;
            }

            public void setBrandId(String brandId) {
                this.brandId = brandId;
            }

            public String getGoodsPrice() {
                return goodsPrice;
            }

            public void setGoodsPrice(String goodsPrice) {
                this.goodsPrice = goodsPrice;
            }

            public String getGoodsSalePrice() {
                return goodsSalePrice;
            }

            public void setGoodsSalePrice(String goodsSalePrice) {
                this.goodsSalePrice = goodsSalePrice;
            }

            public String getGoodsSaleType() {
                return goodsSaleType;
            }

            public void setGoodsSaleType(String goodsSaleType) {
                this.goodsSaleType = goodsSaleType;
            }

            public String getGoodsMarketprice() {
                return goodsMarketprice;
            }

            public void setGoodsMarketprice(String goodsMarketprice) {
                this.goodsMarketprice = goodsMarketprice;
            }

            public String getGoodsSerial() {
                return goodsSerial;
            }

            public void setGoodsSerial(String goodsSerial) {
                this.goodsSerial = goodsSerial;
            }

            public String getGoodsStorageAlarm() {
                return goodsStorageAlarm;
            }

            public void setGoodsStorageAlarm(String goodsStorageAlarm) {
                this.goodsStorageAlarm = goodsStorageAlarm;
            }

            public String getGoodsBarcode() {
                return goodsBarcode;
            }

            public void setGoodsBarcode(String goodsBarcode) {
                this.goodsBarcode = goodsBarcode;
            }

            public String getGoodsClick() {
                return goodsClick;
            }

            public void setGoodsClick(String goodsClick) {
                this.goodsClick = goodsClick;
            }

            public String getGoodsSalenum() {
                return goodsSalenum;
            }

            public void setGoodsSalenum(String goodsSalenum) {
                this.goodsSalenum = goodsSalenum;
            }

            public String getGoodsCollect() {
                return goodsCollect;
            }

            public void setGoodsCollect(String goodsCollect) {
                this.goodsCollect = goodsCollect;
            }

            public String getSpecName() {
                return specName;
            }

            public void setSpecName(String specName) {
                this.specName = specName;
            }

            public String getGoodsSpec() {
                return goodsSpec;
            }

            public void setGoodsSpec(String goodsSpec) {
                this.goodsSpec = goodsSpec;
            }

            public String getGoodsStorage() {
                return goodsStorage;
            }

            public void setGoodsStorage(String goodsStorage) {
                this.goodsStorage = goodsStorage;
            }

            public String getGoodsImage() {
                return goodsImage;
            }

            public void setGoodsImage(String goodsImage) {
                this.goodsImage = goodsImage;
            }

            public String getGoodsBody() {
                return goodsBody;
            }

            public void setGoodsBody(String goodsBody) {
                this.goodsBody = goodsBody;
            }

            public String getMobileBody() {
                return mobileBody;
            }

            public void setMobileBody(String mobileBody) {
                this.mobileBody = mobileBody;
            }

            public String getGoodsState() {
                return goodsState;
            }

            public void setGoodsState(String goodsState) {
                this.goodsState = goodsState;
            }

            public String getGoodsVerify() {
                return goodsVerify;
            }

            public void setGoodsVerify(String goodsVerify) {
                this.goodsVerify = goodsVerify;
            }

            public String getGoodsAddtime() {
                return goodsAddtime;
            }

            public void setGoodsAddtime(String goodsAddtime) {
                this.goodsAddtime = goodsAddtime;
            }

            public String getGoodsEdittime() {
                return goodsEdittime;
            }

            public void setGoodsEdittime(String goodsEdittime) {
                this.goodsEdittime = goodsEdittime;
            }

            public String getAreaid1() {
                return areaid1;
            }

            public void setAreaid1(String areaid1) {
                this.areaid1 = areaid1;
            }

            public String getAreaid2() {
                return areaid2;
            }

            public void setAreaid2(String areaid2) {
                this.areaid2 = areaid2;
            }

            public String getAreaid3() {
                return areaid3;
            }

            public void setAreaid3(String areaid3) {
                this.areaid3 = areaid3;
            }

            public String getColorId() {
                return colorId;
            }

            public void setColorId(String colorId) {
                this.colorId = colorId;
            }

            public String getTransportId() {
                return transportId;
            }

            public void setTransportId(String transportId) {
                this.transportId = transportId;
            }

            public String getGoodsFreight() {
                return goodsFreight;
            }

            public void setGoodsFreight(String goodsFreight) {
                this.goodsFreight = goodsFreight;
            }

            public String getGoodsTransV() {
                return goodsTransV;
            }

            public void setGoodsTransV(String goodsTransV) {
                this.goodsTransV = goodsTransV;
            }

            public String getGoodsVat() {
                return goodsVat;
            }

            public void setGoodsVat(String goodsVat) {
                this.goodsVat = goodsVat;
            }

            public String getGoodsCommend() {
                return goodsCommend;
            }

            public void setGoodsCommend(String goodsCommend) {
                this.goodsCommend = goodsCommend;
            }

            public String getGoodsStcids() {
                return goodsStcids;
            }

            public void setGoodsStcids(String goodsStcids) {
                this.goodsStcids = goodsStcids;
            }

            public String getEvaluationGoodStar() {
                return evaluationGoodStar;
            }

            public void setEvaluationGoodStar(String evaluationGoodStar) {
                this.evaluationGoodStar = evaluationGoodStar;
            }

            public String getEvaluationCount() {
                return evaluationCount;
            }

            public void setEvaluationCount(String evaluationCount) {
                this.evaluationCount = evaluationCount;
            }

            public String getIsVirtual() {
                return isVirtual;
            }

            public void setIsVirtual(String isVirtual) {
                this.isVirtual = isVirtual;
            }

            public String getVirtualIndate() {
                return virtualIndate;
            }

            public void setVirtualIndate(String virtualIndate) {
                this.virtualIndate = virtualIndate;
            }

            public String getVirtualLimit() {
                return virtualLimit;
            }

            public void setVirtualLimit(String virtualLimit) {
                this.virtualLimit = virtualLimit;
            }

            public String getVirtualInvalidRefund() {
                return virtualInvalidRefund;
            }

            public void setVirtualInvalidRefund(String virtualInvalidRefund) {
                this.virtualInvalidRefund = virtualInvalidRefund;
            }

            public String getIsFcode() {
                return isFcode;
            }

            public void setIsFcode(String isFcode) {
                this.isFcode = isFcode;
            }

            public String getIsPresell() {
                return isPresell;
            }

            public void setIsPresell(String isPresell) {
                this.isPresell = isPresell;
            }

            public String getPresellDeliverdate() {
                return presellDeliverdate;
            }

            public void setPresellDeliverdate(String presellDeliverdate) {
                this.presellDeliverdate = presellDeliverdate;
            }

            public String getIsBook() {
                return isBook;
            }

            public void setIsBook(String isBook) {
                this.isBook = isBook;
            }

            public String getBookDownPayment() {
                return bookDownPayment;
            }

            public void setBookDownPayment(String bookDownPayment) {
                this.bookDownPayment = bookDownPayment;
            }

            public String getBookFinalPayment() {
                return bookFinalPayment;
            }

            public void setBookFinalPayment(String bookFinalPayment) {
                this.bookFinalPayment = bookFinalPayment;
            }

            public String getBookDownTime() {
                return bookDownTime;
            }

            public void setBookDownTime(String bookDownTime) {
                this.bookDownTime = bookDownTime;
            }

            public String getBookBuyers() {
                return bookBuyers;
            }

            public void setBookBuyers(String bookBuyers) {
                this.bookBuyers = bookBuyers;
            }

            public String getHaveGift() {
                return haveGift;
            }

            public void setHaveGift(String haveGift) {
                this.haveGift = haveGift;
            }

            public String getIsOwnShop() {
                return isOwnShop;
            }

            public void setIsOwnShop(String isOwnShop) {
                this.isOwnShop = isOwnShop;
            }

            public String getContract1() {
                return contract1;
            }

            public void setContract1(String contract1) {
                this.contract1 = contract1;
            }

            public String getContract2() {
                return contract2;
            }

            public void setContract2(String contract2) {
                this.contract2 = contract2;
            }

            public String getContract3() {
                return contract3;
            }

            public void setContract3(String contract3) {
                this.contract3 = contract3;
            }

            public String getContract4() {
                return contract4;
            }

            public void setContract4(String contract4) {
                this.contract4 = contract4;
            }

            public String getContract5() {
                return contract5;
            }

            public void setContract5(String contract5) {
                this.contract5 = contract5;
            }

            public String getContract6() {
                return contract6;
            }

            public void setContract6(String contract6) {
                this.contract6 = contract6;
            }

            public String getContract7() {
                return contract7;
            }

            public void setContract7(String contract7) {
                this.contract7 = contract7;
            }

            public String getContract8() {
                return contract8;
            }

            public void setContract8(String contract8) {
                this.contract8 = contract8;
            }

            public String getContract9() {
                return contract9;
            }

            public void setContract9(String contract9) {
                this.contract9 = contract9;
            }

            public String getContract10() {
                return contract10;
            }

            public void setContract10(String contract10) {
                this.contract10 = contract10;
            }

            public String getIsChain() {
                return isChain;
            }

            public void setIsChain(String isChain) {
                this.isChain = isChain;
            }

            public String getInviteRate() {
                return inviteRate;
            }

            public void setInviteRate(String inviteRate) {
                this.inviteRate = inviteRate;
            }

            public String getIsFx() {
                return isFx;
            }

            public void setIsFx(String isFx) {
                this.isFx = isFx;
            }

            public String getIsBat() {
                return isBat;
            }

            public void setIsBat(String isBat) {
                this.isBat = isBat;
            }

        }

    }

}
