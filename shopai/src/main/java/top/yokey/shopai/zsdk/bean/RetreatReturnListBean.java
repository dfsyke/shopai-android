package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class RetreatReturnListBean implements Serializable {

    @SerializedName("refund_id")
    private String refundId = "";
    @SerializedName("goods_id")
    private String goodsId = "";
    @SerializedName("goods_name")
    private String goodsName = "";
    @SerializedName("goods_spec")
    private String goodsSpec = "";
    @SerializedName("goods_num")
    private String goodsNum = "";
    @SerializedName("goods_state_v")
    private String goodsStateV = "";
    @SerializedName("ship_state")
    private String shipState = "";
    @SerializedName("delay_state")
    private String delayState = "";
    @SerializedName("order_id")
    private String orderId = "";
    @SerializedName("refund_amount")
    private String refundAmount = "";
    @SerializedName("refund_sn")
    private String refundSn = "";
    @SerializedName("return_type")
    private String returnType = "";
    @SerializedName("order_sn")
    private String orderSn = "";
    @SerializedName("add_time")
    private String addTime = "";
    @SerializedName("goods_img_360")
    private String goodsImg360 = "";
    @SerializedName("seller_state_v")
    private String sellerStateV = "";
    @SerializedName("seller_state")
    private String sellerState = "";
    @SerializedName("admin_state_v")
    private String adminStateV = "";
    @SerializedName("admin_state")
    private String adminState = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("store_name")
    private String storeName = "";

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsSpec() {
        return goodsSpec;
    }

    public void setGoodsSpec(String goodsSpec) {
        this.goodsSpec = goodsSpec;
    }

    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getGoodsStateV() {
        return goodsStateV;
    }

    public void setGoodsStateV(String goodsStateV) {
        this.goodsStateV = goodsStateV;
    }

    public String getShipState() {
        return shipState;
    }

    public void setShipState(String shipState) {
        this.shipState = shipState;
    }

    public String getDelayState() {
        return delayState;
    }

    public void setDelayState(String delayState) {
        this.delayState = delayState;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getRefundSn() {
        return refundSn;
    }

    public void setRefundSn(String refundSn) {
        this.refundSn = refundSn;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getGoodsImg360() {
        return goodsImg360;
    }

    public void setGoodsImg360(String goodsImg360) {
        this.goodsImg360 = goodsImg360;
    }

    public String getSellerStateV() {
        return sellerStateV;
    }

    public void setSellerStateV(String sellerStateV) {
        this.sellerStateV = sellerStateV;
    }

    public String getSellerState() {
        return sellerState;
    }

    public void setSellerState(String sellerState) {
        this.sellerState = sellerState;
    }

    public String getAdminStateV() {
        return adminStateV;
    }

    public void setAdminStateV(String adminStateV) {
        this.adminStateV = adminStateV;
    }

    public String getAdminState() {
        return adminState;
    }

    public void setAdminState(String adminState) {
        this.adminState = adminState;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

}
