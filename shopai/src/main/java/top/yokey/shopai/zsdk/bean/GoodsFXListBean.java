package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class GoodsFXListBean implements Serializable {

    @SerializedName("fx_id")
    private String fxId = "";
    @SerializedName("goods_commonid")
    private String goodsCommonid = "";
    @SerializedName("goods_name")
    private String goodsName = "";
    @SerializedName("goods_price")
    private String goodsPrice = "";
    @SerializedName("fx_time")
    private String fxTime = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("fx_commis_rate")
    private String fxCommisRate = "";
    @SerializedName("goods_image_url")
    private String goodsImageUrl = "";

    public String getFxId() {
        return fxId;
    }

    public void setFxId(String fxId) {
        this.fxId = fxId;
    }

    public String getGoodsCommonid() {
        return goodsCommonid;
    }

    public void setGoodsCommonid(String goodsCommonid) {
        this.goodsCommonid = goodsCommonid;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getFxTime() {
        return fxTime;
    }

    public void setFxTime(String fxTime) {
        this.fxTime = fxTime;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getFxCommisRate() {
        return fxCommisRate;
    }

    public void setFxCommisRate(String fxCommisRate) {
        this.fxCommisRate = fxCommisRate;
    }

    public String getGoodsImageUrl() {
        return goodsImageUrl;
    }

    public void setGoodsImageUrl(String goodsImageUrl) {
        this.goodsImageUrl = goodsImageUrl;
    }

}
