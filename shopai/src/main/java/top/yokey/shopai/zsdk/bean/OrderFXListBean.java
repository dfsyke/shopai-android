package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class OrderFXListBean implements Serializable {

    @SerializedName("goods_id")
    private String goodsId = "";
    @SerializedName("goods_name")
    private String goodsName = "";
    @SerializedName("goods_price")
    private String goodsPrice = "";
    @SerializedName("goods_num")
    private String goodsNum = "";
    @SerializedName("order_sn")
    private String orderSn = "";
    @SerializedName("goods_pay_price")
    private String goodsPayPrice = "";
    @SerializedName("fx_commis_rate")
    private String fxCommisRate = "";
    @SerializedName("add_time")
    private String addTime = "";
    @SerializedName("store_id")
    private String storeId = "";
    @SerializedName("store_name")
    private String storeName = "";
    @SerializedName("goods_image_url")
    private String goodsImageUrl = "";
    @SerializedName("fx_commis_amount")
    private String fxCommisAmount = "";
    @SerializedName("order_state")
    private String orderState = "";
    @SerializedName("order_state_txt")
    private String orderStateTxt = "";

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getGoodsPayPrice() {
        return goodsPayPrice;
    }

    public void setGoodsPayPrice(String goodsPayPrice) {
        this.goodsPayPrice = goodsPayPrice;
    }

    public String getFxCommisRate() {
        return fxCommisRate;
    }

    public void setFxCommisRate(String fxCommisRate) {
        this.fxCommisRate = fxCommisRate;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getGoodsImageUrl() {
        return goodsImageUrl;
    }

    public void setGoodsImageUrl(String goodsImageUrl) {
        this.goodsImageUrl = goodsImageUrl;
    }

    public String getFxCommisAmount() {
        return fxCommisAmount;
    }

    public void setFxCommisAmount(String fxCommisAmount) {
        this.fxCommisAmount = fxCommisAmount;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public String getOrderStateTxt() {
        return orderStateTxt;
    }

    public void setOrderStateTxt(String orderStateTxt) {
        this.orderStateTxt = orderStateTxt;
    }

}
