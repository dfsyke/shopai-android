package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class StoreSaleBean implements Serializable {

    @SerializedName("mansong")
    private MansongBean mansong = null;
    @SerializedName("xianshi")
    private XianshiBean xianshi = null;

    public MansongBean getMansong() {
        return mansong;
    }

    public void setMansong(MansongBean mansong) {
        this.mansong = mansong;
    }

    public XianshiBean getXianshi() {
        return xianshi;
    }

    public void setXianshi(XianshiBean xianshi) {
        this.xianshi = xianshi;
    }

    public static class MansongBean {

        @SerializedName("mansong_id")
        private String mansongId = "";
        @SerializedName("mansong_name")
        private String mansongName = "";
        @SerializedName("quota_id")
        private String quotaId = "";
        @SerializedName("start_time")
        private String startTime = "";
        @SerializedName("end_time")
        private String endTime = "";
        @SerializedName("member_id")
        private String memberId = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("member_name")
        private String memberName = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("state")
        private String state = "";
        @SerializedName("remark")
        private String remark = "";
        @SerializedName("mansong_state_text")
        private String mansongStateText = "";
        @SerializedName("editable")
        private boolean editable = false;
        @SerializedName("start_time_text")
        private String startTimeText = "";
        @SerializedName("end_time_text")
        private String endTimeText = "";
        @SerializedName("rules")
        private ArrayList<RulesBean> rules = null;

        public String getMansongId() {
            return mansongId;
        }

        public void setMansongId(String mansongId) {
            this.mansongId = mansongId;
        }

        public String getMansongName() {
            return mansongName;
        }

        public void setMansongName(String mansongName) {
            this.mansongName = mansongName;
        }

        public String getQuotaId() {
            return quotaId;
        }

        public void setQuotaId(String quotaId) {
            this.quotaId = quotaId;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getMemberName() {
            return memberName;
        }

        public void setMemberName(String memberName) {
            this.memberName = memberName;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getMansongStateText() {
            return mansongStateText;
        }

        public void setMansongStateText(String mansongStateText) {
            this.mansongStateText = mansongStateText;
        }

        public boolean isEditable() {
            return editable;
        }

        public void setEditable(boolean editable) {
            this.editable = editable;
        }

        public String getStartTimeText() {
            return startTimeText;
        }

        public void setStartTimeText(String startTimeText) {
            this.startTimeText = startTimeText;
        }

        public String getEndTimeText() {
            return endTimeText;
        }

        public void setEndTimeText(String endTimeText) {
            this.endTimeText = endTimeText;
        }

        public ArrayList<RulesBean> getRules() {
            return rules;
        }

        public void setRules(ArrayList<RulesBean> rules) {
            this.rules = rules;
        }

        public class RulesBean {

            @SerializedName("rule_id")
            private String ruleId = "";
            @SerializedName("mansong_id")
            private String mansongId = "";
            @SerializedName("price")
            private String price = "";
            @SerializedName("discount")
            private String discount = "";
            @SerializedName("mansong_goods_name")
            private String mansongGoodsName = "";
            @SerializedName("goods_id")
            private String goodsId = "";
            @SerializedName("goods_image")
            private String goodsImage = "";
            @SerializedName("goods_image_url")
            private String goodsImageUrl = "";
            @SerializedName("goods_storage")
            private String goodsStorage = "";
            @SerializedName("goods_url")
            private String goodsUrl = "";

            public String getRuleId() {
                return ruleId;
            }

            public void setRuleId(String ruleId) {
                this.ruleId = ruleId;
            }

            public String getMansongId() {
                return mansongId;
            }

            public void setMansongId(String mansongId) {
                this.mansongId = mansongId;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public String getMansongGoodsName() {
                return mansongGoodsName;
            }

            public void setMansongGoodsName(String mansongGoodsName) {
                this.mansongGoodsName = mansongGoodsName;
            }

            public String getGoodsId() {
                return goodsId;
            }

            public void setGoodsId(String goodsId) {
                this.goodsId = goodsId;
            }

            public String getGoodsImage() {
                return goodsImage;
            }

            public void setGoodsImage(String goodsImage) {
                this.goodsImage = goodsImage;
            }

            public String getGoodsImageUrl() {
                return goodsImageUrl;
            }

            public void setGoodsImageUrl(String goodsImageUrl) {
                this.goodsImageUrl = goodsImageUrl;
            }

            public String getGoodsStorage() {
                return goodsStorage;
            }

            public void setGoodsStorage(String goodsStorage) {
                this.goodsStorage = goodsStorage;
            }

            public String getGoodsUrl() {
                return goodsUrl;
            }

            public void setGoodsUrl(String goodsUrl) {
                this.goodsUrl = goodsUrl;
            }

        }

    }

    public static class XianshiBean {

        @SerializedName("xianshi_id")
        private String xianshiId = "";
        @SerializedName("xianshi_name")
        private String xianshiName = "";
        @SerializedName("xianshi_title")
        private String xianshiTitle = "";
        @SerializedName("xianshi_explain")
        private String xianshiExplain = "";
        @SerializedName("quota_id")
        private String quotaId = "";
        @SerializedName("start_time")
        private String startTime = "";
        @SerializedName("end_time")
        private String endTime = "";
        @SerializedName("member_id")
        private String memberId = "";
        @SerializedName("store_id")
        private String storeId = "";
        @SerializedName("member_name")
        private String memberName = "";
        @SerializedName("store_name")
        private String storeName = "";
        @SerializedName("lower_limit")
        private String lowerLimit = "";
        @SerializedName("state")
        private String state = "";
        @SerializedName("xianshi_state_text")
        private String xianshiStateText = "";
        @SerializedName("editable")
        private boolean editable = false;
        @SerializedName("start_time_text")
        private String startTimeText = "";
        @SerializedName("end_time_text")
        private String endTimeText = "";

        public String getXianshiId() {
            return xianshiId;
        }

        public void setXianshiId(String xianshiId) {
            this.xianshiId = xianshiId;
        }

        public String getXianshiName() {
            return xianshiName;
        }

        public void setXianshiName(String xianshiName) {
            this.xianshiName = xianshiName;
        }

        public String getXianshiTitle() {
            return xianshiTitle;
        }

        public void setXianshiTitle(String xianshiTitle) {
            this.xianshiTitle = xianshiTitle;
        }

        public String getXianshiExplain() {
            return xianshiExplain;
        }

        public void setXianshiExplain(String xianshiExplain) {
            this.xianshiExplain = xianshiExplain;
        }

        public String getQuotaId() {
            return quotaId;
        }

        public void setQuotaId(String quotaId) {
            this.quotaId = quotaId;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getStoreId() {
            return storeId;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getMemberName() {
            return memberName;
        }

        public void setMemberName(String memberName) {
            this.memberName = memberName;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getLowerLimit() {
            return lowerLimit;
        }

        public void setLowerLimit(String lowerLimit) {
            this.lowerLimit = lowerLimit;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getXianshiStateText() {
            return xianshiStateText;
        }

        public void setXianshiStateText(String xianshiStateText) {
            this.xianshiStateText = xianshiStateText;
        }

        public boolean isEditable() {
            return editable;
        }

        public void setEditable(boolean editable) {
            this.editable = editable;
        }

        public String getStartTimeText() {
            return startTimeText;
        }

        public void setStartTimeText(String startTimeText) {
            this.startTimeText = startTimeText;
        }

        public String getEndTimeText() {
            return endTimeText;
        }

        public void setEndTimeText(String endTimeText) {
            this.endTimeText = endTimeText;
        }

    }

}
