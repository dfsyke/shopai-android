package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class MemberFXAddBean implements Serializable {

    @SerializedName("goods_name")
    private String goodsName = "";
    @SerializedName("goods_image")
    private String goodsImage = "";
    @SerializedName("goods_price")
    private String goodsPrice = "";
    @SerializedName("fx_id")
    private String fxId = "";
    @SerializedName("fx_url")
    private String fxUrl = "";

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getFxId() {
        return fxId;
    }

    public void setFxId(String fxId) {
        this.fxId = fxId;
    }

    public String getFxUrl() {
        return fxUrl;
    }

    public void setFxUrl(String fxUrl) {
        this.fxUrl = fxUrl;
    }

}
