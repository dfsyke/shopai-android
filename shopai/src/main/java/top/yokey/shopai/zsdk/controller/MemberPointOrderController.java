package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.PointOrderBean;
import top.yokey.shopai.zsdk.bean.PointOrderListBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class MemberPointOrderController {

    private static final String ACT = "member_pointorder";

    public static void cancelOrder(String orderId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "cancel_order")
                .add("order_id", orderId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void receivingOrder(String orderId, HttpCallBack<String> httpCallBack) {

        ShopAISdk.get().ready(ACT, "receiving_order")
                .add("order_id", orderId)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, baseBean.getDatas());
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void orderList(String page, HttpCallBack<ArrayList<PointOrderListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "orderlist")
                .add("page", ShopAISdk.get().getPageNumber())
                .add("curpage", page)
                .post(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "order_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, PointOrderListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

    public static void orderInfo(String orderId, HttpCallBack<PointOrderBean> httpCallBack) {

        ShopAISdk.get().ready(ACT, "order_info")
                .add("order_id", orderId)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2Object(baseBean.getDatas(), PointOrderBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
