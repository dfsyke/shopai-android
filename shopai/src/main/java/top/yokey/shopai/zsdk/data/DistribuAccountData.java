package top.yokey.shopai.zsdk.data;

import java.io.Serializable;

@SuppressWarnings("ALL")
public class DistribuAccountData implements Serializable {

    private String billUserName = "";
    private String billTypeNumber = "";
    private String billBankName = "";
    private String billTypeCode = "";

    public String getBillUserName() {
        return billUserName;
    }

    public void setBillUserName(String billUserName) {
        this.billUserName = billUserName;
    }

    public String getBillTypeNumber() {
        return billTypeNumber;
    }

    public void setBillTypeNumber(String billTypeNumber) {
        this.billTypeNumber = billTypeNumber;
    }

    public String getBillBankName() {
        return billBankName;
    }

    public void setBillBankName(String billBankName) {
        this.billBankName = billBankName;
    }

    public String getBillTypeCode() {
        return billTypeCode;
    }

    public void setBillTypeCode(String billTypeCode) {
        this.billTypeCode = billTypeCode;
    }

}
