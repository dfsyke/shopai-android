package top.yokey.shopai.zsdk.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("ALL")
public class StoreGoodsClassBean implements Serializable {

    @SerializedName("id")
    private String id = "";
    @SerializedName("name")
    private String name = "";
    @SerializedName("level")
    private String level = "";
    @SerializedName("pid")
    private String pid = "";
    private ArrayList<StoreGoodsClassBean> child = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public ArrayList<StoreGoodsClassBean> getChild() {
        return child;
    }

    public void setChild(ArrayList<StoreGoodsClassBean> child) {
        this.child = child;
    }

}
