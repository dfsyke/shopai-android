package top.yokey.shopai.zsdk.controller;

import java.util.ArrayList;

import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zsdk.ShopAISdk;
import top.yokey.shopai.zsdk.bean.AreaListBean;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.BaseCallBack;
import top.yokey.shopai.zsdk.callback.HttpCallBack;

@SuppressWarnings("ALL")
public class AreaController {

    private static final String ACT = "area";

    public static void areaList(String areaId, HttpCallBack<ArrayList<AreaListBean>> httpCallBack) {

        ShopAISdk.get().ready(ACT, "area_list")
                .add("area_id", areaId)
                .get(new BaseCallBack() {
                    @Override
                    public void onSuccess(String result, BaseBean baseBean) {
                        String data = JsonUtil.getString(baseBean.getDatas(), "area_list");
                        httpCallBack.onSuccess(result, baseBean, JsonUtil.json2ArrayList(data, AreaListBean.class));
                    }

                    @Override
                    public void onFailure(String reason) {
                        httpCallBack.onFailure(reason);
                    }
                });

    }

}
