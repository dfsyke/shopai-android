package top.yokey.shopai.retreat.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.RetreatReturnListBean;

public class RetreatReturnListAdapter extends RecyclerView.Adapter<RetreatReturnListAdapter.ViewHolder> {

    private final ArrayList<RetreatReturnListBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public RetreatReturnListAdapter(ArrayList<RetreatReturnListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        RetreatReturnListBean bean = arrayList.get(position);
        holder.nameTextView.setText(bean.getStoreName());
        ImageHelp.get().displayRadius(bean.getGoodsImg360(), holder.goodsImageView);
        holder.goodsNameTextView.setText(bean.getGoodsName());
        holder.timeTextView.setText(bean.getAddTime());
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getRefundAmount());
        holder.numberTextView.setText("x");
        holder.numberTextView.append(bean.getGoodsNum());
        holder.operaTextView.setText(R.string.returnDetailed);
        if (bean.getShipState().equals("1")) {
            holder.operaTextView.setText(R.string.returnDeliver);
        }
        if (bean.getAdminState().equals("无")) {
            holder.descTextView.setText(bean.getSellerState());
        } else {
            holder.descTextView.setText(bean.getAdminState());
        }

        holder.goodsRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickGoods(position, bean);
            }
        });

        holder.operaTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onOpera(position, bean);
            }
        });

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_retreat_return_list, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, RetreatReturnListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onOpera(int position, RetreatReturnListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickGoods(int position, RetreatReturnListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView descTextView;
        private final RelativeLayout goodsRelativeLayout;
        private final AppCompatImageView goodsImageView;
        private final AppCompatTextView goodsNameTextView;
        private final AppCompatTextView timeTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView numberTextView;
        private final AppCompatTextView operaTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            nameTextView = view.findViewById(R.id.nameTextView);
            descTextView = view.findViewById(R.id.descTextView);
            goodsRelativeLayout = view.findViewById(R.id.goodsRelativeLayout);
            goodsImageView = view.findViewById(R.id.goodsImageView);
            goodsNameTextView = view.findViewById(R.id.goodsNameTextView);
            timeTextView = view.findViewById(R.id.timeTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            numberTextView = view.findViewById(R.id.numberTextView);
            operaTextView = view.findViewById(R.id.operaTextView);

        }

    }

}
