package top.yokey.shopai.retreat.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.RetreatRefundInfoBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberRefundController;

public class RetreatRefundInfoVM extends BaseViewModel {

    private final MutableLiveData<RetreatRefundInfoBean> infoLiveData = new MutableLiveData<>();

    public RetreatRefundInfoVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<RetreatRefundInfoBean> getInfoLiveData() {

        return infoLiveData;

    }

    public void getInfo(String refundId) {

        MemberRefundController.getRefundInfo(refundId, new HttpCallBack<RetreatRefundInfoBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, RetreatRefundInfoBean bean) {
                infoLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
