package top.yokey.shopai.main.adapter;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.view.CountdownTextView;
import top.yokey.shopai.zsdk.bean.SpecialBean;
import top.yokey.shopai.zsdk.data.GoodsData;

class MainSpecialGoods1Adapter extends RecyclerView.Adapter<MainSpecialGoods1Adapter.ViewHolder> {

    private final ArrayList<SpecialBean.Goods1Bean.ItemBean> arrayList;

    MainSpecialGoods1Adapter(ArrayList<SpecialBean.Goods1Bean.ItemBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SpecialBean.Goods1Bean.ItemBean bean = arrayList.get(position);
        ImageHelp.get().displayRadius(bean.getGoodsImage(), holder.mainImageView);
        holder.nameTextView.setText(bean.getGoodsName());
        holder.priceTextView.setText(App.get().getString(R.string.rmb));
        holder.priceTextView.append(bean.getXianshiPrice());
        holder.priceSubTextView.setText(App.get().getString(R.string.rmb));
        holder.priceSubTextView.append(bean.getGoodsPrice());
        holder.priceSubTextView.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        long end = ConvertUtil.string2Long(bean.getEndTime());
        long start = ConvertUtil.string2Long(bean.getStartTime());
        holder.timeTextView.init("", end - start, App.get().getString(R.string.surplus) + "：", "");
        holder.timeTextView.start(0);

        holder.mainRelativeLayout.setOnClickListener(view -> App.get().startGoods(new GoodsData(bean.getGoodsId())));

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_main_special_goods1, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView priceSubTextView;
        private final CountdownTextView timeTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            priceSubTextView = view.findViewById(R.id.priceSubTextView);
            timeTextView = view.findViewById(R.id.timeTextView);

        }

    }

}
