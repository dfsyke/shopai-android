package top.yokey.shopai.main.activity;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.main.adapter.MainCouponAdapter;
import top.yokey.shopai.main.adapter.MainVoucherAdapter;
import top.yokey.shopai.main.viewmodel.MainVoucherVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.CouponListBean;
import top.yokey.shopai.zsdk.bean.VoucherListBean;

@Route(path = ARoutePath.MAIN_VOUCHER)
public class MainVoucherActivity extends BaseActivity {

    private final ArrayList<CouponListBean> couponArrayList = new ArrayList<>();
    private final MainCouponAdapter couponAdapter = new MainCouponAdapter(couponArrayList);
    private final ArrayList<VoucherListBean> voucherArrayList = new ArrayList<>();
    private final MainVoucherAdapter voucherAdapter = new MainVoucherAdapter(voucherArrayList);
    @Autowired(name = Constant.DATA_POSITION)
    int position = 1;
    private Toolbar mainToolbar = null;
    private AppCompatTextView leftTextView = null;
    private AppCompatTextView rightTextView = null;
    private PullRefreshView mainPullRefreshView = null;
    private int page = 1;
    private MainVoucherVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_main_voucher);
        mainToolbar = findViewById(R.id.mainToolbar);
        leftTextView = findViewById(R.id.leftTextView);
        rightTextView = findViewById(R.id.rightTextView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainLinearLayout);
        leftTextView.setText(R.string.platformCoupon);
        rightTextView.setText(R.string.storeVoucher);
        mainPullRefreshView.setItemDecoration(new LineDecoration(App.get().dp2Px(12), false));
        vm = getVM(MainVoucherVM.class);
        if (position == 1) clickLeft();
        if (position == 2) clickRight();

    }

    @Override
    public void initEvent() {

        leftTextView.setOnClickListener(view -> clickLeft());

        rightTextView.setOnClickListener(view -> clickRight());

        mainPullRefreshView.setOnClickListener(view -> {
            if (position == 1) {
                if (mainPullRefreshView.isError() || couponArrayList.size() == 0) {
                    page = 1;
                    getData();
                }
            } else {
                if (mainPullRefreshView.isError() || voucherArrayList.size() == 0) {
                    page = 1;
                    getData();
                }
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

        couponAdapter.setOnItemClickListener((position, bean) -> vm.couponFreeex(bean.getCouponTId()));

        voucherAdapter.setOnItemClickListener((position, bean) -> vm.voucherFreeex(bean.getVoucherTId()));

    }

    @Override
    public void initObserve() {

        vm.getCouponLiveData().observe(this, list -> {
            if (page == 1) couponArrayList.clear();
            couponArrayList.addAll(list);
            mainPullRefreshView.setComplete();
            page++;
        });

        vm.getVoucherLiveData().observe(this, list -> {
            if (page == 1) voucherArrayList.clear();
            voucherArrayList.addAll(list);
            mainPullRefreshView.setComplete();
            page++;
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        Observer<String> observer = string -> ToastHelp.get().showSuccess();

        vm.getCouponFreeexLiveData().observe(this, observer);

        vm.getVoucherFreeexLiveData().observe(this, observer);

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (couponArrayList.size() == 0) {
                    mainPullRefreshView.setError(bean.getReason());
                } else {
                    ToastHelp.get().show(bean.getReason());
                }
                return;
            }
            if (bean.getCode() == 2) {
                if (voucherArrayList.size() == 0) {
                    mainPullRefreshView.setError(bean.getReason());
                } else {
                    ToastHelp.get().show(bean.getReason());
                }
                return;
            }
            ToastHelp.get().show(bean.getReason());
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        if (position == 1) vm.getCoupon(page + "");
        if (position == 2) vm.getVoucher(page + "");

    }

    private void clickLeft() {

        leftTextView.setTextColor(App.get().getColors(R.color.primary));
        leftTextView.setBackgroundResource(R.drawable.selector_switch_left_press);
        rightTextView.setTextColor(App.get().getColors(R.color.accent));
        rightTextView.setBackgroundResource(R.drawable.selector_switch_right);
        mainPullRefreshView.setAdapter(couponAdapter);
        mainPullRefreshView.setComplete();
        position = 1;
        page = 1;
        getData();

    }

    private void clickRight() {

        leftTextView.setTextColor(App.get().getColors(R.color.accent));
        leftTextView.setBackgroundResource(R.drawable.selector_switch_left);
        rightTextView.setTextColor(App.get().getColors(R.color.primary));
        rightTextView.setBackgroundResource(R.drawable.selector_switch_right_press);
        mainPullRefreshView.setAdapter(voucherAdapter);
        mainPullRefreshView.setComplete();
        position = 2;
        page = 1;
        getData();

    }

}
