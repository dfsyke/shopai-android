package top.yokey.shopai.main.activity;

import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.main.adapter.MainSpecialAdapter;
import top.yokey.shopai.main.viewmodel.MainSpecialVM;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.SpecialBean;

@Route(path = ARoutePath.MAIN_SPECIAL)
public class MainSpecialActivity extends BaseActivity {

    private final ArrayList<SpecialBean> arrayList = new ArrayList<>();
    private final MainSpecialAdapter adapter = new MainSpecialAdapter(arrayList);
    @Autowired(name = Constant.DATA_ID)
    String specialId;
    private Toolbar mainToolbar = null;
    private PullRefreshView mainPullRefreshView = null;
    private MainSpecialVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(specialId)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        setToolbar(mainToolbar);
        observeKeyborad(R.id.mainLinearLayout);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setCanLoadMore(false);
        vm = getVM(MainSpecialVM.class);
        getData();

    }

    @Override
    public void initEvent() {

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }

            @Override
            public void onLoadMore() {

            }
        });

    }

    @Override
    public void initObserve() {

        vm.getTitleLiveData().observe(this, string -> setToolbar(mainToolbar, string));

        vm.getSpecialLiveData().observe(this, list -> {
            arrayList.clear();
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getErrorLiveData().observe(this, bean -> DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> getData()));

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getSpecial(specialId);

    }

}
