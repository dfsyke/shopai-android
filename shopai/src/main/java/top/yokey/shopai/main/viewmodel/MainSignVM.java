package top.yokey.shopai.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberSigninController;

public class MainSignVM extends BaseViewModel {

    private final MutableLiveData<String> checkLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> signLiveData = new MutableLiveData<>();

    public MainSignVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getCheckLiveData() {

        return checkLiveData;

    }

    public MutableLiveData<String> getSignLiveData() {

        return signLiveData;

    }

    public void check() {

        MemberSigninController.checksignin(new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                checkLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void sign() {

        MemberSigninController.signinAdd(new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                signLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
