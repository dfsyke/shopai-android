package top.yokey.shopai.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.CartBean;
import top.yokey.shopai.zsdk.bean.CartEditQuantityBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberCartController;
import top.yokey.shopai.zsdk.data.CommonData;

public class MainCartVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<CartBean>> cartLiveData = new MutableLiveData<>();
    private final MutableLiveData<CommonData> cartDelLiveData = new MutableLiveData<>();
    private final MutableLiveData<CommonData> cartAddLiveData = new MutableLiveData<>();
    private final MutableLiveData<CommonData> cartSubLiveData = new MutableLiveData<>();

    public MainCartVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<CartBean>> getCartLiveData() {

        return cartLiveData;

    }

    public MutableLiveData<CommonData> getCartDelLiveData() {

        return cartDelLiveData;

    }

    public MutableLiveData<CommonData> getCartAddLiveData() {

        return cartAddLiveData;

    }

    public MutableLiveData<CommonData> getCartSubLiveData() {

        return cartSubLiveData;

    }

    public void getCart() {

        MemberCartController.cartList(new HttpCallBack<ArrayList<CartBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<CartBean> list) {
                cartLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void delCart(int position, int positionGoods, String cartId) {

        MemberCartController.cartDel(cartId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                CommonData data = new CommonData();
                data.setPos(position);
                data.setPosGoods(positionGoods);
                data.setData(string);
                cartDelLiveData.setValue(data);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void addCart(int position, int positionGoods, String cartId, String goodsId, String quantity) {

        MemberCartController.cartEditQuantity(cartId, goodsId, quantity, new HttpCallBack<CartEditQuantityBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, CartEditQuantityBean bean) {
                CommonData data = new CommonData();
                data.setPos(position);
                data.setPosGoods(positionGoods);
                data.setData(quantity);
                cartAddLiveData.setValue(data);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

    public void subCart(int position, int positionGoods, String cartId, String goodsId, String quantity) {

        MemberCartController.cartEditQuantity(cartId, goodsId, quantity, new HttpCallBack<CartEditQuantityBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, CartEditQuantityBean bean) {
                CommonData data = new CommonData();
                data.setPos(position);
                data.setPosGoods(positionGoods);
                data.setData(quantity);
                cartSubLiveData.setValue(data);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(4, reason));
            }
        });

    }

}
