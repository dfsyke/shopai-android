package top.yokey.shopai.main.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.AndroidConfigBean;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.AppController;
import top.yokey.shopai.zsdk.controller.MemberIndexController;

public class MainLoadVM extends BaseViewModel {

    private final MutableLiveData<AndroidConfigBean> androidConfigLiveData = new MutableLiveData<>();
    private final MutableLiveData<MemberBean> memberLiveData = new MutableLiveData<>();

    public MainLoadVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<AndroidConfigBean> getAndroidConfigLiveData() {

        return androidConfigLiveData;

    }

    public MutableLiveData<MemberBean> getMemberLiveData() {

        return memberLiveData;

    }

    public void getAndroidConfig() {

        AppController.getAndroid(new HttpCallBack<AndroidConfigBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, AndroidConfigBean bean) {
                androidConfigLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getMember() {

        MemberIndexController.index(new HttpCallBack<MemberBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberBean bean) {
                memberLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
