package top.yokey.shopai.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.CartBean;

class MainCartGoodsAdapter extends RecyclerView.Adapter<MainCartGoodsAdapter.ViewHolder> {

    private final ArrayList<CartBean.GoodsBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    MainCartGoodsAdapter(ArrayList<CartBean.GoodsBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CartBean.GoodsBean bean = arrayList.get(position);
        holder.mainCheckBox.setChecked(bean.isSelect());
        ImageHelp.get().displayRadius(bean.getGoodsImageUrl(), holder.mainImageView);
        holder.nameTextView.setText(bean.getGoodsName());
        holder.specTextView.setText(bean.getGoodsSpec());
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getGoodsPrice());
        holder.numberEditText.setText(bean.getGoodsNum());
        holder.mobileImageView.setVisibility(View.GONE);
        holder.activityTextView.setVisibility(View.GONE);
        if (bean.isIfsole()) {
            holder.mobileImageView.setVisibility(View.VISIBLE);
        }
        if (bean.isIfxianshi()) {
            holder.activityTextView.setText(R.string.timeLimitSale);
            holder.activityTextView.setVisibility(View.VISIBLE);
        }
        if (bean.isIfgroupbuy()) {
            holder.activityTextView.setText(R.string.rushToBuy);
            holder.activityTextView.setVisibility(View.VISIBLE);
        }

        holder.mainCheckBox.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onCheck(position, holder.mainCheckBox.isChecked(), bean);
            }
        });

        holder.deleteImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onDelete(position, bean);
            }
        });

        holder.subTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onSub(position, bean);
            }
        });

        holder.addTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onAdd(position, bean);
            }
        });

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_main_cart_goods, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, CartBean.GoodsBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onDelete(int position, CartBean.GoodsBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onAdd(int position, CartBean.GoodsBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onSub(int position, CartBean.GoodsBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onCheck(int position, boolean select, CartBean.GoodsBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatCheckBox mainCheckBox;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView specTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatImageView mobileImageView;
        private final AppCompatTextView activityTextView;
        private final AppCompatImageView deleteImageView;
        private final AppCompatTextView addTextView;
        private final AppCompatEditText numberEditText;
        private final AppCompatTextView subTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainCheckBox = view.findViewById(R.id.mainCheckBox);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            specTextView = view.findViewById(R.id.specTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            mobileImageView = view.findViewById(R.id.mobileImageView);
            activityTextView = view.findViewById(R.id.activityTextView);
            deleteImageView = view.findViewById(R.id.deleteImageView);
            addTextView = view.findViewById(R.id.addTextView);
            numberEditText = view.findViewById(R.id.numberEditText);
            subTextView = view.findViewById(R.id.subTextView);

        }

    }

}
