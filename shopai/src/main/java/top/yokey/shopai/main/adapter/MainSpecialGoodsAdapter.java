package top.yokey.shopai.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.SpecialBean;
import top.yokey.shopai.zsdk.data.GoodsData;

class MainSpecialGoodsAdapter extends RecyclerView.Adapter<MainSpecialGoodsAdapter.ViewHolder> {

    private final ArrayList<SpecialBean.GoodsBean.ItemBean> arrayList;

    MainSpecialGoodsAdapter(ArrayList<SpecialBean.GoodsBean.ItemBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SpecialBean.GoodsBean.ItemBean bean = arrayList.get(position);
        LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) holder.mainImageView.getLayoutParams();
        layoutParams.height = (int) (App.get().getWidth() / 2.1);
        holder.mainImageView.setLayoutParams(layoutParams);
        ImageHelp.get().displayRadius(bean.getGoodsImage(), holder.mainImageView);
        holder.nameTextView.setText(bean.getGoodsName());
        holder.priceTextView.setText(App.get().getString(R.string.rmb));
        holder.priceTextView.append(bean.getGoodsSalePrice());

        holder.mainLinearLayout.setOnClickListener(view -> App.get().startGoods(new GoodsData(bean.getGoodsId())));

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_main_special_goods, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView priceTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            priceTextView = view.findViewById(R.id.priceTextView);

        }

    }

}
