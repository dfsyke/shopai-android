package top.yokey.shopai.main.activity;

import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.AnimUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zsdk.ShopAISdk;

@Route(path = ARoutePath.MAIN_BROWSER)
public class MainBrowserActivity extends BaseActivity {

    @Autowired(name = Constant.DATA_URL)
    String url = "";
    private Toolbar mainToolbar = null;
    private WebView mainWebView = null;
    private ContentLoadingProgressBar mainProgressBar = null;
    private AppCompatImageView backImageView = null;
    private AppCompatImageView forwardImageView = null;
    private AppCompatImageView refreshImageView = null;
    private AppCompatImageView shareImageView = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_main_browser);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainWebView = findViewById(R.id.mainWebView);
        mainProgressBar = findViewById(R.id.mainProgressBar);
        backImageView = findViewById(R.id.backImageView);
        forwardImageView = findViewById(R.id.forwardImageView);
        refreshImageView = findViewById(R.id.refreshImageView);
        shareImageView = findViewById(R.id.shareImageView);

    }

    @Override
    public void initData() {

        if (!url.contains("http://") && !url.contains("https://")) url = "https://" + url;
        setToolbar(mainToolbar, R.drawable.ic_action_exit, R.string.loadIng);
        observeKeyborad(R.id.mainLinearLayout);
        App.get().setWebView(mainWebView);
        mainWebView.loadUrl(VerifyUtil.isEmpty(url) ? ShopAISdk.get().getUrl() : url);

    }

    @Override
    public void initEvent() {

        mainWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                mainWebView.loadUrl(url);
                return true;
            }
        });

        mainWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                mainProgressBar.setProgress(progress);
                if (progress == 100) {
                    if (mainProgressBar.getVisibility() == View.VISIBLE) {
                        AnimUtil.objectAnimator(mainProgressBar, AnimUtil.ALPHA, 1.0f, 0f);
                    }
                    mainProgressBar.setVisibility(View.GONE);
                } else {
                    if (mainProgressBar.getVisibility() == View.GONE) {
                        AnimUtil.objectAnimator(mainProgressBar, AnimUtil.ALPHA, 0f, 1.0f);
                    }
                    mainProgressBar.setVisibility(View.VISIBLE);
                }
                super.onProgressChanged(view, progress);
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                setToolbar(mainToolbar, R.drawable.ic_action_exit, title);
            }
        });

        backImageView.setOnClickListener(view -> mainWebView.goBack());

        forwardImageView.setOnClickListener(view -> mainWebView.goForward());

        refreshImageView.setOnClickListener(view -> mainWebView.reload());

        shareImageView.setOnClickListener(view -> {
        });

    }

    @Override
    public void initObserve() {

    }

}
