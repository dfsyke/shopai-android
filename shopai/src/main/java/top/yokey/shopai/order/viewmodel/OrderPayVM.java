package top.yokey.shopai.order.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.OrderPayBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberBuyController;
import top.yokey.shopai.zsdk.controller.MemberPaymentController;
import top.yokey.shopai.zsdk.data.OrderPayData;

public class OrderPayVM extends BaseViewModel {

    private final MutableLiveData<OrderPayBean> buyLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> payPasswordLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> payLiveData = new MutableLiveData<>();

    public OrderPayVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<OrderPayBean> getBuyLiveData() {

        return buyLiveData;

    }

    public MutableLiveData<String> getPayPasswordLiveData() {

        return payPasswordLiveData;

    }

    public MutableLiveData<String> getPayLiveData() {

        return payLiveData;

    }

    public void getData(String paySn) {

        MemberBuyController.pay(paySn, new HttpCallBack<OrderPayBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, OrderPayBean bean) {
                buyLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void checkPdPwd(String password) {

        MemberBuyController.checkPdPwd(password, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                payPasswordLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void payNew(OrderPayData data) {

        MemberPaymentController.payNew(data, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                payLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
