package top.yokey.shopai.order.activity;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.order.adapter.OrderLogisticsAdapter;
import top.yokey.shopai.order.viewmodel.OrderLogisticsVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;

@Route(path = ARoutePath.ORDER_LOGISTICS)
public class OrderLogisticsActivity extends BaseActivity {

    private final ArrayList<String> arrayList = new ArrayList<>();
    private final OrderLogisticsAdapter adapter = new OrderLogisticsAdapter(arrayList);
    @Autowired(name = Constant.DATA_ID)
    String orderId;
    private Toolbar mainToolbar;
    private AppCompatTextView logisticsTextView;
    private AppCompatTextView logisticsNumberTextView;
    private PullRefreshView mainPullRefreshView;
    private OrderLogisticsVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_order_logistics);
        mainToolbar = findViewById(R.id.mainToolbar);
        logisticsTextView = findViewById(R.id.logisticsTextView);
        logisticsNumberTextView = findViewById(R.id.logisticsNumberTextView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        if (VerifyUtil.isEmpty(orderId)) {
            ToastHelp.get().showDataError();
            onReturn(false);
            return;
        }
        setToolbar(mainToolbar, R.string.seeLogistics);
        observeKeyborad(R.id.mainLinearLayout);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setCanRefresh(false);
        mainPullRefreshView.setCanLoadMore(false);
        mainPullRefreshView.setItemDecoration(new LineDecoration(1, App.get().getColors(R.color.divider), false));
        vm = getVM(OrderLogisticsVM.class);
        vm.getLogistics(orderId);
        mainPullRefreshView.setLoad();

    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initObserve() {

        vm.getLogisticsLiveData().observe(this, bean -> {
            arrayList.clear();
            logisticsTextView.setText(R.string.logisticsCompany);
            logisticsTextView.append("：" + bean.getExpressName());
            logisticsNumberTextView.setText(R.string.logisticsNumber);
            logisticsNumberTextView.append("：" + bean.getShippingCode());
            arrayList.addAll(bean.getDeliverInfo());
            mainPullRefreshView.setComplete();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.getLogistics(orderId));
            }
        });

    }

}
