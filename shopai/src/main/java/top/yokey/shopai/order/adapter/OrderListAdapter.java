package top.yokey.shopai.order.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zsdk.bean.OrderBean;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ViewHolder> {

    private final ArrayList<OrderBean> arrayList;
    private final LineDecoration lineDecoration = new LineDecoration(App.get().dp2Px(16), App.get().getColors(R.color.wap), false);
    private OnItemClickListener onItemClickListener = null;

    public OrderListAdapter(ArrayList<OrderBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        OrderBean bean = arrayList.get(position);
        boolean showPay = false;
        for (int i = 0; i < bean.getOrderList().size(); i++) {
            if (bean.getOrderList().get(i).getOrderState().equals("10")) {
                showPay = true;
            }
        }
        if (bean.getPayAmount() == null) {
            holder.lineView.setVisibility(View.GONE);
            holder.payTextView.setVisibility(View.GONE);
        } else {
            holder.lineView.setVisibility(View.VISIBLE);
            holder.payTextView.setVisibility(View.VISIBLE);
            holder.payTextView.setText(String.format(App.get().getString(R.string.orderPayPrice), bean.getPayAmount()));
        }
        OrderListStoreAdapter adapter = new OrderListStoreAdapter(bean.getOrderList());
        App.get().setRecyclerView(holder.mainRecyclerView, adapter);
        holder.mainRecyclerView.removeItemDecoration(lineDecoration);
        holder.mainRecyclerView.addItemDecoration(lineDecoration);
        holder.lineView.setVisibility(showPay ? View.VISIBLE : View.GONE);
        holder.payTextView.setVisibility(showPay ? View.VISIBLE : View.GONE);

        adapter.setOnItemClickListener(new OrderListStoreAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, OrderBean.OrderListBean orderListBean) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(position, position, orderListBean);
                }
            }

            @Override
            public void onOption(int position, OrderBean.OrderListBean orderListBean) {
                if (onItemClickListener != null) {
                    onItemClickListener.onOption(position, position, orderListBean);
                }
            }

            @Override
            public void onOpera(int position, OrderBean.OrderListBean orderListBean) {
                if (onItemClickListener != null) {
                    onItemClickListener.onOpera(position, position, orderListBean);
                }
            }

            @Override
            public void onClickGoods(int position, int itemPosition, OrderBean.OrderListBean.ExtendOrderGoodsBean extendOrderGoodsBean) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemGoodsClick(position, itemPosition, bean.getOrderList().get(position));
                }
            }
        });

        holder.payTextView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onPay(position, bean);
            }
        });

        holder.mainLinearLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_order_list, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onPay(int position, OrderBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, OrderBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onItemClick(int position, int itemPosition, OrderBean.OrderListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onItemGoodsClick(int position, int itemPosition, OrderBean.OrderListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onOption(int position, int itemPosition, OrderBean.OrderListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onOpera(int position, int itemPosition, OrderBean.OrderListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayoutCompat mainLinearLayout;
        private final RecyclerView mainRecyclerView;
        private final View lineView;
        private final AppCompatTextView payTextView;

        private ViewHolder(View view) {

            super(view);
            mainLinearLayout = view.findViewById(R.id.mainLinearLayout);
            mainRecyclerView = view.findViewById(R.id.mainRecyclerView);
            lineView = view.findViewById(R.id.lineView);
            payTextView = view.findViewById(R.id.payTextView);

        }

    }

}
