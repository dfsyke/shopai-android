package top.yokey.shopai.order.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.OrderDeliverBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberOrderController;

public class OrderLogisticsVM extends BaseViewModel {

    private final MutableLiveData<OrderDeliverBean> logisticsLiveData = new MutableLiveData<>();

    public OrderLogisticsVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<OrderDeliverBean> getLogisticsLiveData() {

        return logisticsLiveData;

    }

    public void getLogistics(String orderId) {

        MemberOrderController.searchDeliver(orderId, new HttpCallBack<OrderDeliverBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, OrderDeliverBean bean) {
                logisticsLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
