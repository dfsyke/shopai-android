package top.yokey.shopai.point.activity;

import android.content.Intent;
import android.text.Html;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.point.viewmodel.PointBuyVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.DialogHelp;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.POINT_BUY)
public class PointBuyActivity extends BaseActivity {

    private Toolbar mainToolbar = null;
    private RelativeLayout addressRelativeLayout = null;
    private AppCompatTextView addressNameTextView = null;
    private AppCompatTextView addressAreaTextView = null;
    private AppCompatImageView goodsImageView = null;
    private AppCompatTextView nameTextView = null;
    private AppCompatTextView pointTextView = null;
    private AppCompatTextView numberTextView = null;
    private AppCompatEditText messageEditText = null;
    private AppCompatTextView settlementTextView = null;
    private AppCompatTextView submitTextView = null;

    private String addressId = "";
    private PointBuyVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_point_buy);
        mainToolbar = findViewById(R.id.mainToolbar);
        addressRelativeLayout = findViewById(R.id.addressRelativeLayout);
        addressNameTextView = findViewById(R.id.addressNameTextView);
        addressAreaTextView = findViewById(R.id.addressAreaTextView);
        goodsImageView = findViewById(R.id.goodsImageView);
        nameTextView = findViewById(R.id.nameTextView);
        pointTextView = findViewById(R.id.pointTextView);
        numberTextView = findViewById(R.id.numberTextView);
        messageEditText = findViewById(R.id.messageEditText);
        settlementTextView = findViewById(R.id.settlementTextView);
        submitTextView = findViewById(R.id.submitTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.confirmOrderInfo);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(PointBuyVM.class);
        vm.step1(addressId);

    }

    @Override
    public void initEvent() {

        addressRelativeLayout.setOnClickListener(view -> App.get().start(get(), ARoutePath.MEMBER_ADDRESS, Constant.DATA_ID, Constant.COMMON_ENABLE, Constant.CODE_ADDRESS));

        submitTextView.setOnClickListener(view -> {
            hideKeyboard();
            vm.step2(addressId, Objects.requireNonNull(messageEditText.getText()).toString());
        });

    }

    @Override
    public void initObserve() {

        vm.getStep1LiveData().observe(this, string -> {
            string = string.replace("[]", "null");
            JSONObject mainJsonObject = JsonUtil.toJSONObject(string);
            JSONObject pointprodArrJSONObject = JsonUtil.getJSONObject(mainJsonObject, "pointprod_arr");
            JSONObject addressInfoJSONObject = JsonUtil.getJSONObject(mainJsonObject, "address_info");
            //地址信息
            if (Objects.requireNonNull(addressInfoJSONObject).toString().equals("{}")) {
                tipsAddAddress();
                return;
            }
            addressNameTextView.setText(JsonUtil.getString(addressInfoJSONObject, "true_name"));
            addressNameTextView.append(" " + JsonUtil.getString(addressInfoJSONObject, "mob_phone"));
            addressAreaTextView.setText(JsonUtil.getString(addressInfoJSONObject, "area_info"));
            addressAreaTextView.append(" " + JsonUtil.getString(addressInfoJSONObject, "address"));
            addressId = JsonUtil.getString(addressInfoJSONObject, "address_id");
            //商品信息
            JSONArray jsonArray = JsonUtil.getJSONArray(pointprodArrJSONObject, "pointprod_list");
            JSONObject jsonObject = JsonUtil.getJSONObject(Objects.requireNonNull(jsonArray), 0);
            ImageHelp.get().displayRadius(JsonUtil.getString(jsonObject, "pgoods_image"), goodsImageView);
            nameTextView.setText(JsonUtil.getString(jsonObject, "pgoods_name"));
            pointTextView.setText(R.string.exchangePoint);
            pointTextView.append("：" + JsonUtil.getString(jsonObject, "pgoods_points"));
            numberTextView.setText("x");
            numberTextView.append(JsonUtil.getString(jsonObject, "quantity"));
            String temp = String.format(App.get().getString(R.string.htmlPointOrderSettlement), JsonUtil.getString(jsonObject, "quantity"), JsonUtil.getString(pointprodArrJSONObject, "pgoods_pointall"));
            settlementTextView.setText(Html.fromHtml(App.get().handlerHtml(temp, "#EE0000")));
        });

        vm.getStep2LiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                DialogHelp.get().query(get(), R.string.loadFailure, bean.getReason(), view -> onReturn(false), view -> vm.step1(addressId));
            } else {
                submitTextView.setEnabled(true);
                submitTextView.setText(R.string.submitOrder);
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

    @Override
    public void onActivityResult(int req, int res, @Nullable Intent intent) {

        super.onActivityResult(req, res, intent);
        if (intent != null && res == RESULT_OK) {
            if (req == Constant.CODE_ADDRESS) {
                addressId = intent.getStringExtra(Constant.DATA_ID);
                vm.step1(addressId);
            }
        } else {
            if (req == Constant.CODE_ADDRESS) {
                if (VerifyUtil.isEmpty(addressId) && VerifyUtil.isEmpty(addressNameTextView.getText().toString())) {
                    tipsAddAddress();
                }
            }
        }

    }

    //自定义方法

    private void tipsAddAddress() {

        DialogHelp.get().query(
                get(),
                R.string.loadFailure,
                R.string.tipsAddReceiveAddress,
                view -> onReturn(false),
                view -> App.get().start(get(), ARoutePath.MEMBER_ADDRESS, Constant.DATA_ID, Constant.COMMON_ENABLE, Constant.CODE_ADDRESS)
        );

    }

}
