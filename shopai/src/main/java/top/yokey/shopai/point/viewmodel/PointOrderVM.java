package top.yokey.shopai.point.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.PointOrderBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberPointOrderController;

public class PointOrderVM extends BaseViewModel {

    private final MutableLiveData<PointOrderBean> orderLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> cancelLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> receiveLiveData = new MutableLiveData<>();

    public PointOrderVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<PointOrderBean> getOrderLiveData() {

        return orderLiveData;

    }

    public MutableLiveData<String> getCancelLiveData() {

        return cancelLiveData;

    }

    public MutableLiveData<String> getReceiveLiveData() {

        return receiveLiveData;

    }

    public void orderInfo(String orderId) {

        MemberPointOrderController.orderInfo(orderId, new HttpCallBack<PointOrderBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, PointOrderBean bean) {
                orderLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void cancel(String orderId) {

        MemberPointOrderController.cancelOrder(orderId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                cancelLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void receive(String orderId) {

        MemberPointOrderController.receivingOrder(orderId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                receiveLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
