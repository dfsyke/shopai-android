package top.yokey.shopai.point.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.PointGoodsListBean;

public class PointListAdapter extends RecyclerView.Adapter<PointListAdapter.ViewHolder> {

    private final ArrayList<PointGoodsListBean> arrayList;
    private boolean isGrid;
    private OnItemClickListener onItemClickListener = null;

    public PointListAdapter(ArrayList<PointGoodsListBean> arrayList, boolean isGrid) {

        this.isGrid = isGrid;
        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        PointGoodsListBean bean = arrayList.get(position);
        ImageHelp.get().displayRadius(bean.getPgoodsImage(), holder.mainImageView);
        int height = (App.get().getWidth() - 32) / 2;
        ViewGroup.LayoutParams layoutParams = holder.mainImageView.getLayoutParams();
        if (isGrid) layoutParams.height = height;
        holder.mainImageView.setLayoutParams(layoutParams);
        holder.nameTextView.setText(bean.getPgoodsName());
        holder.pointTextView.setText(R.string.exchangePoint);
        holder.pointTextView.append("：" + bean.getPgoodsPoints());
        holder.stockTextView.setText(R.string.stock);
        holder.stockTextView.append("：" + bean.getPgoodsStorage());

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view;
        if (isGrid) {
            view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_point_list, group, false);
        } else {
            view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_point_list_ver, group, false);
        }
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setGrid(boolean grid) {

        isGrid = grid;

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, PointGoodsListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView pointTextView;
        private final AppCompatTextView stockTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            pointTextView = view.findViewById(R.id.pointTextView);
            stockTextView = view.findViewById(R.id.stockTextView);

        }

    }

}
