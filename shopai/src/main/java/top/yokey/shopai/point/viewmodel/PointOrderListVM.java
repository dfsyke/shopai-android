package top.yokey.shopai.point.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.PointOrderListBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberPointOrderController;

public class PointOrderListVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<PointOrderListBean>> orderLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> cancelLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> receiveLiveData = new MutableLiveData<>();

    public PointOrderListVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<PointOrderListBean>> getOrderLiveData() {

        return orderLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public MutableLiveData<String> getCancelLiveData() {

        return cancelLiveData;

    }

    public MutableLiveData<String> getReceiveLiveData() {

        return receiveLiveData;

    }

    public void getList(String page) {

        MemberPointOrderController.orderList(page, new HttpCallBack<ArrayList<PointOrderListBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<PointOrderListBean> list) {
                orderLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void cancel(String orderId) {

        MemberPointOrderController.cancelOrder(orderId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                cancelLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void receive(String orderId) {

        MemberPointOrderController.receivingOrder(orderId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                receiveLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
