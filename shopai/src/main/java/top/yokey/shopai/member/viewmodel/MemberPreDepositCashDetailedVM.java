package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.PreDepositCashBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFundController;

public class MemberPreDepositCashDetailedVM extends BaseViewModel {

    private final MutableLiveData<PreDepositCashBean> cashLiveData = new MutableLiveData<>();

    public MemberPreDepositCashDetailedVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<PreDepositCashBean> getCashLiveData() {

        return cashLiveData;

    }

    public void getCash(String id) {

        MemberFundController.pdCashInfo(id, new HttpCallBack<PreDepositCashBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, PreDepositCashBean bean) {
                cashLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().postValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
