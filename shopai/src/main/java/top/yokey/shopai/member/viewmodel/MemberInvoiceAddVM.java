package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberInvoiceController;
import top.yokey.shopai.zsdk.data.InvoiceAddData;

public class MemberInvoiceAddVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<String>> contentLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> addLiveData = new MutableLiveData<>();

    public MemberInvoiceAddVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<String>> getContentLiveData() {

        return contentLiveData;

    }

    public MutableLiveData<String> getAddLiveData() {

        return addLiveData;

    }

    public void getContent() {

        MemberInvoiceController.invoiceContentList(new HttpCallBack<ArrayList<String>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<String> list) {
                contentLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void add(InvoiceAddData data) {

        MemberInvoiceController.invoiceAdd(data, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                addLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
