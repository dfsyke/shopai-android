package top.yokey.shopai.member.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zsdk.bean.AddressBean;

public class MemberAddressAdapter extends RecyclerView.Adapter<MemberAddressAdapter.ViewHolder> {

    private final ArrayList<AddressBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberAddressAdapter(ArrayList<AddressBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        AddressBean bean = arrayList.get(position);
        holder.nameTextView.setText(bean.getTrueName());
        holder.nameTextView.append(" " + bean.getMobPhone());
        holder.areaTextView.setText(bean.getAreaInfo());
        holder.areaTextView.append(" " + bean.getAddress());
        holder.defaultCheckBox.setChecked(bean.getIsDefault().equals(Constant.COMMON_ENABLE));
        holder.defaultCheckBox.setTextColor(App.get().getColors(bean.getIsDefault().equals(Constant.COMMON_ENABLE) ? R.color.accent : R.color.textTwo));

        holder.modifyImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickModify(position, bean);
            }
        });

        holder.deleteImageView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickDelete(position, bean);
            }
        });

        holder.defaultCheckBox.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClickDefault(position, bean);
            }
        });

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_member_address, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, AddressBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickModify(int position, AddressBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickDelete(int position, AddressBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onClickDefault(int position, AddressBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView areaTextView;
        private final AppCompatImageView modifyImageView;
        private final AppCompatImageView deleteImageView;
        private final AppCompatCheckBox defaultCheckBox;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            nameTextView = view.findViewById(R.id.nameTextView);
            areaTextView = view.findViewById(R.id.areaTextView);
            modifyImageView = view.findViewById(R.id.modifyImageView);
            deleteImageView = view.findViewById(R.id.deleteImageView);
            defaultCheckBox = view.findViewById(R.id.defaultCheckBox);

        }

    }

}
