package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberAddressController;
import top.yokey.shopai.zsdk.data.AddressData;

public class MemberAddressAddVM extends BaseViewModel {

    private final MutableLiveData<String> addLiveData = new MutableLiveData<>();

    public MemberAddressAddVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getAddLiveData() {

        return addLiveData;

    }

    public void add(AddressData data) {

        MemberAddressController.addressAdd(data, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                addLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
