package top.yokey.shopai.member.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zsdk.bean.PreDepositRechargeLogBean;

public class MemberPreDepositRechargeLogAdapter extends RecyclerView.Adapter<MemberPreDepositRechargeLogAdapter.ViewHolder> {

    private final ArrayList<PreDepositRechargeLogBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberPreDepositRechargeLogAdapter(ArrayList<PreDepositRechargeLogBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        PreDepositRechargeLogBean bean = arrayList.get(position);
        holder.mainTextView.setTextColor(bean.getPdrPaymentState().equals(Constant.COMMON_ENABLE) ? App.get().getColors(R.color.text) : App.get().getColors(R.color.red));
        holder.mainTextView.setText(bean.getPdrPaymentStateText());
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getPdrAmount());
        holder.descTextView.setText(String.format(App.get().getString(R.string.orderPaySn), bean.getPdrSn()));
        holder.timeTextView.setText(bean.getPdrAddTimeText());

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_member_pre_deposit_recharge_log, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, PreDepositRechargeLogBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatTextView mainTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView descTextView;
        private final AppCompatTextView timeTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainTextView = view.findViewById(R.id.mainTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            descTextView = view.findViewById(R.id.descTextView);
            timeTextView = view.findViewById(R.id.timeTextView);

        }

    }

}
