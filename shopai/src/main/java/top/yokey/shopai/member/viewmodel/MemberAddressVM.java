package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.AddressBean;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberAddressController;
import top.yokey.shopai.zsdk.data.AddressData;

public class MemberAddressVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<AddressBean>> addressLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> defaultLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> deleteLiveData = new MutableLiveData<>();

    public MemberAddressVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<AddressBean>> getAddressLiveData() {

        return addressLiveData;

    }

    public MutableLiveData<String> getDefaultLiveData() {

        return defaultLiveData;

    }

    public MutableLiveData<String> getDeleteLiveData() {

        return deleteLiveData;

    }

    public void getAddress() {

        MemberAddressController.addressList(new HttpCallBack<ArrayList<AddressBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<AddressBean> list) {
                addressLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void setDefault(AddressData data) {

        MemberAddressController.addressEdit(data, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                defaultLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void delete(String addressId) {

        MemberAddressController.addressDel(addressId, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                deleteLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
