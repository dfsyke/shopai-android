package top.yokey.shopai.member.activity;

import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import top.yokey.shopai.R;
import top.yokey.shopai.member.adapter.MemberInviteIncomeAdapter;
import top.yokey.shopai.member.viewmodel.MemberInviteIncomeVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.adapter.ViewPagerAdapter;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.MemberInviteIncomeBean;

@Route(path = ARoutePath.MEMBER_INVITE_INCOME)
public class MemberInviteIncomeActivity extends BaseActivity {

    private final int[] page = new int[3];
    @SuppressWarnings("unchecked")
    private final ArrayList<MemberInviteIncomeBean>[] arrayList = new ArrayList[3];
    private final MemberInviteIncomeAdapter[] adapter = new MemberInviteIncomeAdapter[3];
    private final PullRefreshView[] mainPullRefreshView = new PullRefreshView[3];
    private Toolbar mainToolbar = null;
    private TabLayout mainTabLayout = null;
    private ViewPager mainViewPager = null;
    private MemberInviteIncomeVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_tab_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainTabLayout = findViewById(R.id.mainTabLayout);
        mainViewPager = findViewById(R.id.mainViewPager);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.distribuIncome);
        List<String> titleList = new ArrayList<>();
        titleList.add(getString(R.string.levelOneOffline));
        titleList.add(getString(R.string.levelTwoOffline));
        titleList.add(getString(R.string.levelThrOffline));
        List<View> viewList = new ArrayList<>();
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        viewList.add(getLayoutInflater().inflate(R.layout.include_pull_refresh_view, null));
        for (int i = 0; i < viewList.size(); i++) {
            page[i] = 1;
            arrayList[i] = new ArrayList<>();
            adapter[i] = new MemberInviteIncomeAdapter(arrayList[i]);
            mainPullRefreshView[i] = viewList.get(i).findViewById(R.id.mainPullRefreshView);
            mainTabLayout.addTab(mainTabLayout.newTab().setText(titleList.get(i)));
            mainPullRefreshView[i].setItemDecoration(new LineDecoration(App.get().dp2Px(12), true));
            mainPullRefreshView[i].setAdapter(adapter[i]);
        }
        App.get().setTabLayout(mainTabLayout, mainViewPager, new ViewPagerAdapter(viewList, titleList));
        mainTabLayout.setTabMode(TabLayout.MODE_FIXED);
        mainViewPager.setCurrentItem(0);
        vm = getVM(MemberInviteIncomeVM.class);
        getData(0);

    }

    @Override
    public void initEvent() {

        mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (arrayList[position].size() == 0) {
                    page[position] = 1;
                    getData(position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        for (int i = 0; i < mainPullRefreshView.length; i++) {
            final int pos = i;
            mainPullRefreshView[pos].setOnClickListener(view -> {
                if (mainPullRefreshView[pos].isError() || arrayList[pos].size() == 0) {
                    page[pos] = 1;
                    getData(pos);
                }
            });
            mainPullRefreshView[pos].setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    page[pos] = 1;
                    getData(pos);
                }

                @Override
                public void onLoadMore() {
                    getData(pos);
                }
            });
        }

    }

    @Override
    public void initObserve() {

        vm.getOneLiveData().observe(this, list -> {
            if (page[0] == 1) arrayList[0].clear();
            page[0] = page[0] + 1;
            arrayList[0].addAll(list);
            mainPullRefreshView[0].setComplete();
        });

        vm.getOneHasMoreLiveData().observe(this, bool -> mainPullRefreshView[0].setCanLoadMore(bool));

        vm.getTwoLiveData().observe(this, list -> {
            if (page[1] == 1) arrayList[1].clear();
            page[1] = page[1] + 1;
            arrayList[1].addAll(list);
            mainPullRefreshView[1].setComplete();
        });

        vm.getTwoHasMoreLiveData().observe(this, bool -> mainPullRefreshView[1].setCanLoadMore(bool));

        vm.getThrLiveData().observe(this, list -> {
            if (page[2] == 1) arrayList[2].clear();
            page[2] = page[2] + 1;
            arrayList[2].addAll(list);
            mainPullRefreshView[2].setComplete();
        });

        vm.getThrHasMoreLiveData().observe(this, bool -> mainPullRefreshView[2].setCanLoadMore(bool));

        vm.getErrorLiveData().observe(this, bean -> {
            if (arrayList[bean.getCode() - 1] != null && arrayList[bean.getCode() - 1].size() != 0) {
                ToastHelp.get().show(bean.getReason());
            } else {
                mainPullRefreshView[bean.getCode() - 1].setError(bean.getReason());
            }
        });

    }

    //自定义方法

    private void getData(int position) {

        mainPullRefreshView[position].setLoad();
        if (position == 0) vm.getOne(page[0] + "");
        if (position == 1) vm.getTwo(page[1] + "");
        if (position == 2) vm.getThr(page[2] + "");

    }

}
