package top.yokey.shopai.member.activity;

import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.member.adapter.MemberRedPacketAdapter;
import top.yokey.shopai.member.viewmodel.MemberRedPacketVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.RedPacketListBean;

@Route(path = ARoutePath.MEMBER_RED_PACKET)
public class MemberRedPacketActivity extends BaseActivity {

    private final ArrayList<RedPacketListBean> arrayList = new ArrayList<>();
    private final MemberRedPacketAdapter adapter = new MemberRedPacketAdapter(arrayList);
    private Toolbar mainToolbar = null;
    private PullRefreshView mainPullRefreshView = null;
    private int page = 0;
    private MemberRedPacketVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.mineRedPacket);
        mainPullRefreshView.setItemDecoration(new LineDecoration(App.get().dp2Px(12), true));
        mainPullRefreshView.setAdapter(adapter);
        vm = getVM(MemberRedPacketVM.class);
        getData();

    }

    @Override
    public void initEvent() {

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                page = 1;
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                getData();
            }

            @Override
            public void onLoadMore() {
                getData();
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getRedPacketLiveData().observe(this, list -> {
            if (page == 1) arrayList.clear();
            page = page + 1;
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getHasMoreLiveData().observe(this, bool -> mainPullRefreshView.setCanLoadMore(bool));

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList.size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    mainPullRefreshView.setError(bean.getReason());
                }
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getRedPacket(page + "");

    }

}
