package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberAddressController;
import top.yokey.shopai.zsdk.data.AddressData;

public class MemberAddressModifyVM extends BaseViewModel {

    private final MutableLiveData<String> modifyLiveData = new MutableLiveData<>();

    public MemberAddressModifyVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getModifyLiveData() {

        return modifyLiveData;

    }

    public void modify(AddressData data) {

        MemberAddressController.addressEdit(data, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                modifyLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
