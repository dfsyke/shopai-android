package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.PreDepositCashLogBean;
import top.yokey.shopai.zsdk.bean.PreDepositLogBean;
import top.yokey.shopai.zsdk.bean.PreDepositRechargeLogBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberFundController;

public class MemberPreDepositLogVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<PreDepositLogBean>> preDepositLogLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> preDepositLogHasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<PreDepositRechargeLogBean>> pdRechargeLogLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> pdRechargeLogHasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<PreDepositCashLogBean>> pdCashLogLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> pdCashLogHasMoreLiveData = new MutableLiveData<>();

    public MemberPreDepositLogVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<PreDepositLogBean>> getPreDepositLogLiveData() {

        return preDepositLogLiveData;

    }

    public MutableLiveData<Boolean> getPreDepositLogHasMoreLiveData() {

        return preDepositLogHasMoreLiveData;

    }

    public MutableLiveData<ArrayList<PreDepositRechargeLogBean>> getPdRechargeLogLiveData() {

        return pdRechargeLogLiveData;

    }

    public MutableLiveData<Boolean> getPdRechargeLogHasMoreLiveData() {

        return pdRechargeLogHasMoreLiveData;

    }

    public MutableLiveData<ArrayList<PreDepositCashLogBean>> getPdCashLogLiveData() {

        return pdCashLogLiveData;

    }

    public MutableLiveData<Boolean> getPdCashLogHasMoreLiveData() {

        return pdCashLogHasMoreLiveData;

    }

    public void getPreDepositLog(String page) {

        MemberFundController.preDepositLog(page, new HttpCallBack<ArrayList<PreDepositLogBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<PreDepositLogBean> list) {
                preDepositLogLiveData.setValue(list);
                preDepositLogHasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getPdRechargeLog(String page) {

        MemberFundController.pdRechargeList(page, new HttpCallBack<ArrayList<PreDepositRechargeLogBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<PreDepositRechargeLogBean> list) {
                pdRechargeLogLiveData.setValue(list);
                pdRechargeLogHasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void getPdCashLog(String page) {

        MemberFundController.pdCashList(page, new HttpCallBack<ArrayList<PreDepositCashLogBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<PreDepositCashLogBean> list) {
                pdCashLogLiveData.setValue(list);
                pdCashLogHasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
