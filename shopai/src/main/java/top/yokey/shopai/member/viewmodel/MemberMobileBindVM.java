package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberAccountController;

public class MemberMobileBindVM extends BaseViewModel {

    private final MutableLiveData<String> getCaptchaLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> bindLiveData = new MutableLiveData<>();

    public MemberMobileBindVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<String> getGetCaptchaLiveData() {

        return getCaptchaLiveData;

    }

    public MutableLiveData<String> getBindLiveData() {

        return bindLiveData;

    }

    public void step1(String mobile) {

        MemberAccountController.bindMobikeStep1(mobile, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                getCaptchaLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void step2(String captcha) {

        MemberAccountController.bindMobikeStep2(captcha, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                bindLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

}
