package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.VoucherBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberVoucherController;
import top.yokey.shopai.zsdk.controller.VercodeController;

public class MemberVoucherVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<VoucherBean>> voucherLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> codeKeyLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> addLiveData = new MutableLiveData<>();

    public MemberVoucherVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<VoucherBean>> getVoucherLiveData() {

        return voucherLiveData;

    }

    public MutableLiveData<String> getCodeKeyLiveData() {

        return codeKeyLiveData;

    }

    public MutableLiveData<String> getAddLiveData() {

        return addLiveData;

    }

    public void getVoucher() {

        MemberVoucherController.voucherList(new HttpCallBack<ArrayList<VoucherBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<VoucherBean> list) {
                voucherLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getCodeKey() {

        VercodeController.makeCodeKey(new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                codeKeyLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void add(String pwdCode, String captcha, String codeKey) {

        MemberVoucherController.voucherPwex(pwdCode, captcha, codeKey, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                addLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
