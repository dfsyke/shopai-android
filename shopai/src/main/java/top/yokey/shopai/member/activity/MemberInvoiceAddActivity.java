package top.yokey.shopai.member.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberInvoiceAddVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zsdk.data.InvoiceAddData;

@Route(path = ARoutePath.MEMBER_INVOICE_ADD)
public class MemberInvoiceAddActivity extends BaseActivity {

    private final InvoiceAddData data = new InvoiceAddData();
    private Toolbar mainToolbar = null;
    private AppCompatTextView personTextView = null;
    private AppCompatTextView companyTextView = null;
    private AppCompatEditText titleEditText = null;
    private AppCompatSpinner contentSpinner = null;
    private AppCompatTextView addTextView = null;
    private MemberInvoiceAddVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_invoice_add);
        mainToolbar = findViewById(R.id.mainToolbar);
        personTextView = findViewById(R.id.personTextView);
        companyTextView = findViewById(R.id.companyTextView);
        titleEditText = findViewById(R.id.titleEditText);
        contentSpinner = findViewById(R.id.contentSpinner);
        addTextView = findViewById(R.id.addTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.addInvoice);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(MemberInvoiceAddVM.class);
        data.setTitle("person");
        vm.getContent();

    }

    @Override
    public void initEvent() {

        personTextView.setOnClickListener(view -> {
            data.setTitle("person");
            personTextView.setTextColor(App.get().getColors(R.color.primary));
            personTextView.setBackgroundResource(R.drawable.selector_accent_16dp);
            companyTextView.setTextColor(App.get().getColors(R.color.accent));
            companyTextView.setBackgroundResource(R.drawable.selector_hollow_accent_16dp);
        });

        companyTextView.setOnClickListener(view -> {
            data.setTitle("company");
            personTextView.setTextColor(App.get().getColors(R.color.accent));
            personTextView.setBackgroundResource(R.drawable.selector_hollow_accent_16dp);
            companyTextView.setTextColor(App.get().getColors(R.color.primary));
            companyTextView.setBackgroundResource(R.drawable.selector_accent_16dp);
        });

        addTextView.setOnClickListener(view -> {
            data.setTitle(Objects.requireNonNull(titleEditText.getText()).toString());
            vm.add(data);
        });

    }

    @Override
    public void initObserve() {

        vm.getContentLiveData().observe(this, list -> {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(get(), R.layout.spinner_simple, list);
            adapter.setDropDownViewResource(R.layout.spinner_item);
            contentSpinner.setAdapter(adapter);
            contentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    data.setContent(list.get(position));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });

        vm.getAddLiveData().observe(this, string -> {
            ToastHelp.get().show(R.string.addInvoiceSuccess);
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> ToastHelp.get().show(bean.getReason()));

    }

}
