package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberInviteIncomeBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberInviteController;

public class MemberInviteIncomeVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<MemberInviteIncomeBean>> oneLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<MemberInviteIncomeBean>> twoLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<MemberInviteIncomeBean>> thrLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> oneHasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> twoHasMoreLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> thrHasMoreLiveData = new MutableLiveData<>();

    public MemberInviteIncomeVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<MemberInviteIncomeBean>> getOneLiveData() {

        return oneLiveData;

    }

    public MutableLiveData<ArrayList<MemberInviteIncomeBean>> getTwoLiveData() {

        return twoLiveData;

    }

    public MutableLiveData<ArrayList<MemberInviteIncomeBean>> getThrLiveData() {

        return thrLiveData;

    }

    public MutableLiveData<Boolean> getOneHasMoreLiveData() {

        return oneHasMoreLiveData;

    }

    public MutableLiveData<Boolean> getTwoHasMoreLiveData() {

        return twoHasMoreLiveData;

    }

    public MutableLiveData<Boolean> getThrHasMoreLiveData() {

        return thrHasMoreLiveData;

    }

    public void getOne(String page) {

        MemberInviteController.inviteOne(page, new HttpCallBack<ArrayList<MemberInviteIncomeBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<MemberInviteIncomeBean> list) {
                oneLiveData.setValue(list);
                oneHasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getTwo(String page) {

        MemberInviteController.inviteTwo(page, new HttpCallBack<ArrayList<MemberInviteIncomeBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<MemberInviteIncomeBean> list) {
                twoLiveData.setValue(list);
                twoHasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void getThr(String page) {

        MemberInviteController.inviteThr(page, new HttpCallBack<ArrayList<MemberInviteIncomeBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<MemberInviteIncomeBean> list) {
                thrLiveData.setValue(list);
                thrHasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
