package top.yokey.shopai.member.activity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.Objects;

import top.yokey.shopai.R;
import top.yokey.shopai.member.viewmodel.MemberPasswordVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.other.CountDown;
import top.yokey.shopai.zcom.util.ConvertUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;

@Route(path = ARoutePath.MEMBER_PASSWORD)
public class MemberPasswordActivity extends BaseActivity {

    private Toolbar mainToolbar = null;
    private AppCompatEditText captchaEditText = null;
    private AppCompatTextView captchaTextView = null;
    private AppCompatEditText passwordEditText = null;
    private AppCompatImageView passwordClearImageView = null;
    private AppCompatTextView modifyTextView = null;

    private MemberPasswordVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_password);
        mainToolbar = findViewById(R.id.mainToolbar);
        captchaEditText = findViewById(R.id.captchaEditText);
        captchaTextView = findViewById(R.id.captchaTextView);
        passwordEditText = findViewById(R.id.passwordEditText);
        passwordClearImageView = findViewById(R.id.passwordClearImageView);
        modifyTextView = findViewById(R.id.modifyTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.modifyPassword);
        observeKeyborad(R.id.mainLinearLayout);
        vm = getVM(MemberPasswordVM.class);
        canModify();

    }

    @Override
    public void initEvent() {

        captchaEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (VerifyUtil.isEmpty(Objects.requireNonNull(captchaEditText.getText()).toString())) {
                    captchaEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_code), null, null, null);
                } else {
                    captchaEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_code_accent), null, null, null);
                }
                canModify();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        captchaTextView.setOnClickListener(view -> {
            captchaTextView.setEnabled(false);
            captchaTextView.setText(R.string.handlerIng);
            vm.step2();
        });

        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (VerifyUtil.isEmpty(Objects.requireNonNull(passwordEditText.getText()).toString())) {
                    passwordClearImageView.setVisibility(View.GONE);
                    passwordEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_password), null, null, null);
                } else {
                    passwordClearImageView.setVisibility(View.VISIBLE);
                    passwordEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(App.get().getDrawables(R.drawable.ic_hint_password_accent), null, null, null);
                }
                canModify();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        passwordClearImageView.setOnClickListener(view -> passwordEditText.setText(""));

        modifyTextView.setOnClickListener(view -> {
            captchaEditText.setEnabled(false);
            captchaTextView.setEnabled(false);
            passwordEditText.setEnabled(false);
            passwordClearImageView.setEnabled(false);
            modifyTextView.setEnabled(false);
            modifyTextView.setText(R.string.tipsCheckCaptcha);
            modifyTextView.setTextColor(App.get().getColors(R.color.textThr));
            String captcha = Objects.requireNonNull(captchaEditText.getText()).toString();
            vm.step3(captcha);
        });

    }

    @Override
    public void initObserve() {

        vm.getStep2LiveData().observe(this, string -> {
            ToastHelp.get().show(R.string.captchaSendSuccess);
            final int time = ConvertUtil.string2Int(string);
            new CountDown(time * 1000, Constant.TIME_TICK) {
                int count = time;

                @Override
                public void onTick(long millisUntilFinished) {
                    super.onTick(millisUntilFinished);
                    count--;
                    String temp = count + " S";
                    captchaTextView.setText(temp);
                    captchaTextView.setTextColor(App.get().getColors(R.color.textThr));
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    captchaTextView.setEnabled(true);
                    captchaTextView.setText(R.string.get);
                    captchaTextView.setTextColor(App.get().getColors(R.color.accent));
                }
            }.start();
        });

        vm.getStep3LiveData().observe(this, string -> {
            modifyTextView.setText(R.string.handlerIng);
            modifyTextView.setTextColor(App.get().getColors(R.color.textThr));
            vm.step4();
        });

        vm.getStep4LiveData().observe(this, string -> {
            modifyTextView.setText(R.string.handlerIng);
            modifyTextView.setTextColor(App.get().getColors(R.color.textThr));
            vm.step5(Objects.requireNonNull(passwordEditText.getText()).toString());
        });

        vm.getStep5LiveData().observe(this, string -> {
            ToastHelp.get().show(R.string.passwordModifySuccess);
            onReturn(false);
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                ToastHelp.get().show(bean.getReason());
                captchaTextView.setText(R.string.get);
                captchaTextView.setEnabled(true);
            } else {
                ToastHelp.get().show(bean.getReason());
                captchaEditText.setEnabled(true);
                captchaTextView.setEnabled(true);
                passwordEditText.setEnabled(true);
                passwordClearImageView.setEnabled(true);
                modifyTextView.setEnabled(true);
                modifyTextView.setTextColor(App.get().getColors(R.color.accent));
                modifyTextView.setText(R.string.modify);
            }
        });

    }

    //自定义方法

    private void canModify() {

        if (passwordClearImageView.getVisibility() == View.VISIBLE && !VerifyUtil.isEmpty(Objects.requireNonNull(captchaEditText.getText()).toString())) {
            modifyTextView.setEnabled(true);
            modifyTextView.setTextColor(App.get().getColors(R.color.accent));
        } else {
            modifyTextView.setEnabled(false);
            modifyTextView.setTextColor(App.get().getColors(R.color.textThr));
        }

    }

}
