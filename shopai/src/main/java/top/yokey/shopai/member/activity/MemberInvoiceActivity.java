package top.yokey.shopai.member.activity;

import android.content.Intent;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.member.adapter.MemberInvoiceAdapter;
import top.yokey.shopai.member.viewmodel.MemberInvoiceVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.InvoiceListBean;

@Route(path = ARoutePath.MEMBER_INVOICE)
public class MemberInvoiceActivity extends BaseActivity {

    private final ArrayList<InvoiceListBean> arrayList = new ArrayList<>();
    private final MemberInvoiceAdapter adapter = new MemberInvoiceAdapter(arrayList);
    @Autowired(name = Constant.DATA_ID)
    String id;
    private Toolbar mainToolbar = null;
    private AppCompatImageView toolbarImageView = null;
    private PullRefreshView mainPullRefreshView = null;
    private MemberInvoiceVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.mineInvoice);
        observeKeyborad(R.id.mainLinearLayout);
        toolbarImageView.setImageResource(R.drawable.ic_action_plus);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setCanLoadMore(false);
        mainPullRefreshView.setItemDecoration(new LineDecoration(App.get().dp2Px(16), true));
        vm = getVM(MemberInvoiceVM.class);

    }

    @Override
    public void initEvent() {

        toolbarImageView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_INVOICE_ADD));

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }

            @Override
            public void onLoadMore() {

            }
        });

        adapter.setOnItemClickListener(new MemberInvoiceAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, InvoiceListBean bean) {
                if (!VerifyUtil.isEmpty(id)) {
                    Intent intent = new Intent();
                    intent.putExtra(Constant.DATA_ID, bean.getInvId());
                    intent.putExtra(Constant.DATA_CONTENT, bean.getInvTitle() + " " + bean.getInvContent());
                    App.get().finishOk(get(), intent);
                }
            }

            @Override
            public void onClickDelete(int position, InvoiceListBean bean) {
                vm.delInvoice(bean.getInvId());
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getInvoiceLiveData().observe(this, list -> {
            arrayList.clear();
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getDelLiveData().observe(this, string -> {
            ToastHelp.get().showSuccess();
            getData();
        });

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList.size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    mainPullRefreshView.setError(bean.getReason());
                }
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();
        getData();

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getInvoice();

    }

}
