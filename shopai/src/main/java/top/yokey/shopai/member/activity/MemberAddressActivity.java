package top.yokey.shopai.member.activity;

import android.content.Intent;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.member.adapter.MemberAddressAdapter;
import top.yokey.shopai.member.viewmodel.MemberAddressVM;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;
import top.yokey.shopai.zcom.help.ToastHelp;
import top.yokey.shopai.zcom.other.Constant;
import top.yokey.shopai.zcom.recycler.LineDecoration;
import top.yokey.shopai.zcom.util.JsonUtil;
import top.yokey.shopai.zcom.util.VerifyUtil;
import top.yokey.shopai.zcom.view.PullRefreshView;
import top.yokey.shopai.zsdk.bean.AddressBean;
import top.yokey.shopai.zsdk.data.AddressData;

@Route(path = ARoutePath.MEMBER_ADDRESS)
public class MemberAddressActivity extends BaseActivity {

    private final ArrayList<AddressBean> arrayList = new ArrayList<>();
    private final MemberAddressAdapter adapter = new MemberAddressAdapter(arrayList);
    @Autowired(name = Constant.DATA_ID)
    String id;
    private Toolbar mainToolbar = null;
    private AppCompatImageView toolbarImageView = null;
    private PullRefreshView mainPullRefreshView = null;
    private MemberAddressVM vm = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_pull_refresh_view);
        mainToolbar = findViewById(R.id.mainToolbar);
        toolbarImageView = findViewById(R.id.toolbarImageView);
        mainPullRefreshView = findViewById(R.id.mainPullRefreshView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.receiveAddress);
        observeKeyborad(R.id.mainLinearLayout);
        toolbarImageView.setImageResource(R.drawable.ic_action_plus);
        mainPullRefreshView.setAdapter(adapter);
        mainPullRefreshView.setCanLoadMore(false);
        mainPullRefreshView.setItemDecoration(new LineDecoration(App.get().dp2Px(16), true));
        vm = getVM(MemberAddressVM.class);

    }

    @Override
    public void initEvent() {

        toolbarImageView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_ADDRESS_ADD));

        mainPullRefreshView.setOnClickListener(view -> {
            if (mainPullRefreshView.isError() || arrayList.size() == 0) {
                getData();
            }
        });

        mainPullRefreshView.setOnRefreshListener(new PullRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }

            @Override
            public void onLoadMore() {

            }
        });

        adapter.setOnItemClickListener(new MemberAddressAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position, AddressBean bean) {
                if (VerifyUtil.isEmpty(id)) {
                    App.get().start(ARoutePath.MEMBER_ADDRESS_MODIFY, Constant.DATA_JSON, JsonUtil.toJson(bean));
                    return;
                }
                Intent intent = new Intent();
                intent.putExtra(Constant.DATA_ID, bean.getAddressId());
                App.get().finishOk(get(), intent);
            }

            @Override
            public void onClickModify(int position, AddressBean bean) {
                App.get().start(ARoutePath.MEMBER_ADDRESS_MODIFY, Constant.DATA_JSON, JsonUtil.toJson(bean));
            }

            @Override
            public void onClickDelete(int position, AddressBean bean) {
                vm.delete(bean.getAddressId());
            }

            @Override
            public void onClickDefault(int position, AddressBean bean) {
                AddressData data = new AddressData();
                data.setAddress(bean.getAddress());
                data.setAddressId(bean.getAddressId());
                data.setAreaId(bean.getAreaId());
                data.setAreaInfo(bean.getAreaInfo());
                data.setCityId(bean.getCityId());
                data.setIsDefault(Constant.COMMON_ENABLE);
                data.setMobPhone(bean.getMobPhone());
                data.setTrueName(bean.getTrueName());
                vm.setDefault(data);
            }
        });

    }

    @Override
    public void initObserve() {

        vm.getAddressLiveData().observe(this, list -> {
            arrayList.clear();
            arrayList.addAll(list);
            mainPullRefreshView.setComplete();
        });

        vm.getDefaultLiveData().observe(this, string -> getData());

        vm.getDeleteLiveData().observe(this, string -> getData());

        vm.getErrorLiveData().observe(this, bean -> {
            if (bean.getCode() == 1) {
                if (arrayList.size() != 0) {
                    ToastHelp.get().show(bean.getReason());
                } else {
                    mainPullRefreshView.setError(bean.getReason());
                }
            } else {
                ToastHelp.get().show(bean.getReason());
            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();
        getData();

    }

    //自定义方法

    private void getData() {

        mainPullRefreshView.setLoad();
        vm.getAddress();

    }

}
