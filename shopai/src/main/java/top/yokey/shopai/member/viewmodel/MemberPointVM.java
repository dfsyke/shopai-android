package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.MemberAssetBean;
import top.yokey.shopai.zsdk.bean.PointLogBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberIndexController;
import top.yokey.shopai.zsdk.controller.MemberPointController;

public class MemberPointVM extends BaseViewModel {

    private final MutableLiveData<MemberAssetBean> assetLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<PointLogBean>> logLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> hasMoreLiveData = new MutableLiveData<>();

    public MemberPointVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<MemberAssetBean> getAssetLiveData() {

        return assetLiveData;

    }

    public MutableLiveData<ArrayList<PointLogBean>> getLogLiveData() {

        return logLiveData;

    }

    public MutableLiveData<Boolean> getHasMoreLiveData() {

        return hasMoreLiveData;

    }

    public void getAsset() {

        MemberIndexController.myAsset(new HttpCallBack<MemberAssetBean>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, MemberAssetBean bean) {
                assetLiveData.setValue(bean);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getLog(String page) {

        MemberPointController.pointsLog(page, new HttpCallBack<ArrayList<PointLogBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<PointLogBean> list) {
                logLiveData.setValue(list);
                hasMoreLiveData.setValue(baseBean.isHasmore());
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

}
