package top.yokey.shopai.member.activity;

import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;

@Route(path = ARoutePath.MEMBER_SETTING)
public class MemberSettingActivity extends BaseActivity {

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void initEvent() {

    }

    @Override
    public void initObserve() {

    }

}
