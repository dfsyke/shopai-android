package top.yokey.shopai.member.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zsdk.bean.PreDepositCashLogBean;

public class MemberPreDepositCashLogAdapter extends RecyclerView.Adapter<MemberPreDepositCashLogAdapter.ViewHolder> {

    private final ArrayList<PreDepositCashLogBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberPreDepositCashLogAdapter(ArrayList<PreDepositCashLogBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        PreDepositCashLogBean bean = arrayList.get(position);
        holder.mainTextView.setText(String.format(App.get().getString(R.string.cashExamineState), bean.getPdcPaymentStateText()));
        holder.priceTextView.setText("￥");
        holder.priceTextView.append(bean.getPdcAmount());
        holder.descTextView.setText(String.format(App.get().getString(R.string.orderPaySn), bean.getPdcSn()));
        holder.timeTextView.setText(bean.getPdcAddTimeText());

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {

        View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_member_pre_deposit_cash_log, group, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, PreDepositCashLogBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatTextView mainTextView;
        private final AppCompatTextView priceTextView;
        private final AppCompatTextView descTextView;
        private final AppCompatTextView timeTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainTextView = view.findViewById(R.id.mainTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            descTextView = view.findViewById(R.id.descTextView);
            timeTextView = view.findViewById(R.id.timeTextView);

        }

    }

}
