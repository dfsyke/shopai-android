package top.yokey.shopai.member.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import top.yokey.shopai.zcom.base.BaseErrorBean;
import top.yokey.shopai.zcom.base.BaseViewModel;
import top.yokey.shopai.zsdk.bean.BaseBean;
import top.yokey.shopai.zsdk.bean.CouponBean;
import top.yokey.shopai.zsdk.callback.HttpCallBack;
import top.yokey.shopai.zsdk.controller.MemberCouponController;
import top.yokey.shopai.zsdk.controller.VercodeController;

public class MemberCouponVM extends BaseViewModel {

    private final MutableLiveData<ArrayList<CouponBean>> couponLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> codeKeyLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> addLiveData = new MutableLiveData<>();

    public MemberCouponVM(@NonNull Application application) {

        super(application);

    }

    public MutableLiveData<ArrayList<CouponBean>> getCouponLiveData() {

        return couponLiveData;

    }

    public MutableLiveData<String> getCodeKeyLiveData() {

        return codeKeyLiveData;

    }

    public MutableLiveData<String> getAddLiveData() {

        return addLiveData;

    }

    public void getCoupon() {

        MemberCouponController.couponLst(new HttpCallBack<ArrayList<CouponBean>>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, ArrayList<CouponBean> list) {
                couponLiveData.setValue(list);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(1, reason));
            }
        });

    }

    public void getCodeKey() {

        VercodeController.makeCodeKey(new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                codeKeyLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(2, reason));
            }
        });

    }

    public void add(String pwdCode, String captcha, String codeKey) {

        MemberCouponController.rpPwex(pwdCode, captcha, codeKey, new HttpCallBack<String>() {
            @Override
            public void onSuccess(String result, BaseBean baseBean, String string) {
                addLiveData.setValue(string);
            }

            @Override
            public void onFailure(String reason) {
                getErrorLiveData().setValue(new BaseErrorBean(3, reason));
            }
        });

    }

}
