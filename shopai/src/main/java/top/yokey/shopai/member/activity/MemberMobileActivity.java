package top.yokey.shopai.member.activity;

import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.facade.annotation.Route;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.App;
import top.yokey.shopai.zcom.arouter.ARoutePath;
import top.yokey.shopai.zcom.base.BaseActivity;

@Route(path = ARoutePath.MEMBER_MOBILE)
public class MemberMobileActivity extends BaseActivity {

    private Toolbar mainToolbar = null;
    private AppCompatImageView checkImageView = null;
    private AppCompatTextView tipsTextView = null;
    private AppCompatTextView bindTextView = null;
    private AppCompatTextView untyingTextView = null;

    @Override
    public void initView() {

        setContentView(R.layout.activity_member_mobile);
        mainToolbar = findViewById(R.id.mainToolbar);
        checkImageView = findViewById(R.id.checkImageView);
        tipsTextView = findViewById(R.id.tipsTextView);
        bindTextView = findViewById(R.id.bindTextView);
        untyingTextView = findViewById(R.id.untyingTextView);

    }

    @Override
    public void initData() {

        setToolbar(mainToolbar, R.string.mobile);
        observeKeyborad(R.id.mainLinearLayout);

    }

    @Override
    public void initEvent() {

        bindTextView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_MOBILE_BIND));

        untyingTextView.setOnClickListener(view -> App.get().start(ARoutePath.MEMBER_MOBILE_UNTYING));

    }

    @Override
    public void initObserve() {

    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    //自定义方法

    private void setData() {

        if (App.get().getMemberBean().getMemberInfoAll().getMemberMobileBind().equals("1")) {
            tipsTextView.setText(String.format(getString(R.string.mineBindMobile), App.get().getMemberBean().getMemberInfoAll().getMemberMobile()));
            checkImageView.setVisibility(View.VISIBLE);
            bindTextView.setVisibility(View.GONE);
            untyingTextView.setVisibility(View.VISIBLE);
        } else {
            tipsTextView.setText(R.string.tipsUnbindMobile);
            checkImageView.setVisibility(View.GONE);
            bindTextView.setVisibility(View.VISIBLE);
            untyingTextView.setVisibility(View.GONE);
        }

    }

}
