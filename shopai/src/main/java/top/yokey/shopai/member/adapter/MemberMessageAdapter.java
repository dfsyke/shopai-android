package top.yokey.shopai.member.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import top.yokey.shopai.R;
import top.yokey.shopai.zcom.help.ImageHelp;
import top.yokey.shopai.zsdk.bean.MessageListBean;

public class MemberMessageAdapter extends RecyclerView.Adapter<MemberMessageAdapter.ViewHolder> {

    private final ArrayList<MessageListBean> arrayList;
    private OnItemClickListener onItemClickListener = null;

    public MemberMessageAdapter(ArrayList<MessageListBean> arrayList) {

        this.arrayList = arrayList;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        MessageListBean bean = arrayList.get(position);
        ImageHelp.get().displayRadius(bean.getAvatar(), holder.mainImageView);
        holder.nameTextView.setText(bean.getUName());
        holder.timeTextView.setText(bean.getTime());
        String msg = bean.getTMsg();
        holder.contentTextView.setText(msg);
        if (msg.contains("SIMG")) {
            holder.contentTextView.setText("[图片]");
        }
        if (msg.startsWith(":") && msg.endsWith(":")) {
            holder.contentTextView.setText("[表情]");
        }
        holder.stateTextView.setVisibility(bean.getRState().equals("2") ? View.VISIBLE : View.GONE);

        holder.mainRelativeLayout.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onClick(position, bean);
            }
        });

        holder.mainRelativeLayout.setOnLongClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onLongClick(position, bean);
            }
            return false;
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_member_message, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public int getItemCount() {

        return arrayList.size();

    }

    public void setOnItemClickListener(OnItemClickListener listener) {

        this.onItemClickListener = listener;

    }

    public interface OnItemClickListener {

        @SuppressWarnings({"EmptyMethod"})
        void onClick(int position, MessageListBean bean);

        @SuppressWarnings({"EmptyMethod"})
        void onLongClick(int position, MessageListBean bean);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final RelativeLayout mainRelativeLayout;
        private final AppCompatImageView mainImageView;
        private final AppCompatTextView nameTextView;
        private final AppCompatTextView timeTextView;
        private final AppCompatTextView contentTextView;
        private final AppCompatTextView stateTextView;

        private ViewHolder(View view) {

            super(view);
            mainRelativeLayout = view.findViewById(R.id.mainRelativeLayout);
            mainImageView = view.findViewById(R.id.mainImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            timeTextView = view.findViewById(R.id.timeTextView);
            contentTextView = view.findViewById(R.id.contentTextView);
            stateTextView = view.findViewById(R.id.stateTextView);

        }

    }

}
