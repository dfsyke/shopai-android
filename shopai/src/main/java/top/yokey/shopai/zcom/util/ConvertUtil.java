package top.yokey.shopai.zcom.util;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

@SuppressWarnings("ALL")
public class ConvertUtil {

    public static int string2Int(String content) {

        try {
            return Integer.parseInt(content);
        } catch (NumberFormatException e) {
            return 0;
        }

    }

    public static long string2Long(String content) {

        try {
            return Long.parseLong(content);
        } catch (NumberFormatException e) {
            return 0;
        }

    }

    public static float string2Float(String content) {

        try {
            return Float.parseFloat(content);
        } catch (NumberFormatException e) {
            return 0;
        }

    }

    public static double string2Double(String content) {

        try {
            return Double.parseDouble(content);
        } catch (NumberFormatException e) {
            return 0;
        }

    }

    public static String path2Base64(String path) {

        try {
            InputStream inputStream = new FileInputStream(path);
            byte[] data = new byte[inputStream.available()];
            inputStream.read(data);
            return "data:image/jpeg;base64," + Base64.encodeToString(data, Base64.DEFAULT);
        } catch (IOException e) {
            return "";
        }

    }

    public static File uri2File(Activity activity, Uri uri) {

        String path = "";
        if (uri.getScheme().equals("file")) {
            path = uri.getEncodedPath();
            if (path != null) {
                path = Uri.decode(path);
                ContentResolver contentResolver = activity.getContentResolver();
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("(").append(MediaStore.Images.ImageColumns.DATA).append("=").append("'" + path + "'").append(")");
                Cursor cursor = contentResolver.query(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        new String[]{MediaStore.Images.ImageColumns._ID, MediaStore.Images.ImageColumns.DATA}, stringBuffer.toString(), null, null
                );
                int index = 0;
                int data = 0;
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    index = cursor.getColumnIndex(MediaStore.Images.ImageColumns._ID);
                    index = cursor.getInt(index);
                    data = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    path = cursor.getString(data);
                }
                cursor.close();
            }
            if (path != null) {
                return new File(path);
            }
        }

        if (uri.getScheme().equals("content")) {
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = activity.getContentResolver().query(uri, proj, null, null, null);
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                path = cursor.getString(columnIndex);
            }
            cursor.close();
            return new File(path);
        }

        return null;

    }

    public static Uri file2Uri(Activity activity, File file) {

        String path = file.getAbsolutePath();
        Cursor cursor = activity.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID}, MediaStore.Images.Media.DATA + "=? ",
                new String[]{path}, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            Uri uri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(uri, "" + id);
        } else {
            if (file.exists()) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(MediaStore.Images.Media.DATA, path);
                return activity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
            } else {
                return null;
            }
        }

    }

    public static String uri2Base64(Activity activity, Uri uri) {

        try {
            InputStream inputStream = activity.getContentResolver().openInputStream(uri);
            byte[] data = new byte[inputStream.available()];
            inputStream.read(data);
            return "data:image/jpeg;base64," + Base64.encodeToString(data, Base64.DEFAULT);
        } catch (IOException e) {
            return "";
        }

    }

    public static Bitmap uri2Bitmap(Activity activity, Uri uri) {

        try {
            return BitmapFactory.decodeStream(activity.getContentResolver().openInputStream(uri));
        } catch (FileNotFoundException e) {
            return null;
        }

    }

}
