package top.yokey.shopai.zcom.util;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;

@SuppressWarnings("ALL")
public class AnimUtil {

    public static final int TIME = 500;
    public static final String ALPHA = "alpha";
    public static final String SCALE_X = "scaleX";
    public static final String SCALE_Y = "scaleY";
    public static final String TRABSLATION_X = "translationX";
    public static final String TRABSLATION_Y = "translationY";

    public static void objectAnimator(View view, String name, float... values) {

        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, name, values);
        objectAnimator.setDuration(TIME).start();

    }

    public static void objectAnimator(View view, String name, AnimatorCallBack callBack, float... values) {

        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, name, values);
        objectAnimator.setDuration(TIME).start();
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (callBack != null) {
                    callBack.onEnd();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }

    public interface AnimatorCallBack {

        void onEnd();

    }

}
