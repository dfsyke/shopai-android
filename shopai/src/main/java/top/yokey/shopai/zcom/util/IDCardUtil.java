package top.yokey.shopai.zcom.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("ALL")
public class IDCardUtil {

    private static final int CHINA_ID_MIN_LENGTH = 15;
    private static final int CHINA_ID_MAX_LENGTH = 18;
    private static final int power[] = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
    private static Map<String, String> cityCodes = new HashMap<>();
    private static Map<String, Integer> twFirstCode = new HashMap<>();
    private static Map<String, Integer> hkFirstCode = new HashMap<>();

    static {
        cityCodes.put("11", "北京");
        cityCodes.put("12", "天津");
        cityCodes.put("13", "河北");
        cityCodes.put("14", "山西");
        cityCodes.put("15", "内蒙古");
        cityCodes.put("21", "辽宁");
        cityCodes.put("22", "吉林");
        cityCodes.put("23", "黑龙江");
        cityCodes.put("31", "上海");
        cityCodes.put("32", "江苏");
        cityCodes.put("33", "浙江");
        cityCodes.put("34", "安徽");
        cityCodes.put("35", "福建");
        cityCodes.put("36", "江西");
        cityCodes.put("37", "山东");
        cityCodes.put("41", "河南");
        cityCodes.put("42", "湖北");
        cityCodes.put("43", "湖南");
        cityCodes.put("44", "广东");
        cityCodes.put("45", "广西");
        cityCodes.put("46", "海南");
        cityCodes.put("50", "重庆");
        cityCodes.put("51", "四川");
        cityCodes.put("52", "贵州");
        cityCodes.put("53", "云南");
        cityCodes.put("54", "西藏");
        cityCodes.put("61", "陕西");
        cityCodes.put("62", "甘肃");
        cityCodes.put("63", "青海");
        cityCodes.put("64", "宁夏");
        cityCodes.put("65", "新疆");
        cityCodes.put("71", "台湾");
        cityCodes.put("81", "香港");
        cityCodes.put("82", "澳门");
        cityCodes.put("91", "国外");
        twFirstCode.put("A", 10);
        twFirstCode.put("B", 11);
        twFirstCode.put("C", 12);
        twFirstCode.put("D", 13);
        twFirstCode.put("E", 14);
        twFirstCode.put("F", 15);
        twFirstCode.put("G", 16);
        twFirstCode.put("H", 17);
        twFirstCode.put("J", 18);
        twFirstCode.put("K", 19);
        twFirstCode.put("L", 20);
        twFirstCode.put("M", 21);
        twFirstCode.put("N", 22);
        twFirstCode.put("P", 23);
        twFirstCode.put("Q", 24);
        twFirstCode.put("R", 25);
        twFirstCode.put("S", 26);
        twFirstCode.put("T", 27);
        twFirstCode.put("U", 28);
        twFirstCode.put("V", 29);
        twFirstCode.put("X", 30);
        twFirstCode.put("Y", 31);
        twFirstCode.put("W", 32);
        twFirstCode.put("Z", 33);
        twFirstCode.put("I", 34);
        twFirstCode.put("O", 35);
        hkFirstCode.put("A", 1);
        hkFirstCode.put("B", 2);
        hkFirstCode.put("C", 3);
        hkFirstCode.put("R", 18);
        hkFirstCode.put("U", 21);
        hkFirstCode.put("Z", 26);
        hkFirstCode.put("X", 24);
        hkFirstCode.put("W", 23);
        hkFirstCode.put("O", 15);
        hkFirstCode.put("N", 14);
    }

    //私有方法

    private static int getPowerSum(int[] iArr) {

        int iSum = 0;
        if (power.length == iArr.length) {
            for (int i = 0; i < iArr.length; i++) {
                for (int j = 0; j < power.length; j++) {
                    if (i == j) {
                        iSum = iSum + iArr[i] * power[j];
                    }
                }
            }
        }
        return iSum;

    }

    private static int[] convertChar2Int(char[] ca) {

        int len = ca.length;
        int[] iArr = new int[len];
        for (int i = 0; i < len; i++) {
            iArr[i] = ConvertUtil.string2Int(String.valueOf(ca[i]));
        }
        return iArr;

    }

    private static boolean isNum(String val) {

        return !(val == null || "".equals(val)) && val.matches("^[0-9]*$");

    }

    private static boolean validateIdCard18(String idCard) {

        boolean bTrue = false;
        if (idCard == null) {
            return false;
        }
        if (idCard.length() == CHINA_ID_MAX_LENGTH) {
            String code17 = idCard.substring(0, 17);
            String code18 = idCard.substring(17, CHINA_ID_MAX_LENGTH);
            if (isNum(code17)) {
                char[] cArr = code17.toCharArray();
                if (cArr != null) {
                    int[] iCard = convertChar2Int(cArr);
                    int iSum17 = getPowerSum(iCard);
                    String val = getCheckCode18(iSum17);
                    if (val.length() > 0) {
                        if (val.equalsIgnoreCase(code18)) {
                            bTrue = true;
                        }
                    }
                }
            }
        }
        return bTrue;

    }

    private static boolean validateIdCard15(String idCard) {

        if (idCard.length() != CHINA_ID_MIN_LENGTH) {
            return false;
        }
        if (isNum(idCard)) {
            String proCode = idCard.substring(0, 2);
            if (cityCodes.get(proCode) == null) {
                return false;
            }
            String birthCode = idCard.substring(6, 12);
            Date birthDate = null;
            try {
                birthDate = new SimpleDateFormat("yy").parse(birthCode.substring(0, 2));
            } catch (ParseException e) {
                birthDate = null;
            }
            Calendar cal = Calendar.getInstance();
            if (birthDate != null) cal.setTime(birthDate);
            if (!validateDateSmllerThenNow(cal.get(Calendar.YEAR), Integer.valueOf(birthCode.substring(2, 4)), Integer.valueOf(birthCode.substring(4, 6)))) {
                return false;
            }
        } else {
            return false;
        }
        return true;

    }

    private static boolean validateDateSmllerThenNow(int iYear, int iMonth, int iDate) {

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int datePerMonth;
        int MIN = 1930;
        if (iYear < MIN || iYear >= year) {
            return false;
        }
        if (iMonth < 1 || iMonth > 12) {
            return false;
        }
        switch (iMonth) {
            case 4:
            case 6:
            case 9:
            case 11:
                datePerMonth = 30;
                break;
            case 2:
                boolean dm = ((iYear % 4 == 0 && iYear % 100 != 0) || (iYear % 400 == 0)) && (iYear > MIN && iYear < year);
                datePerMonth = dm ? 29 : 28;
                break;
            default:
                datePerMonth = 31;
        }
        return (iDate >= 1) && (iDate <= datePerMonth);

    }

    private static String getBirth(String idCard) {

        Integer len = idCard.length();
        if (len < CHINA_ID_MIN_LENGTH) {
            return "";
        } else if (len == CHINA_ID_MIN_LENGTH) {
            idCard = convert15Card218(idCard);
        }
        return idCard.substring(6, 14);

    }

    private static String getCheckCode18(int iSum) {

        String sCode = "";
        switch (iSum % 11) {
            case 10:
                sCode = "2";
                break;
            case 9:
                sCode = "3";
                break;
            case 8:
                sCode = "4";
                break;
            case 7:
                sCode = "5";
                break;
            case 6:
                sCode = "6";
                break;
            case 5:
                sCode = "7";
                break;
            case 4:
                sCode = "8";
                break;
            case 3:
                sCode = "9";
                break;
            case 2:
                sCode = "x";
                break;
            case 1:
                sCode = "0";
                break;
            case 0:
                sCode = "1";
                break;
        }
        return sCode;

    }

    private static String convert15Card218(String idCard) {

        String idCard18 = "";
        if (idCard.length() != CHINA_ID_MIN_LENGTH) {
            return "";
        }
        if (isNum(idCard)) {
            String birthday = idCard.substring(6, 12);
            Date birthDate = null;
            try {
                birthDate = new SimpleDateFormat("yyMMdd").parse(birthday);
            } catch (ParseException e) {
                birthDate = null;
            }
            Calendar cal = Calendar.getInstance();
            if (birthDate != null) cal.setTime(birthDate);
            String sYear = String.valueOf(cal.get(Calendar.YEAR));
            idCard18 = idCard.substring(0, 6) + sYear + idCard.substring(8);
            char[] cArr = idCard18.toCharArray();
            if (cArr != null) {
                int[] iCard = convertChar2Int(cArr);
                int iSum17 = getPowerSum(iCard);
                String sVal = getCheckCode18(iSum17);
                if (sVal.length() > 0) {
                    idCard18 += sVal;
                } else {
                    return "";
                }
            }
        } else {
            return "";
        }
        return idCard18;

    }

    private static String[] validateIdCard10(String idCard) {

        String[] info = new String[3];
        String card = idCard.replaceAll("[\\(|\\)]", "");
        if (card.length() != 8 && card.length() != 9 && idCard.length() != 10) {
            return new String[]{};
        }
        if (idCard.matches("^[a-zA-Z][0-9]{9}$")) {
            info[0] = "台湾";
            String char2 = idCard.substring(1, 2);
            if (char2.equals("1")) {
                info[1] = "M";
            } else if (char2.equals("2")) {
                info[1] = "F";
            } else {
                info[1] = "N";
                info[2] = "false";
                return info;
            }
            info[2] = isTWCard(idCard) ? "true" : "false";
        } else if (idCard.matches("^[1|5|7][0-9]{6}\\(?[0-9A-Z]\\)?$")) {
            info[0] = "澳门";
            info[1] = "N";
        } else if (idCard.matches("^[A-Z]{1,2}[0-9]{6}\\(?[0-9A]\\)?$")) {
            info[0] = "香港";
            info[1] = "N";
            info[2] = isHKIdCard(idCard) ? "true" : "false";
        } else {
            return new String[]{};
        }
        return info;

    }

    //公共方法

    public static int getAge(String idCard) {

        int iAge = 0;
        if (idCard.length() == CHINA_ID_MIN_LENGTH) {
            idCard = convert15Card218(idCard);
        }
        String year = idCard.substring(6, 10);
        Calendar cal = Calendar.getInstance();
        int iCurrYear = cal.get(Calendar.YEAR);
        iAge = iCurrYear - Integer.valueOf(year);
        return iAge;

    }

    public static boolean isIdCard(String idCard) {

        idCard = idCard.trim();
        if (validateIdCard18(idCard)) {
            return true;
        }
        if (validateIdCard15(idCard)) {
            return true;
        }
        String[] card = validateIdCard10(idCard);
        if (card != null) {
            return card[2].equals("true");
        }
        return false;

    }

    public static boolean isTWCard(String idCard) {

        String start = idCard.substring(0, 1);
        String mid = idCard.substring(1, 9);
        String end = idCard.substring(9, 10);
        Integer iStart = twFirstCode.get(start);
        Integer sum = iStart / 10 + (iStart % 10) * 9;
        char[] chars = mid.toCharArray();
        Integer iflag = 8;
        for (char c : chars) {
            sum = sum + Integer.valueOf(c + "") * iflag;
            iflag--;
        }
        return (sum % 10 == 0 ? 0 : 10 - sum % 10) == Integer.valueOf(end);

    }

    public static boolean isHKIdCard(String idCard) {

        String card = idCard.replaceAll("[\\(|\\)]", "");
        Integer sum = 0;
        if (card.length() == 9) {
            sum = ((int) card.substring(0, 1).toUpperCase().toCharArray()[0] - 55) * 9 + ((int) card.substring(1, 2).toUpperCase().toCharArray()[0] - 55) * 8;
            card = card.substring(1, 9);
        } else {
            sum = 522 + ((int) card.substring(0, 1).toUpperCase().toCharArray()[0] - 55) * 8;
        }
        String mid = card.substring(1, 7);
        String end = card.substring(7, 8);
        char[] chars = mid.toCharArray();
        Integer iflag = 7;
        for (char c : chars) {
            sum = sum + Integer.valueOf(c + "") * iflag;
            iflag--;
        }
        if (end.toUpperCase().equals("A")) {
            sum = sum + 10;
        } else {
            sum = sum + Integer.valueOf(end);
        }
        return (sum % 11 == 0);

    }

    public static Short getYear(String idCard) {

        Integer len = idCard.length();
        if (len < CHINA_ID_MIN_LENGTH) {
            return 0;
        } else if (len == CHINA_ID_MIN_LENGTH) {
            idCard = convert15Card218(idCard);
        }
        return Short.valueOf(idCard.substring(6, 10));

    }

    public static Short getMonth(String idCard) {

        Integer len = idCard.length();
        if (len < CHINA_ID_MIN_LENGTH) {
            return 0;
        } else if (len == CHINA_ID_MIN_LENGTH) {
            idCard = convert15Card218(idCard);
        }
        return Short.valueOf(idCard.substring(10, 12));

    }

    public static Short getDay(String idCard) {

        Integer len = idCard.length();
        if (len < CHINA_ID_MIN_LENGTH) {
            return 0;
        } else if (len == CHINA_ID_MIN_LENGTH) {
            idCard = convert15Card218(idCard);
        }
        return Short.valueOf(idCard.substring(12, 14));

    }

    public static String getGender(String idCard) {

        String sGender = "N";
        if (idCard.length() == CHINA_ID_MIN_LENGTH) {
            idCard = convert15Card218(idCard);
        }
        String sCardNum = idCard.substring(16, 17);
        if (ConvertUtil.string2Int(sCardNum) % 2 != 0) {
            sGender = "M";
        } else {
            sGender = "F";
        }
        return sGender;

    }

    public static String getBirthday(String idCard) {

        return getBirth(idCard).replaceAll("(\\d{4})(\\d{2})(\\d{2})", "$1-$2-$3");

    }

    public static String getProvince(String idCard) {

        int len = idCard.length();
        String sProvince = null;
        String sProvinNum = "";
        if (len == CHINA_ID_MIN_LENGTH || len == CHINA_ID_MAX_LENGTH) {
            sProvinNum = idCard.substring(0, 2);
        }
        sProvince = cityCodes.get(sProvinNum);
        return sProvince;

    }

}
