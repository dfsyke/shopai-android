package top.yokey.shopai.zcom.recycler;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

@SuppressWarnings("ALL")
public class SpaceDecoration extends RecyclerView.ItemDecoration {

    private int space = 0;

    public SpaceDecoration(int space) {

        this.space = space;

    }

    @Override
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {

        rect.top = space;
        rect.left = space;
        rect.right = space;
        rect.bottom = space;

    }

}
