package top.yokey.shopai.zcom.base;

@SuppressWarnings("ALL")
public class BaseErrorBean {

    private int code = -1;
    private String reason = "";

    public BaseErrorBean(int code, String reason) {

        this.code = code;
        this.reason = reason;

    }

    public int getCode() {

        return code;

    }

    public void setCode(int code) {

        this.code = code;

    }

    public String getReason() {

        return reason;

    }

    public void setReason(String reason) {

        this.reason = reason;

    }

}
