package top.yokey.shopai.zcom.help;

import android.util.Log;

import top.yokey.shopai.zcom.util.VerifyUtil;

@SuppressWarnings("ALL")
public class LoggerHelp {

    private static volatile LoggerHelp instance = null;
    private String tag = "";

    public static LoggerHelp get() {

        if (instance == null) {
            synchronized (LoggerHelp.class) {
                if (instance == null) {
                    instance = new LoggerHelp();
                }
            }
        }
        return instance;

    }

    public void init(String tag) {

        this.tag = tag;

    }

    public void show(String content) {

        if (VerifyUtil.isEmpty(content)) {
            Log.i(tag, "empty");
        } else {
            Log.i(tag, content);
        }

    }

}
