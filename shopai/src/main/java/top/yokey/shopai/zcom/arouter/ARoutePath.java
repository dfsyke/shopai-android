package top.yokey.shopai.zcom.arouter;

@SuppressWarnings("ALL")
public class ARoutePath {

    public static final String DISTRIBU_ACCOUNT = "/distribu/account";
    public static final String DISTRIBU_ADD = "/distribu/add";
    public static final String DISTRIBU_BILL = "/distribu/bill";
    public static final String DISTRIBU_CASH_APPLY = "/distribu/cash/apply";
    public static final String DISTRIBU_CASH_INFO = "/distribu/cash/info";
    public static final String DISTRIBU_CASH_LOG = "/distribu/cash/log";
    public static final String DISTRIBU_DEPOSIT = "/distribu/deposit";
    public static final String DISTRIBU_GOODS = "/distribu/goods";
    public static final String DISTRIBU_INDEX = "/distribu/index";
    public static final String DISTRIBU_ORDER = "/distribu/order";
    public static final String DISTRIBU_SHARE = "/distribu/share";

    public static final String GOODS_BUY = "/goods/buy";
    public static final String GOODS_CATE = "/goods/cate";
    public static final String GOODS_EVALUATE = "/goods/evaluate";
    public static final String GOODS_GOODS = "/goods/goods";
    public static final String GOODS_LIST = "/goods/list";
    public static final String GOODS_PIN_GOU = "/goods/pin/gou";
    public static final String GOODS_ROBBUY = "/goods/roubbuy";

    public static final String MAIN_AREA = "/main/area";
    public static final String MAIN_ARTICLE = "/main/article";
    public static final String MAIN_ARTICLE_DETAIL = "/main/article/detail";
    public static final String MAIN_BROWSER = "/main/browser";
    public static final String MAIN_LOAD = "/main/load";
    public static final String MAIN_MAIN = "/main/main";
    public static final String MAIN_RED_PACKET = "/main/red/packet";
    public static final String MAIN_SEARCH = "/main/search";
    public static final String MAIN_SIGN = "/main/sign";
    public static final String MAIN_SPECIAL = "/main/special";
    public static final String MAIN_VOUCHER = "/main/voucher";

    public static final String MEMBER_ADDRESS = "/member/address";
    public static final String MEMBER_ADDRESS_ADD = "/member/address/add";
    public static final String MEMBER_ADDRESS_MODIFY = "/member/address/modify";
    public static final String MEMBER_CENTER = "/member/center";
    public static final String MEMBER_CHAT = "/member/chat";
    public static final String MEMBER_COUPON = "/member/coupon";
    public static final String MEMBER_EMAIL = "/member/email";
    public static final String MEMBER_FAVORITE = "/member/favorite";
    public static final String MEMBER_FEEDBACK = "/member/feedback";
    public static final String MEMBER_FIND_PASS = "/member/find/pass";
    public static final String MEMBER_FOOTPRINT = "/member/footprint";
    public static final String MEMBER_INVITE = "/member/invite";
    public static final String MEMBER_INVITE_INCOME = "/member/invite/income";
    public static final String MEMBER_INVOICE = "/member/invoice";
    public static final String MEMBER_INVOICE_ADD = "/member/invoice/add";
    public static final String MEMBER_LOGIN = "/member/login";
    public static final String MEMBER_MESSAGE = "/member/message";
    public static final String MEMBER_MOBILE = "/member/mobile";
    public static final String MEMBER_MOBILE_BIND = "/member/mobile/bind";
    public static final String MEMBER_MOBILE_UNTYING = "/member/mobile/untying";
    public static final String MEMBER_PASSWORD = "/member/password";
    public static final String MEMBER_PAY_PASSWORD = "/member/pay/password";
    public static final String MEMBER_POINT = "/member/point";
    public static final String MEMBER_PRE_DEPOSIT = "/member/pre/deposit";
    public static final String MEMBER_PRE_DEPOSIT_CASH = "/member/pre/deposit/cash";
    public static final String MEMBER_PRE_DEPOSIT_CASH_DETAILED = "/member/pre/deposit/cash/deltailed";
    public static final String MEMBER_PRE_DEPOSIT_LOG = "/member/pre/deposit/log";
    public static final String MEMBER_PRE_DEPOSIT_PAY = "/member/pre/deposit/pay";
    public static final String MEMBER_RECHARGE_CARD = "/member/recharge/card";
    public static final String MEMBER_RED_PACKET = "/member/red/packet";
    public static final String MEMBER_REGISTER = "/member/register";
    public static final String MEMBER_SETTING = "/member/setting";
    public static final String MEMBER_VOUCHER = "/member/voucher";

    public static final String ORDER_DETAILED = "/order/detailed";
    public static final String ORDER_EVALUATE = "/order/evaluate";
    public static final String ORDER_EVALUATE_AGAIN = "/order/evaluate/again";
    public static final String ORDER_LIST = "/order/list";
    public static final String ORDER_LOGISTICS = "/order/logistics";
    public static final String ORDER_PAY = "/order/pay";
    public static final String ORDER_PIN_GOU = "/order/pin/gou";

    public static final String POINT_POINT = "/point/point";
    public static final String POINT_BUY = "/point/buy";
    public static final String POINT_LIST = "/point/list";
    public static final String POINT_ORDER = "/point/order";
    public static final String POINT_ORDER_LIST = "/point/order/list";

    public static final String RETREAT_LIST = "/retreat/list";
    public static final String RETREAT_REFUND = "/retreat/refund";
    public static final String RETREAT_REFUND_ALL = "/retreat/refund/all";
    public static final String RETREAT_REFUND_INFO = "/retreat/refund/info";
    public static final String RETREAT_RETURN = "/retreat/return";
    public static final String RETREAT_RETURN_DELIVER = "/retreat/return/deliver";
    public static final String RETREAT_RETURN_INFO = "/retreat/return/info";

    public static final String SELLER_ADDRESS = "/seller/address";
    public static final String SELLER_CHAT = "/seller/chat";
    public static final String SELLER_DELIVER = "/seller/deliver";
    public static final String SELLER_EXPRESS = "/seller/express";
    public static final String SELLER_INDEX = "/seller/index";
    public static final String SELLER_LOGIN = "/seller/login";
    public static final String SELLER_MESSAGE = "/seller/message";
    public static final String SELLER_ORDER = "/seller/order";
    public static final String SELLER_SETTING = "/seller/setting";

    public static final String STORE_CLASS = "/store/class";
    public static final String STORE_GOODS = "/store/goods";
    public static final String STORE_INDEX = "/store/index";
    public static final String STORE_INFO = "/store/info";
    public static final String STORE_LIST = "/store/list";
    public static final String STORE_SEARCH = "/store/search";
    public static final String STORE_VOUCHER = "/store/voucher";

}
