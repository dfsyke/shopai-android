package top.yokey.shopai.zcom.view;

@SuppressWarnings("ALL")
public class PullRefreshViewHelper {

    public static final int LOAD_STYLE_ROTAP = 1;
    public static final int LOAD_STYLE_DOUBLE = 2;
    public static final int LOAD_STYLE_WAVE = 3;
    public static final int LOAD_STYLE_WAND = 4;
    public static final int LOAD_STYLE_PULSE = 5;
    public static final int LOAD_STYLE_CHASING = 6;
    public static final int LOAD_STYLE_THREE = 7;
    public static final int LOAD_STYLE_CIRCLE = 8;
    public static final int LOAD_STYLE_CUBE = 9;
    public static final int LOAD_STYLE_FADING = 10;
    public static final int LOAD_STYLE_FOLDING = 11;
    public static final int LOAD_STYLE_ROTAC = 12;
    private static volatile PullRefreshViewHelper instance = null;
    private int[] colors = null;
    private int textSize = 0;
    private int loadStyle = 0, emptyImage = 0, errorImage = 0;
    private int loadColor = 0, emptyColor = 0, errorColor = 0;
    private String loadText = "", emptyText = "", errorText = "";

    public static PullRefreshViewHelper get() {

        if (instance == null) {
            synchronized (PullRefreshViewHelper.class) {
                if (instance == null) {
                    instance = new PullRefreshViewHelper();
                }
            }
        }
        return instance;

    }

    public void init(int textSize) {

        this.textSize = textSize;

    }

    public int[] getColors() {

        return colors;

    }

    public void setColors(int... colors) {

        this.colors = colors;

    }

    public int getTextSize() {

        return textSize;

    }

    public void setTextSize(int textSize) {

        this.textSize = textSize;

    }

    public int getLoadStyle() {

        return loadStyle;

    }

    public void setLoadStyle(int loadStyle) {

        this.loadStyle = loadStyle;

    }

    public int getEmptyImage() {

        return emptyImage;

    }

    public void setEmptyImage(int emptyImage) {

        this.emptyImage = emptyImage;

    }

    public int getErrorImage() {

        return errorImage;

    }

    public void setErrorImage(int errorImage) {

        this.errorImage = errorImage;

    }

    public int getLoadColor() {

        return loadColor;

    }

    public void setLoadColor(int loadColor) {

        this.loadColor = loadColor;

    }

    public int getEmptyColor() {

        return emptyColor;

    }

    public void setEmptyColor(int emptyColor) {

        this.emptyColor = emptyColor;

    }

    public int getErrorColor() {

        return errorColor;

    }

    public void setErrorColor(int errorColor) {

        this.errorColor = errorColor;

    }

    public String getLoadText() {

        return loadText;

    }

    public void setLoadText(String loadText) {

        this.loadText = loadText;

    }

    public String getEmptyText() {

        return emptyText;

    }

    public void setEmptyText(String emptyText) {

        this.emptyText = emptyText;

    }

    public String getErrorText() {

        return errorText;

    }

    public void setErrorText(String errorText) {

        this.errorText = errorText;

    }

}
