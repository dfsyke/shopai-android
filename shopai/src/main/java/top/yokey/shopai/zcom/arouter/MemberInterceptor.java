package top.yokey.shopai.zcom.arouter;

import android.content.Context;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Interceptor;
import com.alibaba.android.arouter.facade.callback.InterceptorCallback;
import com.alibaba.android.arouter.facade.template.IInterceptor;

import top.yokey.shopai.zcom.App;

@SuppressWarnings("ALL")
@Interceptor(priority = 1)
public class MemberInterceptor implements IInterceptor {

    private String[] paths = null;
    private Context context = null;

    @Override
    public void init(Context context) {

        this.context = context;
        this.paths = new String[]{
                //Distribu
                ARoutePath.DISTRIBU_ACCOUNT, ARoutePath.DISTRIBU_ADD, ARoutePath.DISTRIBU_BILL, ARoutePath.DISTRIBU_CASH_APPLY,
                ARoutePath.DISTRIBU_CASH_INFO, ARoutePath.DISTRIBU_CASH_LOG, ARoutePath.DISTRIBU_DEPOSIT, ARoutePath.DISTRIBU_GOODS,
                ARoutePath.DISTRIBU_INDEX, ARoutePath.DISTRIBU_ORDER, ARoutePath.DISTRIBU_SHARE,
                //Goods
                ARoutePath.GOODS_BUY,
                //Main
                ARoutePath.MAIN_SIGN,
                //Member
                ARoutePath.MEMBER_ADDRESS, ARoutePath.MEMBER_ADDRESS_ADD, ARoutePath.MEMBER_ADDRESS_MODIFY,
                ARoutePath.MEMBER_CENTER, ARoutePath.MEMBER_CHAT, ARoutePath.MEMBER_COUPON, ARoutePath.MEMBER_EMAIL,
                ARoutePath.MEMBER_FAVORITE, ARoutePath.MEMBER_FEEDBACK, ARoutePath.MEMBER_FOOTPRINT, ARoutePath.MEMBER_INVITE,
                ARoutePath.MEMBER_INVITE_INCOME, ARoutePath.MEMBER_INVOICE, ARoutePath.MEMBER_INVOICE_ADD, ARoutePath.MEMBER_MESSAGE,
                ARoutePath.MEMBER_MOBILE, ARoutePath.MEMBER_MOBILE_BIND, ARoutePath.MEMBER_MOBILE_UNTYING, ARoutePath.MEMBER_PASSWORD,
                ARoutePath.MEMBER_PAY_PASSWORD, ARoutePath.MEMBER_POINT, ARoutePath.MEMBER_PRE_DEPOSIT, ARoutePath.MEMBER_PRE_DEPOSIT_CASH,
                ARoutePath.MEMBER_PRE_DEPOSIT_CASH_DETAILED, ARoutePath.MEMBER_PRE_DEPOSIT_LOG, ARoutePath.MEMBER_PRE_DEPOSIT_PAY,
                ARoutePath.MEMBER_RECHARGE_CARD, ARoutePath.MEMBER_RED_PACKET, ARoutePath.MEMBER_SETTING, ARoutePath.MEMBER_VOUCHER,
                //Order
                ARoutePath.ORDER_DETAILED, ARoutePath.ORDER_EVALUATE, ARoutePath.ORDER_EVALUATE_AGAIN, ARoutePath.ORDER_LIST, ARoutePath.ORDER_LOGISTICS,
                ARoutePath.ORDER_PAY, ARoutePath.ORDER_PIN_GOU,
                //Point
                ARoutePath.POINT_POINT, ARoutePath.POINT_BUY, ARoutePath.POINT_LIST, ARoutePath.POINT_ORDER, ARoutePath.POINT_ORDER_LIST,
                //Retreat
                ARoutePath.RETREAT_LIST, ARoutePath.RETREAT_REFUND, ARoutePath.RETREAT_REFUND_ALL, ARoutePath.RETREAT_REFUND_INFO, ARoutePath.RETREAT_RETURN,
                ARoutePath.RETREAT_RETURN_DELIVER, ARoutePath.RETREAT_RETURN_INFO,
        };

    }

    @Override
    public void process(Postcard postcard, InterceptorCallback callback) {

        if (check(postcard.getPath())) {
            if (App.get().isMemberLogin()) {
                callback.onContinue(postcard);
            } else {
                App.get().startMemberLogin();
            }
        } else {
            callback.onContinue(postcard);
        }

    }

    //自定义方法

    private boolean check(String path) {

        for (String str : paths) {
            if (str.equals(path)) {
                return true;
            }
        }

        return false;

    }

}
