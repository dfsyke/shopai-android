package top.yokey.shopai.zcom.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import top.yokey.shopai.R;

@SuppressWarnings("ALL")
public class ArcView extends View {

    private int width = 0;
    private int height = 0;
    private int arcHeight = 0;
    private int bgColor = 0;
    private Paint paint = null;
    private Context context = null;

    public ArcView(Context context) {

        this(context, null);

    }

    public ArcView(Context context, @Nullable AttributeSet attrs) {

        this(context, attrs, 0);

    }

    public ArcView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {

        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ArcView);
        arcHeight = typedArray.getDimensionPixelSize(R.styleable.ArcView_arcHeight, 0);
        bgColor = typedArray.getColor(R.styleable.ArcView_bgColor, Color.parseColor("#303F9F"));
        this.context = context;
        paint = new Paint();

    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(bgColor);
        paint.setAntiAlias(true);
        Rect rect = new Rect(0, 0, width, height - arcHeight);
        canvas.drawRect(rect, paint);
        Path path = new Path();
        path.moveTo(0, height - arcHeight);
        path.quadTo(width / 2, height, width, height - arcHeight);
        canvas.drawPath(path, paint);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        }
        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        }
        setMeasuredDimension(width, height);

    }

}
