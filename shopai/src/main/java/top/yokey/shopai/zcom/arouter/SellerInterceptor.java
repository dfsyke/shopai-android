package top.yokey.shopai.zcom.arouter;

import android.content.Context;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Interceptor;
import com.alibaba.android.arouter.facade.callback.InterceptorCallback;
import com.alibaba.android.arouter.facade.template.IInterceptor;

import top.yokey.shopai.zcom.App;

@SuppressWarnings("ALL")
@Interceptor(priority = 2)
public class SellerInterceptor implements IInterceptor {

    private String[] paths = null;
    private Context context = null;

    @Override
    public void init(Context context) {

        this.context = context;
        this.paths = new String[]{
                //Seller
                ARoutePath.SELLER_ADDRESS, ARoutePath.SELLER_CHAT, ARoutePath.SELLER_DELIVER, ARoutePath.SELLER_EXPRESS,
                ARoutePath.SELLER_INDEX, ARoutePath.SELLER_MESSAGE, ARoutePath.SELLER_ORDER, ARoutePath.SELLER_SETTING,
        };

    }

    @Override
    public void process(Postcard postcard, InterceptorCallback callback) {

        if (check(postcard.getPath())) {
            if (App.get().isSellerLogin()) {
                callback.onContinue(postcard);
            } else {
                App.get().startSellerLogin();
            }
        } else {
            callback.onContinue(postcard);
        }

    }

    //自定义方法

    private boolean check(String path) {

        for (String str : paths) {
            if (str.equals(path)) {
                return true;
            }
        }

        return false;

    }

}
